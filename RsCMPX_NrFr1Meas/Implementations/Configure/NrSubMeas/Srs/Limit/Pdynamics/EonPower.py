from typing import List

from .......Internal.Core import Core
from .......Internal.CommandsGroup import CommandsGroup
from .......Internal import Conversions
from .......Internal.RepeatedCapability import RepeatedCapability
from ....... import repcap


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class EonPowerCls:
	"""EonPower commands group definition. 1 total commands, 0 Subgroups, 1 group commands
	Repeated Capability: EonPowerScs, default value after init: EonPowerScs.Nr15"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("eonPower", core, parent)
		self._cmd_group.rep_cap = RepeatedCapability(self._cmd_group.group_name, 'repcap_eonPowerScs_get', 'repcap_eonPowerScs_set', repcap.EonPowerScs.Nr15)

	def repcap_eonPowerScs_set(self, eonPowerScs: repcap.EonPowerScs) -> None:
		"""Repeated Capability default value numeric suffix.
		This value is used, if you do not explicitely set it in the child set/get methods, or if you leave it to EonPowerScs.Default
		Default value after init: EonPowerScs.Nr15"""
		self._cmd_group.set_repcap_enum_value(eonPowerScs)

	def repcap_eonPowerScs_get(self) -> repcap.EonPowerScs:
		"""Returns the current default repeated capability for the child set/get methods"""
		# noinspection PyTypeChecker
		return self._cmd_group.get_repcap_enum_value()

	def set(self, on_power: List[float], eonPowerScs=repcap.EonPowerScs.Default) -> None:
		"""SCPI: CONFigure:NRSub:MEASurement<Instance>:SRS:LIMit:PDYNamics:EONPower<scs> \n
		Snippet: driver.configure.nrSubMeas.srs.limit.pdynamics.eonPower.set(on_power = [1.1, 2.2, 3.3], eonPowerScs = repcap.EonPowerScs.Default) \n
		No command help available \n
			:param on_power: No help available
			:param eonPowerScs: optional repeated capability selector. Default value: Nr15 (settable in the interface 'EonPower')
		"""
		param = Conversions.list_to_csv_str(on_power)
		eonPowerScs_cmd_val = self._cmd_group.get_repcap_cmd_value(eonPowerScs, repcap.EonPowerScs)
		self._core.io.write(f'CONFigure:NRSub:MEASurement<Instance>:SRS:LIMit:PDYNamics:EONPower{eonPowerScs_cmd_val} {param}')

	def get(self, eonPowerScs=repcap.EonPowerScs.Default) -> List[float]:
		"""SCPI: CONFigure:NRSub:MEASurement<Instance>:SRS:LIMit:PDYNamics:EONPower<scs> \n
		Snippet: value: List[float] = driver.configure.nrSubMeas.srs.limit.pdynamics.eonPower.get(eonPowerScs = repcap.EonPowerScs.Default) \n
		No command help available \n
			:param eonPowerScs: optional repeated capability selector. Default value: Nr15 (settable in the interface 'EonPower')
			:return: on_power: No help available"""
		eonPowerScs_cmd_val = self._cmd_group.get_repcap_cmd_value(eonPowerScs, repcap.EonPowerScs)
		response = self._core.io.query_bin_or_ascii_float_list(f'CONFigure:NRSub:MEASurement<Instance>:SRS:LIMit:PDYNamics:EONPower{eonPowerScs_cmd_val}?')
		return response

	def clone(self) -> 'EonPowerCls':
		"""Clones the group by creating new object from it and its whole existing subgroups
		Also copies all the existing default Repeated Capabilities setting,
		which you can change independently without affecting the original group"""
		new_group = EonPowerCls(self._core, self._cmd_group.parent)
		self._cmd_group.synchronize_repcaps(new_group)
		return new_group
