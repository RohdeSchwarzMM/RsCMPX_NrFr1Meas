Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:SRS:TRACe:PDYNamics:MAXimum
	single: FETCh:NRSub:MEASurement<Instance>:SRS:TRACe:PDYNamics:MAXimum

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:SRS:TRACe:PDYNamics:MAXimum
	FETCh:NRSub:MEASurement<Instance>:SRS:TRACe:PDYNamics:MAXimum



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.Srs.Trace.Pdynamics.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: