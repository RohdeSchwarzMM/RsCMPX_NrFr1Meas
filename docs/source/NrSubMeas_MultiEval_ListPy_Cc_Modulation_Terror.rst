Terror
----------------------------------------





.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.ListPy.Cc.Modulation.Terror.TerrorCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrSubMeas.multiEval.listPy.cc.modulation.terror.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrSubMeas_MultiEval_ListPy_Cc_Modulation_Terror_Average.rst
	NrSubMeas_MultiEval_ListPy_Cc_Modulation_Terror_Current.rst
	NrSubMeas_MultiEval_ListPy_Cc_Modulation_Terror_Extreme.rst
	NrSubMeas_MultiEval_ListPy_Cc_Modulation_Terror_StandardDev.rst