Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:MEValuation:TRACe[:LAYer<layer>]:PDYNamics:CURRent
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:TRACe[:LAYer<layer>]:PDYNamics:CURRent

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:MEValuation:TRACe[:LAYer<layer>]:PDYNamics:CURRent
	FETCh:NRSub:MEASurement<Instance>:MEValuation:TRACe[:LAYer<layer>]:PDYNamics:CURRent



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.Trace.Layer.Pdynamics.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: