Lcheck
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NRSub:MEASurement<Instance>:TPC:PSTeps:LCHeck

.. code-block:: python

	FETCh:NRSub:MEASurement<Instance>:TPC:PSTeps:LCHeck



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.Tpc.Psteps.Lcheck.LcheckCls
	:members:
	:undoc-members:
	:noindex: