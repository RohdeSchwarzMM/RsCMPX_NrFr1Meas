Result
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:PRACh:RESult:MODulation
	single: CONFigure:NRSub:MEASurement<Instance>:PRACh:RESult:PDYNamics
	single: CONFigure:NRSub:MEASurement<Instance>:PRACh:RESult[:ALL]
	single: CONFigure:NRSub:MEASurement<Instance>:PRACh:RESult:EVMagnitude
	single: CONFigure:NRSub:MEASurement<Instance>:PRACh:RESult:EVPReamble
	single: CONFigure:NRSub:MEASurement<Instance>:PRACh:RESult:MERRor
	single: CONFigure:NRSub:MEASurement<Instance>:PRACh:RESult:PERRor
	single: CONFigure:NRSub:MEASurement<Instance>:PRACh:RESult:IQ
	single: CONFigure:NRSub:MEASurement<Instance>:PRACh:RESult:PVPReamble
	single: CONFigure:NRSub:MEASurement<Instance>:PRACh:RESult:TXM

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:PRACh:RESult:MODulation
	CONFigure:NRSub:MEASurement<Instance>:PRACh:RESult:PDYNamics
	CONFigure:NRSub:MEASurement<Instance>:PRACh:RESult[:ALL]
	CONFigure:NRSub:MEASurement<Instance>:PRACh:RESult:EVMagnitude
	CONFigure:NRSub:MEASurement<Instance>:PRACh:RESult:EVPReamble
	CONFigure:NRSub:MEASurement<Instance>:PRACh:RESult:MERRor
	CONFigure:NRSub:MEASurement<Instance>:PRACh:RESult:PERRor
	CONFigure:NRSub:MEASurement<Instance>:PRACh:RESult:IQ
	CONFigure:NRSub:MEASurement<Instance>:PRACh:RESult:PVPReamble
	CONFigure:NRSub:MEASurement<Instance>:PRACh:RESult:TXM



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.Prach.Result.ResultCls
	:members:
	:undoc-members:
	:noindex: