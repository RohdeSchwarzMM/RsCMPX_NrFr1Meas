Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:PRACh:TRACe:PDYNamics:MAXimum
	single: FETCh:NRSub:MEASurement<Instance>:PRACh:TRACe:PDYNamics:MAXimum

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:PRACh:TRACe:PDYNamics:MAXimum
	FETCh:NRSub:MEASurement<Instance>:PRACh:TRACe:PDYNamics:MAXimum



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.Prach.Trace.Pdynamics.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: