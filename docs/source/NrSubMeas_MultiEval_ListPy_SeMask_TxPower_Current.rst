Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST:SEMask:TXPower:CURRent
	single: CALCulate:NRSub:MEASurement<Instance>:MEValuation:LIST:SEMask:TXPower:CURRent

.. code-block:: python

	FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST:SEMask:TXPower:CURRent
	CALCulate:NRSub:MEASurement<Instance>:MEValuation:LIST:SEMask:TXPower:CURRent



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.ListPy.SeMask.TxPower.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: