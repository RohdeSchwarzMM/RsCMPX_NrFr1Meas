Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:SRS:PDYNamics:MINimum
	single: FETCh:NRSub:MEASurement<Instance>:SRS:PDYNamics:MINimum
	single: CALCulate:NRSub:MEASurement<Instance>:SRS:PDYNamics:MINimum

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:SRS:PDYNamics:MINimum
	FETCh:NRSub:MEASurement<Instance>:SRS:PDYNamics:MINimum
	CALCulate:NRSub:MEASurement<Instance>:SRS:PDYNamics:MINimum



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.Srs.Pdynamics.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: