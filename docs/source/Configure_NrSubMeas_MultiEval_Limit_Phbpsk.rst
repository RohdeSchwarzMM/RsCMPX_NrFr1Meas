Phbpsk
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:LIMit:PHBPsk:FERRor
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:LIMit:PHBPsk:ESFLatness

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:MEValuation:LIMit:PHBPsk:FERRor
	CONFigure:NRSub:MEASurement<Instance>:MEValuation:LIMit:PHBPsk:ESFLatness



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.MultiEval.Limit.Phbpsk.PhbpskCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.nrSubMeas.multiEval.limit.phbpsk.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_NrSubMeas_MultiEval_Limit_Phbpsk_EvMagnitude.rst
	Configure_NrSubMeas_MultiEval_Limit_Phbpsk_Ibe.rst
	Configure_NrSubMeas_MultiEval_Limit_Phbpsk_IqOffset.rst
	Configure_NrSubMeas_MultiEval_Limit_Phbpsk_Merror.rst
	Configure_NrSubMeas_MultiEval_Limit_Phbpsk_Perror.rst