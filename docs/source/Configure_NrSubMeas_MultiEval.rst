MultiEval
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:PFORmat
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:TOUT
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:DMODe
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:SCSPacing
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:CBANdwidth
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:DFTPrecoding
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:MSCHeme
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:CPRefix
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:NSValue
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:PLCid
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:CTYPe
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:PUSChconfig
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:MAPType
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:REPetition
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:SCONdition
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:MMODe
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:MOEXception
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:NVFilter
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:CTVFilter
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:NSUBframes
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:FSTRucture
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:DSSPusch
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:GHOPping

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:MEValuation:PFORmat
	CONFigure:NRSub:MEASurement<Instance>:MEValuation:TOUT
	CONFigure:NRSub:MEASurement<Instance>:MEValuation:DMODe
	CONFigure:NRSub:MEASurement<Instance>:MEValuation:SCSPacing
	CONFigure:NRSub:MEASurement<Instance>:MEValuation:CBANdwidth
	CONFigure:NRSub:MEASurement<Instance>:MEValuation:DFTPrecoding
	CONFigure:NRSub:MEASurement<Instance>:MEValuation:MSCHeme
	CONFigure:NRSub:MEASurement<Instance>:MEValuation:CPRefix
	CONFigure:NRSub:MEASurement<Instance>:MEValuation:NSValue
	CONFigure:NRSub:MEASurement<Instance>:MEValuation:PLCid
	CONFigure:NRSub:MEASurement<Instance>:MEValuation:CTYPe
	CONFigure:NRSub:MEASurement<Instance>:MEValuation:PUSChconfig
	CONFigure:NRSub:MEASurement<Instance>:MEValuation:MAPType
	CONFigure:NRSub:MEASurement<Instance>:MEValuation:REPetition
	CONFigure:NRSub:MEASurement<Instance>:MEValuation:SCONdition
	CONFigure:NRSub:MEASurement<Instance>:MEValuation:MMODe
	CONFigure:NRSub:MEASurement<Instance>:MEValuation:MOEXception
	CONFigure:NRSub:MEASurement<Instance>:MEValuation:NVFilter
	CONFigure:NRSub:MEASurement<Instance>:MEValuation:CTVFilter
	CONFigure:NRSub:MEASurement<Instance>:MEValuation:NSUBframes
	CONFigure:NRSub:MEASurement<Instance>:MEValuation:FSTRucture
	CONFigure:NRSub:MEASurement<Instance>:MEValuation:DSSPusch
	CONFigure:NRSub:MEASurement<Instance>:MEValuation:GHOPping



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.MultiEval.MultiEvalCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.nrSubMeas.multiEval.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_NrSubMeas_MultiEval_Allocation.rst
	Configure_NrSubMeas_MultiEval_BwConfig.rst
	Configure_NrSubMeas_MultiEval_Dmrs.rst
	Configure_NrSubMeas_MultiEval_Endc.rst
	Configure_NrSubMeas_MultiEval_Limit.rst
	Configure_NrSubMeas_MultiEval_ListPy.rst
	Configure_NrSubMeas_MultiEval_Modulation.rst
	Configure_NrSubMeas_MultiEval_Mslot.rst
	Configure_NrSubMeas_MultiEval_MsubFrames.rst
	Configure_NrSubMeas_MultiEval_Pcomp.rst
	Configure_NrSubMeas_MultiEval_Pdynamics.rst
	Configure_NrSubMeas_MultiEval_RbAllocation.rst
	Configure_NrSubMeas_MultiEval_Result.rst
	Configure_NrSubMeas_MultiEval_Scount.rst
	Configure_NrSubMeas_MultiEval_Spectrum.rst
	Configure_NrSubMeas_MultiEval_Trace.rst