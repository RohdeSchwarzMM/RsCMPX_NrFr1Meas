Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:SRS:EVMSymbol:CURRent
	single: FETCh:NRSub:MEASurement<Instance>:SRS:EVMSymbol:CURRent

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:SRS:EVMSymbol:CURRent
	FETCh:NRSub:MEASurement<Instance>:SRS:EVMSymbol:CURRent



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.Srs.EvmSymbol.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: