Caggregation
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:CAGGregation:MCARrier
	single: CONFigure:NRSub:MEASurement<Instance>:CAGGregation:DCARrier

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:CAGGregation:MCARrier
	CONFigure:NRSub:MEASurement<Instance>:CAGGregation:DCARrier



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.Caggregation.CaggregationCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.nrSubMeas.caggregation.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_NrSubMeas_Caggregation_AcSpacing.rst
	Configure_NrSubMeas_Caggregation_Cbandwidth.rst
	Configure_NrSubMeas_Caggregation_Frequency.rst