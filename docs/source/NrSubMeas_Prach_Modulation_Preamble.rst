Preamble<Preamble>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr64
	rc = driver.nrSubMeas.prach.modulation.preamble.repcap_preamble_get()
	driver.nrSubMeas.prach.modulation.preamble.repcap_preamble_set(repcap.Preamble.Nr1)



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:PRACh:MODulation:PREamble<Number>
	single: FETCh:NRSub:MEASurement<Instance>:PRACh:MODulation:PREamble<Number>

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:PRACh:MODulation:PREamble<Number>
	FETCh:NRSub:MEASurement<Instance>:PRACh:MODulation:PREamble<Number>



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.Prach.Modulation.Preamble.PreambleCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrSubMeas.prach.modulation.preamble.clone()