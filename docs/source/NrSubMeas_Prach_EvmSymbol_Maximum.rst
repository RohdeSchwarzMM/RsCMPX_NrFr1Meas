Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:PRACh:EVMSymbol:MAXimum
	single: FETCh:NRSub:MEASurement<Instance>:PRACh:EVMSymbol:MAXimum
	single: CALCulate:NRSub:MEASurement<Instance>:PRACh:EVMSymbol:MAXimum

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:PRACh:EVMSymbol:MAXimum
	FETCh:NRSub:MEASurement<Instance>:PRACh:EVMSymbol:MAXimum
	CALCulate:NRSub:MEASurement<Instance>:PRACh:EVMSymbol:MAXimum



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.Prach.EvmSymbol.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: