State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:STATe

.. code-block:: python

	FETCh:NRSub:MEASurement<Instance>:MEValuation:STATe



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.State.StateCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrSubMeas.multiEval.state.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrSubMeas_MultiEval_State_All.rst