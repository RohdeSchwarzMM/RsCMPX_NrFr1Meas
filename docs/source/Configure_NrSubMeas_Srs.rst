Srs
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:SRS:TOUT
	single: CONFigure:NRSub:MEASurement<Instance>:SRS:REPetition
	single: CONFigure:NRSub:MEASurement<Instance>:SRS:SCONdition
	single: CONFigure:NRSub:MEASurement<Instance>:SRS:MOEXception
	single: CONFigure:NRSub:MEASurement<Instance>:SRS:SEQuence
	single: CONFigure:NRSub:MEASurement<Instance>:SRS:SPOSition
	single: CONFigure:NRSub:MEASurement<Instance>:SRS:NOSYmbols
	single: CONFigure:NRSub:MEASurement<Instance>:SRS:PERiodicity
	single: CONFigure:NRSub:MEASurement<Instance>:SRS:NOSubframes

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:SRS:TOUT
	CONFigure:NRSub:MEASurement<Instance>:SRS:REPetition
	CONFigure:NRSub:MEASurement<Instance>:SRS:SCONdition
	CONFigure:NRSub:MEASurement<Instance>:SRS:MOEXception
	CONFigure:NRSub:MEASurement<Instance>:SRS:SEQuence
	CONFigure:NRSub:MEASurement<Instance>:SRS:SPOSition
	CONFigure:NRSub:MEASurement<Instance>:SRS:NOSYmbols
	CONFigure:NRSub:MEASurement<Instance>:SRS:PERiodicity
	CONFigure:NRSub:MEASurement<Instance>:SRS:NOSubframes



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.Srs.SrsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.nrSubMeas.srs.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_NrSubMeas_Srs_Limit.rst
	Configure_NrSubMeas_Srs_Modulation.rst
	Configure_NrSubMeas_Srs_Pdynamics.rst
	Configure_NrSubMeas_Srs_Result.rst
	Configure_NrSubMeas_Srs_Rmapping.rst
	Configure_NrSubMeas_Srs_Rtype.rst
	Configure_NrSubMeas_Srs_Scount.rst
	Configure_NrSubMeas_Srs_Tcomb.rst