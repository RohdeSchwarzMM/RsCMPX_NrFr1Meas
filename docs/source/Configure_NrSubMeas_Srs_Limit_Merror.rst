Merror
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:SRS:LIMit:MERRor

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:SRS:LIMit:MERRor



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.Srs.Limit.Merror.MerrorCls
	:members:
	:undoc-members:
	:noindex: