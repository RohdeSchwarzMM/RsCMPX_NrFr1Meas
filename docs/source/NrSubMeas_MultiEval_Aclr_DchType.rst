DchType
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:ACLR:DCHType

.. code-block:: python

	FETCh:NRSub:MEASurement<Instance>:MEValuation:ACLR:DCHType



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.Aclr.DchType.DchTypeCls
	:members:
	:undoc-members:
	:noindex: