AdMrs
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>[:CC<no>]:BWPart:PUCCh:ADMRs

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>[:CC<no>]:BWPart:PUCCh:ADMRs



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.Cc.BwPart.Pucch.AdMrs.AdMrsCls
	:members:
	:undoc-members:
	:noindex: