Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:ACLR:ENDC:AVERage
	single: CALCulate:NRSub:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:ACLR:ENDC:AVERage

.. code-block:: python

	FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:ACLR:ENDC:AVERage
	CALCulate:NRSub:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:ACLR:ENDC:AVERage



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.ListPy.Segment.Aclr.Endc.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: