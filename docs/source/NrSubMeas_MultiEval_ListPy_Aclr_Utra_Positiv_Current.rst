Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST:ACLR:UTRA<nr6g>:POSitiv:CURRent
	single: CALCulate:NRSub:MEASurement<Instance>:MEValuation:LIST:ACLR:UTRA<nr6g>:POSitiv:CURRent

.. code-block:: python

	FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST:ACLR:UTRA<nr6g>:POSitiv:CURRent
	CALCulate:NRSub:MEASurement<Instance>:MEValuation:LIST:ACLR:UTRA<nr6g>:POSitiv:CURRent



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.ListPy.Aclr.Utra.Positiv.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: