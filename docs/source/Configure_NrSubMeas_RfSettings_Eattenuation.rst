Eattenuation
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:RFSettings:EATTenuation

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:RFSettings:EATTenuation



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.RfSettings.Eattenuation.EattenuationCls
	:members:
	:undoc-members:
	:noindex: