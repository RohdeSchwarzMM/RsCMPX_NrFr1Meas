Tpc
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:TPC:TOUT
	single: CONFigure:NRSub:MEASurement<Instance>:TPC:DIRection
	single: CONFigure:NRSub:MEASurement<Instance>:TPC:STID
	single: CONFigure:NRSub:MEASurement<Instance>:TPC:PATTern

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:TPC:TOUT
	CONFigure:NRSub:MEASurement<Instance>:TPC:DIRection
	CONFigure:NRSub:MEASurement<Instance>:TPC:STID
	CONFigure:NRSub:MEASurement<Instance>:TPC:PATTern



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.Tpc.TpcCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.nrSubMeas.tpc.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_NrSubMeas_Tpc_Limit.rst