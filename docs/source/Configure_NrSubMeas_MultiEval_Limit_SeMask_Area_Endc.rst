Endc
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:LIMit:SEMask:AREA<nr>:ENDC

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:MEValuation:LIMit:SEMask:AREA<nr>:ENDC



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.MultiEval.Limit.SeMask.Area.Endc.EndcCls
	:members:
	:undoc-members:
	:noindex: