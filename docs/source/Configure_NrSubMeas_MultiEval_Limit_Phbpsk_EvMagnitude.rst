EvMagnitude
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:LIMit:PHBPsk:EVMagnitude

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:MEValuation:LIMit:PHBPsk:EVMagnitude



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.MultiEval.Limit.Phbpsk.EvMagnitude.EvMagnitudeCls
	:members:
	:undoc-members:
	:noindex: