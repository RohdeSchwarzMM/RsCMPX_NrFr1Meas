Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:MEValuation[:CC<no>][:LAYer<layer>]:PERRor:CURRent
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation[:CC<no>][:LAYer<layer>]:PERRor:CURRent
	single: CALCulate:NRSub:MEASurement<Instance>:MEValuation[:CC<no>][:LAYer<layer>]:PERRor:CURRent

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:MEValuation[:CC<no>][:LAYer<layer>]:PERRor:CURRent
	FETCh:NRSub:MEASurement<Instance>:MEValuation[:CC<no>][:LAYer<layer>]:PERRor:CURRent
	CALCulate:NRSub:MEASurement<Instance>:MEValuation[:CC<no>][:LAYer<layer>]:PERRor:CURRent



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.Cc.Layer.Perror.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: