Peak
----------------------------------------





.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.Cc.Layer.EvMagnitude.Peak.PeakCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrSubMeas.multiEval.cc.layer.evMagnitude.peak.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrSubMeas_MultiEval_Cc_Layer_EvMagnitude_Peak_Average.rst
	NrSubMeas_MultiEval_Cc_Layer_EvMagnitude_Peak_Current.rst
	NrSubMeas_MultiEval_Cc_Layer_EvMagnitude_Peak_Maximum.rst