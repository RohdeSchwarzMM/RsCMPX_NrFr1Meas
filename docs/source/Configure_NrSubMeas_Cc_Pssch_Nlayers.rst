Nlayers
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>[:CC<no>]:PSSCh:NLAYers

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>[:CC<no>]:PSSCh:NLAYers



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.Cc.Pssch.Nlayers.NlayersCls
	:members:
	:undoc-members:
	:noindex: