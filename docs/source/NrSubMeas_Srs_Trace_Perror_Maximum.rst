Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:SRS:TRACe:PERRor:MAXimum
	single: FETCh:NRSub:MEASurement<Instance>:SRS:TRACe:PERRor:MAXimum

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:SRS:TRACe:PERRor:MAXimum
	FETCh:NRSub:MEASurement<Instance>:SRS:TRACe:PERRor:MAXimum



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.Srs.Trace.Perror.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: