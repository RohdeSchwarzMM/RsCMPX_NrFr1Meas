ObwLimit
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:LIMit:SEMask:UDEFined:OBWLimit:CAGGregation

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:MEValuation:LIMit:SEMask:UDEFined:OBWLimit:CAGGregation



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.MultiEval.Limit.SeMask.UserDefined.ObwLimit.ObwLimitCls
	:members:
	:undoc-members:
	:noindex: