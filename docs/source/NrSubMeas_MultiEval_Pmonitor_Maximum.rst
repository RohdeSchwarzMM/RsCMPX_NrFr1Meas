Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:MEValuation:PMONitor:MAXimum
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:PMONitor:MAXimum

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:MEValuation:PMONitor:MAXimum
	FETCh:NRSub:MEASurement<Instance>:MEValuation:PMONitor:MAXimum



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.Pmonitor.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: