StandardDev
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:SRS:PDYNamics:SDEViation
	single: FETCh:NRSub:MEASurement<Instance>:SRS:PDYNamics:SDEViation
	single: CALCulate:NRSub:MEASurement<Instance>:SRS:PDYNamics:SDEViation

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:SRS:PDYNamics:SDEViation
	FETCh:NRSub:MEASurement<Instance>:SRS:PDYNamics:SDEViation
	CALCulate:NRSub:MEASurement<Instance>:SRS:PDYNamics:SDEViation



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.Srs.Pdynamics.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: