Minimum
----------------------------------------





.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.ListPy.Segment.SeMask.Margin.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrSubMeas.multiEval.listPy.segment.seMask.margin.minimum.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrSubMeas_MultiEval_ListPy_Segment_SeMask_Margin_Minimum_Negativ.rst
	NrSubMeas_MultiEval_ListPy_Segment_SeMask_Margin_Minimum_Positiv.rst