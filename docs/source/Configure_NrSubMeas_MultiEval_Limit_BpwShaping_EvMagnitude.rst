EvMagnitude
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:LIMit:BPWShaping:EVMagnitude

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:MEValuation:LIMit:BPWShaping:EVMagnitude



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.MultiEval.Limit.BpwShaping.EvMagnitude.EvMagnitudeCls
	:members:
	:undoc-members:
	:noindex: