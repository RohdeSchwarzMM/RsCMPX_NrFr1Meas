Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:MEValuation:ACLR:CURRent
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:ACLR:CURRent
	single: CALCulate:NRSub:MEASurement<Instance>:MEValuation:ACLR:CURRent

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:MEValuation:ACLR:CURRent
	FETCh:NRSub:MEASurement<Instance>:MEValuation:ACLR:CURRent
	CALCulate:NRSub:MEASurement<Instance>:MEValuation:ACLR:CURRent



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.Aclr.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: