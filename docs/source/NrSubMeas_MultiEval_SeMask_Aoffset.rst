Aoffset
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:SEMask:AOFFset

.. code-block:: python

	FETCh:NRSub:MEASurement<Instance>:MEValuation:SEMask:AOFFset



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.SeMask.Aoffset.AoffsetCls
	:members:
	:undoc-members:
	:noindex: