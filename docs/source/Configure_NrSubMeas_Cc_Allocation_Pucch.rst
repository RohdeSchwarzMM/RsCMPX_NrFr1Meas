Pucch
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>[:CC<no>]:ALLocation<Allocation>:PUCCh

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>[:CC<no>]:ALLocation<Allocation>:PUCCh



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.Cc.Allocation.Pucch.PucchCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.nrSubMeas.cc.allocation.pucch.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_NrSubMeas_Cc_Allocation_Pucch_Dmrs.rst
	Configure_NrSubMeas_Cc_Allocation_Pucch_Ghopping.rst
	Configure_NrSubMeas_Cc_Allocation_Pucch_IcShift.rst
	Configure_NrSubMeas_Cc_Allocation_Pucch_IsfHopping.rst
	Configure_NrSubMeas_Cc_Allocation_Pucch_Occ.rst
	Configure_NrSubMeas_Cc_Allocation_Pucch_ShPrb.rst
	Configure_NrSubMeas_Cc_Allocation_Pucch_TdoIndex.rst