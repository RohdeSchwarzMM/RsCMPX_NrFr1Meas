Positiv
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:SEMask:MARGin:CURRent:POSitiv

.. code-block:: python

	FETCh:NRSub:MEASurement<Instance>:MEValuation:SEMask:MARGin:CURRent:POSitiv



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.SeMask.Margin.Current.Positiv.PositivCls
	:members:
	:undoc-members:
	:noindex: