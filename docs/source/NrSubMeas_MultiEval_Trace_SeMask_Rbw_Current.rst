Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:MEValuation:TRACe:SEMask:RBW<kHz>:CURRent
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:TRACe:SEMask:RBW<kHz>:CURRent

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:MEValuation:TRACe:SEMask:RBW<kHz>:CURRent
	FETCh:NRSub:MEASurement<Instance>:MEValuation:TRACe:SEMask:RBW<kHz>:CURRent



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.Trace.SeMask.Rbw.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: