DchType
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr6g>:ACLR:DCHType

.. code-block:: python

	FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr6g>:ACLR:DCHType



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.ListPy.Segment.Aclr.DchType.DchTypeCls
	:members:
	:undoc-members:
	:noindex: