Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:SRS:PDYNamics:MAXimum
	single: FETCh:NRSub:MEASurement<Instance>:SRS:PDYNamics:MAXimum
	single: CALCulate:NRSub:MEASurement<Instance>:SRS:PDYNamics:MAXimum

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:SRS:PDYNamics:MAXimum
	FETCh:NRSub:MEASurement<Instance>:SRS:PDYNamics:MAXimum
	CALCulate:NRSub:MEASurement<Instance>:SRS:PDYNamics:MAXimum



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.Srs.Pdynamics.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: