Alength
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:SEMask:ALENgth

.. code-block:: python

	FETCh:NRSub:MEASurement<Instance>:MEValuation:SEMask:ALENgth



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.SeMask.Alength.AlengthCls
	:members:
	:undoc-members:
	:noindex: