Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST:POWer:TXPower:MAXimum
	single: CALCulate:NRSub:MEASurement<Instance>:MEValuation:LIST:POWer:TXPower:MAXimum

.. code-block:: python

	FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST:POWer:TXPower:MAXimum
	CALCulate:NRSub:MEASurement<Instance>:MEValuation:LIST:POWer:TXPower:MAXimum



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.ListPy.Power.TxPower.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: