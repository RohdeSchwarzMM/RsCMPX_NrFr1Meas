PlcId
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:NETWork[:CC<no>]:PLCid

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:NETWork[:CC<no>]:PLCid



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.Network.Cc.PlcId.PlcIdCls
	:members:
	:undoc-members:
	:noindex: