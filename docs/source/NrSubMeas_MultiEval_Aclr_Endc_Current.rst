Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:MEValuation:ACLR:ENDC:CURRent
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:ACLR:ENDC:CURRent
	single: CALCulate:NRSub:MEASurement<Instance>:MEValuation:ACLR:ENDC:CURRent

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:MEValuation:ACLR:ENDC:CURRent
	FETCh:NRSub:MEASurement<Instance>:MEValuation:ACLR:ENDC:CURRent
	CALCulate:NRSub:MEASurement<Instance>:MEValuation:ACLR:ENDC:CURRent



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.Aclr.Endc.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: