NsubChannels
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>[:CC<no>]:PSSCh:NSUBchannels

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>[:CC<no>]:PSSCh:NSUBchannels



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.Cc.Pssch.NsubChannels.NsubChannelsCls
	:members:
	:undoc-members:
	:noindex: