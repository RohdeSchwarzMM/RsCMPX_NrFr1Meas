Limit
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:SRS:LIMit:FERRor

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:SRS:LIMit:FERRor



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.Srs.Limit.LimitCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.nrSubMeas.srs.limit.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_NrSubMeas_Srs_Limit_EvMagnitude.rst
	Configure_NrSubMeas_Srs_Limit_Merror.rst
	Configure_NrSubMeas_Srs_Limit_Pdynamics.rst
	Configure_NrSubMeas_Srs_Limit_Perror.rst
	Configure_NrSubMeas_Srs_Limit_Ptolerance.rst