Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:MEValuation:PMONitor:AVERage
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:PMONitor:AVERage

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:MEValuation:PMONitor:AVERage
	FETCh:NRSub:MEASurement<Instance>:MEValuation:PMONitor:AVERage



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.Pmonitor.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: