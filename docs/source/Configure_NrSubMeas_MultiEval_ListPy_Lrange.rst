Lrange
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:LIST:LRANge

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:MEValuation:LIST:LRANge



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.MultiEval.ListPy.Lrange.LrangeCls
	:members:
	:undoc-members:
	:noindex: