Dallocation
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr6g>:SEMask:DALLocation

.. code-block:: python

	FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr6g>:SEMask:DALLocation



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.ListPy.Segment.SeMask.Dallocation.DallocationCls
	:members:
	:undoc-members:
	:noindex: