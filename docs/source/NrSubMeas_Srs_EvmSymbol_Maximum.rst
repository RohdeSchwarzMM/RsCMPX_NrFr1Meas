Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:SRS:EVMSymbol:MAXimum
	single: FETCh:NRSub:MEASurement<Instance>:SRS:EVMSymbol:MAXimum

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:SRS:EVMSymbol:MAXimum
	FETCh:NRSub:MEASurement<Instance>:SRS:EVMSymbol:MAXimum



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.Srs.EvmSymbol.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: