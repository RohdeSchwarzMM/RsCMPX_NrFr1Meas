Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:PRACh:TRACe:PDYNamics:AVERage
	single: FETCh:NRSub:MEASurement<Instance>:PRACh:TRACe:PDYNamics:AVERage

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:PRACh:TRACe:PDYNamics:AVERage
	FETCh:NRSub:MEASurement<Instance>:PRACh:TRACe:PDYNamics:AVERage



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.Prach.Trace.Pdynamics.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: