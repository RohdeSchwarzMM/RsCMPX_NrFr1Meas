Tracking
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:MODulation:TRACking:PHASe
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:MODulation:TRACking:TIMing
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:MODulation:TRACking:LEVel

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:MEValuation:MODulation:TRACking:PHASe
	CONFigure:NRSub:MEASurement<Instance>:MEValuation:MODulation:TRACking:TIMing
	CONFigure:NRSub:MEASurement<Instance>:MEValuation:MODulation:TRACking:LEVel



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.MultiEval.Modulation.Tracking.TrackingCls
	:members:
	:undoc-members:
	:noindex: