TdoIndex
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>[:CC<no>]:ALLocation<Allocation>:PUCCh:TDOindex

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>[:CC<no>]:ALLocation<Allocation>:PUCCh:TDOindex



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.Cc.Allocation.Pucch.TdoIndex.TdoIndexCls
	:members:
	:undoc-members:
	:noindex: