Pdynamics
----------------------------------------





.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.Prach.Trace.Pdynamics.PdynamicsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrSubMeas.prach.trace.pdynamics.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrSubMeas_Prach_Trace_Pdynamics_Average.rst
	NrSubMeas_Prach_Trace_Pdynamics_Current.rst
	NrSubMeas_Prach_Trace_Pdynamics_Maximum.rst