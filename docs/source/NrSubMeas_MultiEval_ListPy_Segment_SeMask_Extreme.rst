Extreme
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr6g>:SEMask:EXTReme
	single: CALCulate:NRSub:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr6g>:SEMask:EXTReme

.. code-block:: python

	FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr6g>:SEMask:EXTReme
	CALCulate:NRSub:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr6g>:SEMask:EXTReme



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.ListPy.Segment.SeMask.Extreme.ExtremeCls
	:members:
	:undoc-members:
	:noindex: