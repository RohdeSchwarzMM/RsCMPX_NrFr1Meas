Maximum
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:TRACe:SEMask:AREA<area>:NEGative:MAXimum

.. code-block:: python

	FETCh:NRSub:MEASurement<Instance>:MEValuation:TRACe:SEMask:AREA<area>:NEGative:MAXimum



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.Trace.SeMask.Area.Negative.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: