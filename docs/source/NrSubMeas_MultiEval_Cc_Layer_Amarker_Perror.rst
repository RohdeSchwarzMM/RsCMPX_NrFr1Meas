Perror
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation[:CC<no>][:LAYer<layer>]:AMARker<Nr>:PERRor

.. code-block:: python

	FETCh:NRSub:MEASurement<Instance>:MEValuation[:CC<no>][:LAYer<layer>]:AMARker<Nr>:PERRor



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.Cc.Layer.Amarker.Perror.PerrorCls
	:members:
	:undoc-members:
	:noindex: