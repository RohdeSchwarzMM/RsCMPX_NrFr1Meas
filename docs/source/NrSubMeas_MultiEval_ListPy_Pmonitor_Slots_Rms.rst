Rms
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST:PMONitor:SLOTs:RMS

.. code-block:: python

	FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST:PMONitor:SLOTs:RMS



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.ListPy.Pmonitor.Slots.Rms.RmsCls
	:members:
	:undoc-members:
	:noindex: