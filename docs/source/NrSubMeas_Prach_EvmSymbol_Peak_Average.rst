Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:PRACh:EVMSymbol:PEAK:AVERage
	single: FETCh:NRSub:MEASurement<Instance>:PRACh:EVMSymbol:PEAK:AVERage

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:PRACh:EVMSymbol:PEAK:AVERage
	FETCh:NRSub:MEASurement<Instance>:PRACh:EVMSymbol:PEAK:AVERage



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.Prach.EvmSymbol.Peak.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: