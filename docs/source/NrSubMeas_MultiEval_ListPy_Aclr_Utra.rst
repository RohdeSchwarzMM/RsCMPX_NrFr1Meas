Utra<UtraChannel>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr2
	rc = driver.nrSubMeas.multiEval.listPy.aclr.utra.repcap_utraChannel_get()
	driver.nrSubMeas.multiEval.listPy.aclr.utra.repcap_utraChannel_set(repcap.UtraChannel.Nr1)





.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.ListPy.Aclr.Utra.UtraCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrSubMeas.multiEval.listPy.aclr.utra.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrSubMeas_MultiEval_ListPy_Aclr_Utra_Negativ.rst
	NrSubMeas_MultiEval_ListPy_Aclr_Utra_Positiv.rst