Cbgt
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:PRACh:LIMit:PDYNamics:TTOLerance:CBGT

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:PRACh:LIMit:PDYNamics:TTOLerance:CBGT



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.Prach.Limit.Pdynamics.Ttolerance.Cbgt.CbgtCls
	:members:
	:undoc-members:
	:noindex: