Phbpsk
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>[:CC<no>]:BWPart:PUCCh:PHBPsk

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>[:CC<no>]:BWPart:PUCCh:PHBPsk



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.Cc.BwPart.Pucch.Phbpsk.PhbpskCls
	:members:
	:undoc-members:
	:noindex: