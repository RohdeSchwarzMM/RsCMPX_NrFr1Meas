Modulation
----------------------------------------





.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.Srs.Modulation.ModulationCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrSubMeas.srs.modulation.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrSubMeas_Srs_Modulation_Average.rst
	NrSubMeas_Srs_Modulation_Current.rst
	NrSubMeas_Srs_Modulation_Extreme.rst
	NrSubMeas_Srs_Modulation_Nsymbol.rst
	NrSubMeas_Srs_Modulation_StandardDev.rst