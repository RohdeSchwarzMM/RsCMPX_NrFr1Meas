Cbandwidth<ChannelBw>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Bw5 .. Bw100
	rc = driver.configure.nrSubMeas.multiEval.modulation.ewLength.cbandwidth.repcap_channelBw_get()
	driver.configure.nrSubMeas.multiEval.modulation.ewLength.cbandwidth.repcap_channelBw_set(repcap.ChannelBw.Bw5)



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:MODulation:EWLength:CBANdwidth<bw>

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:MEValuation:MODulation:EWLength:CBANdwidth<bw>



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.MultiEval.Modulation.EwLength.Cbandwidth.CbandwidthCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.nrSubMeas.multiEval.modulation.ewLength.cbandwidth.clone()