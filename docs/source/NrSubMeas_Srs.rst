Srs
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: INITiate:NRSub:MEASurement<Instance>:SRS
	single: STOP:NRSub:MEASurement<Instance>:SRS
	single: ABORt:NRSub:MEASurement<Instance>:SRS

.. code-block:: python

	INITiate:NRSub:MEASurement<Instance>:SRS
	STOP:NRSub:MEASurement<Instance>:SRS
	ABORt:NRSub:MEASurement<Instance>:SRS



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.Srs.SrsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrSubMeas.srs.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrSubMeas_Srs_EvmSymbol.rst
	NrSubMeas_Srs_Modulation.rst
	NrSubMeas_Srs_Pdynamics.rst
	NrSubMeas_Srs_PvSymbol.rst
	NrSubMeas_Srs_State.rst
	NrSubMeas_Srs_Trace.rst