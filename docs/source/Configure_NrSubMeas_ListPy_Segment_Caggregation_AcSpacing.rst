AcSpacing
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:LIST:SEGMent<no>:CAGGregation:ACSPacing

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:LIST:SEGMent<no>:CAGGregation:ACSPacing



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.ListPy.Segment.Caggregation.AcSpacing.AcSpacingCls
	:members:
	:undoc-members:
	:noindex: