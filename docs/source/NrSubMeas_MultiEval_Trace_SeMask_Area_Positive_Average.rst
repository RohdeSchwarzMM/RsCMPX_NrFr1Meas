Average
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:TRACe:SEMask:AREA<area>:POSitive:AVERage

.. code-block:: python

	FETCh:NRSub:MEASurement<Instance>:MEValuation:TRACe:SEMask:AREA<area>:POSitive:AVERage



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.Trace.SeMask.Area.Positive.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: