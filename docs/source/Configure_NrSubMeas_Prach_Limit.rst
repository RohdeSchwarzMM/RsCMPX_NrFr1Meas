Limit
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:PRACh:LIMit:FERRor

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:PRACh:LIMit:FERRor



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.Prach.Limit.LimitCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.nrSubMeas.prach.limit.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_NrSubMeas_Prach_Limit_EvMagnitude.rst
	Configure_NrSubMeas_Prach_Limit_Merror.rst
	Configure_NrSubMeas_Prach_Limit_Pdynamics.rst
	Configure_NrSubMeas_Prach_Limit_Perror.rst