Iq
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NRSub:MEASurement<Instance>:PRACh:TRACe:IQ

.. code-block:: python

	FETCh:NRSub:MEASurement<Instance>:PRACh:TRACe:IQ



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.Prach.Trace.Iq.IqCls
	:members:
	:undoc-members:
	:noindex: