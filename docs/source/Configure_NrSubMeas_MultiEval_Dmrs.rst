Dmrs
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:DMRS:CONFigtype
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:DMRS:MAXLength
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:DMRS:APOSition
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:DMRS:LZERo

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:MEValuation:DMRS:CONFigtype
	CONFigure:NRSub:MEASurement<Instance>:MEValuation:DMRS:MAXLength
	CONFigure:NRSub:MEASurement<Instance>:MEValuation:DMRS:APOSition
	CONFigure:NRSub:MEASurement<Instance>:MEValuation:DMRS:LZERo



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.MultiEval.Dmrs.DmrsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.nrSubMeas.multiEval.dmrs.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_NrSubMeas_MultiEval_Dmrs_Sgeneration.rst