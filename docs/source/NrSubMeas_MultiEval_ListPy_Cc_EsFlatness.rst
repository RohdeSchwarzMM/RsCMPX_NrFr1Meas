EsFlatness
----------------------------------------





.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.ListPy.Cc.EsFlatness.EsFlatnessCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrSubMeas.multiEval.listPy.cc.esFlatness.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrSubMeas_MultiEval_ListPy_Cc_EsFlatness_Difference.rst
	NrSubMeas_MultiEval_ListPy_Cc_EsFlatness_Maxr.rst
	NrSubMeas_MultiEval_ListPy_Cc_EsFlatness_Minr.rst
	NrSubMeas_MultiEval_ListPy_Cc_EsFlatness_Ripple.rst
	NrSubMeas_MultiEval_ListPy_Cc_EsFlatness_ScIndex.rst