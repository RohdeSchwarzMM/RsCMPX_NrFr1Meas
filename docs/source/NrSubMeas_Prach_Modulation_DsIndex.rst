DsIndex
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NRSub:MEASurement<Instance>:PRACh:MODulation:DSINdex

.. code-block:: python

	FETCh:NRSub:MEASurement<Instance>:PRACh:MODulation:DSINdex



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.Prach.Modulation.DsIndex.DsIndexCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrSubMeas.prach.modulation.dsIndex.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrSubMeas_Prach_Modulation_DsIndex_Preamble.rst