StandardDev
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:SRS:MODulation:SDEViation
	single: FETCh:NRSub:MEASurement<Instance>:SRS:MODulation:SDEViation

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:SRS:MODulation:SDEViation
	FETCh:NRSub:MEASurement<Instance>:SRS:MODulation:SDEViation



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.Srs.Modulation.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: