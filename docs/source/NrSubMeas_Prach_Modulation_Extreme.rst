Extreme
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:PRACh:MODulation:EXTReme
	single: FETCh:NRSub:MEASurement<Instance>:PRACh:MODulation:EXTReme
	single: CALCulate:NRSub:MEASurement<Instance>:PRACh:MODulation:EXTReme

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:PRACh:MODulation:EXTReme
	FETCh:NRSub:MEASurement<Instance>:PRACh:MODulation:EXTReme
	CALCulate:NRSub:MEASurement<Instance>:PRACh:MODulation:EXTReme



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.Prach.Modulation.Extreme.ExtremeCls
	:members:
	:undoc-members:
	:noindex: