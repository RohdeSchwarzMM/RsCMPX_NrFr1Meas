Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:SRS:TRACe:PERRor:CURRent
	single: FETCh:NRSub:MEASurement<Instance>:SRS:TRACe:PERRor:CURRent

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:SRS:TRACe:PERRor:CURRent
	FETCh:NRSub:MEASurement<Instance>:SRS:TRACe:PERRor:CURRent



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.Srs.Trace.Perror.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: