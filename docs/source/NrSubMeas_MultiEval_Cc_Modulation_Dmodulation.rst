Dmodulation
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation[:CC<no>]:MODulation:DMODulation

.. code-block:: python

	FETCh:NRSub:MEASurement<Instance>:MEValuation[:CC<no>]:MODulation:DMODulation



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.Cc.Modulation.Dmodulation.DmodulationCls
	:members:
	:undoc-members:
	:noindex: