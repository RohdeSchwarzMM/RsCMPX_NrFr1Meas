Cc<CarrierComponentOne>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr1
	rc = driver.configure.nrSubMeas.network.cc.repcap_carrierComponentOne_get()
	driver.configure.nrSubMeas.network.cc.repcap_carrierComponentOne_set(repcap.CarrierComponentOne.Nr1)





.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.Network.Cc.CcCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.nrSubMeas.network.cc.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_NrSubMeas_Network_Cc_Cbandwidth.rst
	Configure_NrSubMeas_Network_Cc_PlcId.rst