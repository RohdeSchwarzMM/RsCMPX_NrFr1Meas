TaPosition
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>[:CC<no>]:TAPosition

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>[:CC<no>]:TAPosition



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.Cc.TaPosition.TaPositionCls
	:members:
	:undoc-members:
	:noindex: