MultiEval
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: INITiate:NRSub:MEASurement<Instance>:MEValuation
	single: STOP:NRSub:MEASurement<Instance>:MEValuation
	single: ABORt:NRSub:MEASurement<Instance>:MEValuation

.. code-block:: python

	INITiate:NRSub:MEASurement<Instance>:MEValuation
	STOP:NRSub:MEASurement<Instance>:MEValuation
	ABORt:NRSub:MEASurement<Instance>:MEValuation



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.MultiEvalCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrSubMeas.multiEval.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrSubMeas_MultiEval_Aclr.rst
	NrSubMeas_MultiEval_Amarker.rst
	NrSubMeas_MultiEval_Cc.rst
	NrSubMeas_MultiEval_Dmarker.rst
	NrSubMeas_MultiEval_Layer.rst
	NrSubMeas_MultiEval_ListPy.rst
	NrSubMeas_MultiEval_Pmonitor.rst
	NrSubMeas_MultiEval_ReferenceMarker.rst
	NrSubMeas_MultiEval_SeMask.rst
	NrSubMeas_MultiEval_State.rst
	NrSubMeas_MultiEval_Trace.rst
	NrSubMeas_MultiEval_TxPower.rst
	NrSubMeas_MultiEval_VfThroughput.rst