Tpc
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: INITiate:NRSub:MEASurement<Instance>:TPC
	single: STOP:NRSub:MEASurement<Instance>:TPC
	single: ABORt:NRSub:MEASurement<Instance>:TPC

.. code-block:: python

	INITiate:NRSub:MEASurement<Instance>:TPC
	STOP:NRSub:MEASurement<Instance>:TPC
	ABORt:NRSub:MEASurement<Instance>:TPC



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.Tpc.TpcCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrSubMeas.tpc.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrSubMeas_Tpc_Psteps.rst
	NrSubMeas_Tpc_State.rst
	NrSubMeas_Tpc_Trace.rst