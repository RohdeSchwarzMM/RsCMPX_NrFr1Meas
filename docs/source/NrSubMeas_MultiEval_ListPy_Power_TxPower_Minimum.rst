Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST:POWer:TXPower:MINimum
	single: CALCulate:NRSub:MEASurement<Instance>:MEValuation:LIST:POWer:TXPower:MINimum

.. code-block:: python

	FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST:POWer:TXPower:MINimum
	CALCulate:NRSub:MEASurement<Instance>:MEValuation:LIST:POWer:TXPower:MINimum



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.ListPy.Power.TxPower.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: