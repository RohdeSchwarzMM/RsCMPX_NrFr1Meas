Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:MEValuation:TXPower:CURRent
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:TXPower:CURRent

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:MEValuation:TXPower:CURRent
	FETCh:NRSub:MEASurement<Instance>:MEValuation:TXPower:CURRent



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.TxPower.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: