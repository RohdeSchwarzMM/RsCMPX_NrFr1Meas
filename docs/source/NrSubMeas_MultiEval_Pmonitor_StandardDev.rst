StandardDev
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:MEValuation:PMONitor:SDEViation
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:PMONitor:SDEViation

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:MEValuation:PMONitor:SDEViation
	FETCh:NRSub:MEASurement<Instance>:MEValuation:PMONitor:SDEViation



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.Pmonitor.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: