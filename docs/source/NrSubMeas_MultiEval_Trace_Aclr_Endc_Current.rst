Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:MEValuation:TRACe:ACLR:ENDC:CURRent
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:TRACe:ACLR:ENDC:CURRent

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:MEValuation:TRACe:ACLR:ENDC:CURRent
	FETCh:NRSub:MEASurement<Instance>:MEValuation:TRACe:ACLR:ENDC:CURRent



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.Trace.Aclr.Endc.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: