ActLimit
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:LIMit:SEMask:ACTLimit:ENDC
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:LIMit:SEMask:ACTLimit:CAGGregation

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:MEValuation:LIMit:SEMask:ACTLimit:ENDC
	CONFigure:NRSub:MEASurement<Instance>:MEValuation:LIMit:SEMask:ACTLimit:CAGGregation



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.MultiEval.Limit.SeMask.ActLimit.ActLimitCls
	:members:
	:undoc-members:
	:noindex: