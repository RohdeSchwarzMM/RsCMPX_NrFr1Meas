Peak
----------------------------------------





.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.Prach.EvmSymbol.Peak.PeakCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrSubMeas.prach.evmSymbol.peak.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrSubMeas_Prach_EvmSymbol_Peak_Average.rst
	NrSubMeas_Prach_EvmSymbol_Peak_Current.rst
	NrSubMeas_Prach_EvmSymbol_Peak_Maximum.rst