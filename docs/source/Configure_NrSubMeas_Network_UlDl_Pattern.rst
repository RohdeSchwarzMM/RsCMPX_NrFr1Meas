Pattern
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:NETWork:ULDL:PATTern

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:NETWork:ULDL:PATTern



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.Network.UlDl.Pattern.PatternCls
	:members:
	:undoc-members:
	:noindex: