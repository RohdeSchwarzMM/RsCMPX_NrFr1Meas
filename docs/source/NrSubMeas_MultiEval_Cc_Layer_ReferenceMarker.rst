ReferenceMarker
----------------------------------------





.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.Cc.Layer.ReferenceMarker.ReferenceMarkerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrSubMeas.multiEval.cc.layer.referenceMarker.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrSubMeas_MultiEval_Cc_Layer_ReferenceMarker_EvMagnitude.rst
	NrSubMeas_MultiEval_Cc_Layer_ReferenceMarker_Merror.rst
	NrSubMeas_MultiEval_Cc_Layer_ReferenceMarker_Perror.rst