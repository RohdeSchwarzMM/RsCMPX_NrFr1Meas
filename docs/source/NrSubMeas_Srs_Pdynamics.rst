Pdynamics
----------------------------------------





.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.Srs.Pdynamics.PdynamicsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrSubMeas.srs.pdynamics.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrSubMeas_Srs_Pdynamics_Average.rst
	NrSubMeas_Srs_Pdynamics_Current.rst
	NrSubMeas_Srs_Pdynamics_Maximum.rst
	NrSubMeas_Srs_Pdynamics_Minimum.rst
	NrSubMeas_Srs_Pdynamics_StandardDev.rst