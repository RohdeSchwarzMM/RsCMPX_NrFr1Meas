Perror
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:LIMit:QAM<order>:PERRor

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:MEValuation:LIMit:QAM<order>:PERRor



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.MultiEval.Limit.Qam.Perror.PerrorCls
	:members:
	:undoc-members:
	:noindex: