Trace
----------------------------------------





.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.Srs.Trace.TraceCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrSubMeas.srs.trace.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrSubMeas_Srs_Trace_Evm.rst
	NrSubMeas_Srs_Trace_Iq.rst
	NrSubMeas_Srs_Trace_Merror.rst
	NrSubMeas_Srs_Trace_Pdynamics.rst
	NrSubMeas_Srs_Trace_Perror.rst
	NrSubMeas_Srs_Trace_PvSymbol.rst