Obw
----------------------------------------





.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.ListPy.SeMask.Obw.ObwCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrSubMeas.multiEval.listPy.seMask.obw.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrSubMeas_MultiEval_ListPy_SeMask_Obw_Average.rst
	NrSubMeas_MultiEval_ListPy_SeMask_Obw_Current.rst
	NrSubMeas_MultiEval_ListPy_SeMask_Obw_Extreme.rst
	NrSubMeas_MultiEval_ListPy_SeMask_Obw_StandardDev.rst