Pucch
----------------------------------------





.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.Cc.BwPart.Pucch.PucchCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.nrSubMeas.cc.bwPart.pucch.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_NrSubMeas_Cc_BwPart_Pucch_AdMrs.rst
	Configure_NrSubMeas_Cc_BwPart_Pucch_Phbpsk.rst