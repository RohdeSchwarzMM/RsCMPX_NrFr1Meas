Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:SRS:TRACe:EVM:AVERage
	single: FETCh:NRSub:MEASurement<Instance>:SRS:TRACe:EVM:AVERage

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:SRS:TRACe:EVM:AVERage
	FETCh:NRSub:MEASurement<Instance>:SRS:TRACe:EVM:AVERage



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.Srs.Trace.Evm.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: