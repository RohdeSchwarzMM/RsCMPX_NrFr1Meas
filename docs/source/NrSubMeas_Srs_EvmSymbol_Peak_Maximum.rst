Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:SRS:EVMSymbol:PEAK:MAXimum
	single: FETCh:NRSub:MEASurement<Instance>:SRS:EVMSymbol:PEAK:MAXimum

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:SRS:EVMSymbol:PEAK:MAXimum
	FETCh:NRSub:MEASurement<Instance>:SRS:EVMSymbol:PEAK:MAXimum



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.Srs.EvmSymbol.Peak.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: