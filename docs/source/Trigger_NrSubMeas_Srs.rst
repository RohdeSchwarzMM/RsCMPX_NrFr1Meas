Srs
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: TRIGger:NRSub:MEASurement<Instance>:SRS:THReshold
	single: TRIGger:NRSub:MEASurement<Instance>:SRS:SLOPe
	single: TRIGger:NRSub:MEASurement<Instance>:SRS:TOUT
	single: TRIGger:NRSub:MEASurement<Instance>:SRS:MGAP

.. code-block:: python

	TRIGger:NRSub:MEASurement<Instance>:SRS:THReshold
	TRIGger:NRSub:MEASurement<Instance>:SRS:SLOPe
	TRIGger:NRSub:MEASurement<Instance>:SRS:TOUT
	TRIGger:NRSub:MEASurement<Instance>:SRS:MGAP



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Trigger.NrSubMeas.Srs.SrsCls
	:members:
	:undoc-members:
	:noindex: