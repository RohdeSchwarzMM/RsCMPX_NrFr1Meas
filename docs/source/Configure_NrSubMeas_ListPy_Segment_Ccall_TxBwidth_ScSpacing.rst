ScSpacing
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:LIST:SEGMent<no>:CCALl:TXBWidth:SCSPacing

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:LIST:SEGMent<no>:CCALl:TXBWidth:SCSPacing



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.ListPy.Segment.Ccall.TxBwidth.ScSpacing.ScSpacingCls
	:members:
	:undoc-members:
	:noindex: