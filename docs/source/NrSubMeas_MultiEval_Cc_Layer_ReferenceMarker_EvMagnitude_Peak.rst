Peak
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation[:CC<no>][:LAYer<layer>]:REFMarker:EVMagnitude:PEAK

.. code-block:: python

	FETCh:NRSub:MEASurement<Instance>:MEValuation[:CC<no>][:LAYer<layer>]:REFMarker:EVMagnitude:PEAK



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.Cc.Layer.ReferenceMarker.EvMagnitude.Peak.PeakCls
	:members:
	:undoc-members:
	:noindex: