StandardDev
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST:POWer:TXPower:SDEViation

.. code-block:: python

	FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST:POWer:TXPower:SDEViation



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.ListPy.Power.TxPower.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: