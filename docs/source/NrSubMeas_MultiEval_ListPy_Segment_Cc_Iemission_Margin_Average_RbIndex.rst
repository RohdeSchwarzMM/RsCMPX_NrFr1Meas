RbIndex
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>[:CC<Carrier>]:IEMission:MARGin:AVERage:RBINdex

.. code-block:: python

	FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>[:CC<Carrier>]:IEMission:MARGin:AVERage:RBINdex



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.ListPy.Segment.Cc.Iemission.Margin.Average.RbIndex.RbIndexCls
	:members:
	:undoc-members:
	:noindex: