Segment<SEGMent>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr512
	rc = driver.nrSubMeas.multiEval.listPy.segment.repcap_sEGMent_get()
	driver.nrSubMeas.multiEval.listPy.segment.repcap_sEGMent_set(repcap.SEGMent.Nr1)





.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.ListPy.Segment.SegmentCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrSubMeas.multiEval.listPy.segment.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrSubMeas_MultiEval_ListPy_Segment_Aclr.rst
	NrSubMeas_MultiEval_ListPy_Segment_Cc.rst
	NrSubMeas_MultiEval_ListPy_Segment_Pmonitor.rst
	NrSubMeas_MultiEval_ListPy_Segment_Power.rst
	NrSubMeas_MultiEval_ListPy_Segment_SeMask.rst