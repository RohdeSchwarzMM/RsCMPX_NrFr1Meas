Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST:ACLR:NR:POSitiv:AVERage
	single: CALCulate:NRSub:MEASurement<Instance>:MEValuation:LIST:ACLR:NR:POSitiv:AVERage

.. code-block:: python

	FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST:ACLR:NR:POSitiv:AVERage
	CALCulate:NRSub:MEASurement<Instance>:MEValuation:LIST:ACLR:NR:POSitiv:AVERage



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.ListPy.Aclr.Nr.Positiv.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: