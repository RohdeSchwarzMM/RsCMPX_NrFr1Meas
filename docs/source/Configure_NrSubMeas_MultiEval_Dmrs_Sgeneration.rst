Sgeneration
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:DMRS:SGENeration

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:MEValuation:DMRS:SGENeration



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.MultiEval.Dmrs.Sgeneration.SgenerationCls
	:members:
	:undoc-members:
	:noindex: