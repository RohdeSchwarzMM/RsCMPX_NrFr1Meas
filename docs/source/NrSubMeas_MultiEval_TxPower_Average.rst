Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:MEValuation:TXPower:AVERage
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:TXPower:AVERage

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:MEValuation:TXPower:AVERage
	FETCh:NRSub:MEASurement<Instance>:MEValuation:TXPower:AVERage



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.TxPower.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: