TxPower
----------------------------------------





.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.TxPower.TxPowerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrSubMeas.multiEval.txPower.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrSubMeas_MultiEval_TxPower_Average.rst
	NrSubMeas_MultiEval_TxPower_Current.rst
	NrSubMeas_MultiEval_TxPower_Maximum.rst
	NrSubMeas_MultiEval_TxPower_Minimum.rst
	NrSubMeas_MultiEval_TxPower_StandardDev.rst