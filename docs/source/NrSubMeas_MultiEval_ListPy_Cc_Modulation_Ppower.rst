Ppower
----------------------------------------





.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.ListPy.Cc.Modulation.Ppower.PpowerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrSubMeas.multiEval.listPy.cc.modulation.ppower.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrSubMeas_MultiEval_ListPy_Cc_Modulation_Ppower_Average.rst
	NrSubMeas_MultiEval_ListPy_Cc_Modulation_Ppower_Current.rst
	NrSubMeas_MultiEval_ListPy_Cc_Modulation_Ppower_Maximum.rst
	NrSubMeas_MultiEval_ListPy_Cc_Modulation_Ppower_Minimum.rst
	NrSubMeas_MultiEval_ListPy_Cc_Modulation_Ppower_StandardDev.rst