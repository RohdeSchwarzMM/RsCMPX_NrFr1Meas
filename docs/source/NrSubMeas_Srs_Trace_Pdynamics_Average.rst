Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:SRS:TRACe:PDYNamics:AVERage
	single: FETCh:NRSub:MEASurement<Instance>:SRS:TRACe:PDYNamics:AVERage

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:SRS:TRACe:PDYNamics:AVERage
	FETCh:NRSub:MEASurement<Instance>:SRS:TRACe:PDYNamics:AVERage



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.Srs.Trace.Pdynamics.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: