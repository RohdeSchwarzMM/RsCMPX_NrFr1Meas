Utra<UtraChannel>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr2
	rc = driver.configure.nrSubMeas.multiEval.limit.aclr.utra.repcap_utraChannel_get()
	driver.configure.nrSubMeas.multiEval.limit.aclr.utra.repcap_utraChannel_set(repcap.UtraChannel.Nr1)





.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.MultiEval.Limit.Aclr.Utra.UtraCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.nrSubMeas.multiEval.limit.aclr.utra.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_NrSubMeas_MultiEval_Limit_Aclr_Utra_Cbandwidth.rst