Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST:POWer:TXPower:AVERage
	single: CALCulate:NRSub:MEASurement<Instance>:MEValuation:LIST:POWer:TXPower:AVERage

.. code-block:: python

	FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST:POWer:TXPower:AVERage
	CALCulate:NRSub:MEASurement<Instance>:MEValuation:LIST:POWer:TXPower:AVERage



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.ListPy.Power.TxPower.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: