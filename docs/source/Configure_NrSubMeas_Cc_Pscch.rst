Pscch
----------------------------------------





.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.Cc.Pscch.PscchCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.nrSubMeas.cc.pscch.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_NrSubMeas_Cc_Pscch_Did.rst
	Configure_NrSubMeas_Cc_Pscch_Nbits.rst
	Configure_NrSubMeas_Cc_Pscch_Nrb.rst
	Configure_NrSubMeas_Cc_Pscch_Nsymbols.rst