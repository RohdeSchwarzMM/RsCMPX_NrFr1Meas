Setup
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:LIST:SEGMent<no>:SETup

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:LIST:SEGMent<no>:SETup



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.ListPy.Segment.Setup.SetupCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.nrSubMeas.listPy.segment.setup.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_NrSubMeas_ListPy_Segment_Setup_Additional.rst