EonPower<EonPower>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr2
	rc = driver.configure.nrSubMeas.prach.limit.pdynamics.eonPower.repcap_eonPower_get()
	driver.configure.nrSubMeas.prach.limit.pdynamics.eonPower.repcap_eonPower_set(repcap.EonPower.Nr1)



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:PRACh:LIMit:PDYNamics:EONPower<e>

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:PRACh:LIMit:PDYNamics:EONPower<e>



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.Prach.Limit.Pdynamics.EonPower.EonPowerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.nrSubMeas.prach.limit.pdynamics.eonPower.clone()