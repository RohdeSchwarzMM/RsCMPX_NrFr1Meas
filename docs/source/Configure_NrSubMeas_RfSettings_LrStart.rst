LrStart
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:RFSettings:LRSTart

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:RFSettings:LRSTart



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.RfSettings.LrStart.LrStartCls
	:members:
	:undoc-members:
	:noindex: