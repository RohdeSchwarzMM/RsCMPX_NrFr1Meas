Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:PRACh:PDYNamics:CURRent
	single: FETCh:NRSub:MEASurement<Instance>:PRACh:PDYNamics:CURRent
	single: CALCulate:NRSub:MEASurement<Instance>:PRACh:PDYNamics:CURRent

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:PRACh:PDYNamics:CURRent
	FETCh:NRSub:MEASurement<Instance>:PRACh:PDYNamics:CURRent
	CALCulate:NRSub:MEASurement<Instance>:PRACh:PDYNamics:CURRent



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.Prach.Pdynamics.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: