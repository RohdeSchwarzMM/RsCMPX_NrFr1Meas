Layer<Layer>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr2
	rc = driver.nrSubMeas.multiEval.trace.layer.repcap_layer_get()
	driver.nrSubMeas.multiEval.trace.layer.repcap_layer_set(repcap.Layer.Nr1)





.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.Trace.Layer.LayerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrSubMeas.multiEval.trace.layer.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrSubMeas_MultiEval_Trace_Layer_Pdynamics.rst