Rmapping
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:SRS:RMAPping

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:SRS:RMAPping



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.Srs.Rmapping.RmappingCls
	:members:
	:undoc-members:
	:noindex: