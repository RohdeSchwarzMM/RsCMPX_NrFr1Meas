ObwLimit
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:LIMit:SEMask:STANdard:OBWLimit:ENDC
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:LIMit:SEMask:STANdard:OBWLimit:CAGGregation

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:MEValuation:LIMit:SEMask:STANdard:OBWLimit:ENDC
	CONFigure:NRSub:MEASurement<Instance>:MEValuation:LIMit:SEMask:STANdard:OBWLimit:CAGGregation



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.MultiEval.Limit.SeMask.Standard.ObwLimit.ObwLimitCls
	:members:
	:undoc-members:
	:noindex: