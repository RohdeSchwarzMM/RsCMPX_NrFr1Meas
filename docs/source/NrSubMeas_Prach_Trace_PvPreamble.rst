PvPreamble
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:PRACh:TRACe:PVPReamble
	single: FETCh:NRSub:MEASurement<Instance>:PRACh:TRACe:PVPReamble

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:PRACh:TRACe:PVPReamble
	FETCh:NRSub:MEASurement<Instance>:PRACh:TRACe:PVPReamble



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.Prach.Trace.PvPreamble.PvPreambleCls
	:members:
	:undoc-members:
	:noindex: