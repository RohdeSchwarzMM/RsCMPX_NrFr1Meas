EonPower<EonPowerScs>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr15 .. Nr60
	rc = driver.configure.nrSubMeas.srs.limit.pdynamics.eonPower.repcap_eonPowerScs_get()
	driver.configure.nrSubMeas.srs.limit.pdynamics.eonPower.repcap_eonPowerScs_set(repcap.EonPowerScs.Nr15)



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:SRS:LIMit:PDYNamics:EONPower<scs>

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:SRS:LIMit:PDYNamics:EONPower<scs>



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.Srs.Limit.Pdynamics.EonPower.EonPowerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.nrSubMeas.srs.limit.pdynamics.eonPower.clone()