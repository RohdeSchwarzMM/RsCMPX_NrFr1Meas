Preamble<Preamble>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr64
	rc = driver.nrSubMeas.prach.modulation.scorrelation.preamble.repcap_preamble_get()
	driver.nrSubMeas.prach.modulation.scorrelation.preamble.repcap_preamble_set(repcap.Preamble.Nr1)



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NRSub:MEASurement<Instance>:PRACh:MODulation:SCORrelation:PREamble<Number>

.. code-block:: python

	FETCh:NRSub:MEASurement<Instance>:PRACh:MODulation:SCORrelation:PREamble<Number>



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.Prach.Modulation.Scorrelation.Preamble.PreambleCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrSubMeas.prach.modulation.scorrelation.preamble.clone()