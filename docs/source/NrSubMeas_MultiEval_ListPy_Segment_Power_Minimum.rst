Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr6g>:POWer:MINimum
	single: CALCulate:NRSub:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr6g>:POWer:MINimum

.. code-block:: python

	FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr6g>:POWer:MINimum
	CALCulate:NRSub:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr6g>:POWer:MINimum



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.ListPy.Segment.Power.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: