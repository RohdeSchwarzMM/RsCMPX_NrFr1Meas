Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST:SEMask:TXPower:MAXimum
	single: CALCulate:NRSub:MEASurement<Instance>:MEValuation:LIST:SEMask:TXPower:MAXimum

.. code-block:: python

	FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST:SEMask:TXPower:MAXimum
	CALCulate:NRSub:MEASurement<Instance>:MEValuation:LIST:SEMask:TXPower:MAXimum



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.ListPy.SeMask.TxPower.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: