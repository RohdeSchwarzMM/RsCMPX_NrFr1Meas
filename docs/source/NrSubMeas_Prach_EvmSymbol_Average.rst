Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:PRACh:EVMSymbol:AVERage
	single: FETCh:NRSub:MEASurement<Instance>:PRACh:EVMSymbol:AVERage
	single: CALCulate:NRSub:MEASurement<Instance>:PRACh:EVMSymbol:AVERage

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:PRACh:EVMSymbol:AVERage
	FETCh:NRSub:MEASurement<Instance>:PRACh:EVMSymbol:AVERage
	CALCulate:NRSub:MEASurement<Instance>:PRACh:EVMSymbol:AVERage



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.Prach.EvmSymbol.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: