Merror
----------------------------------------





.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.Cc.Layer.Merror.MerrorCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrSubMeas.multiEval.cc.layer.merror.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrSubMeas_MultiEval_Cc_Layer_Merror_Average.rst
	NrSubMeas_MultiEval_Cc_Layer_Merror_Current.rst
	NrSubMeas_MultiEval_Cc_Layer_Merror_Maximum.rst