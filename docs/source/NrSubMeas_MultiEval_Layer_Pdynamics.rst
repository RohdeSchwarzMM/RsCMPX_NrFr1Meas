Pdynamics
----------------------------------------





.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.Layer.Pdynamics.PdynamicsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrSubMeas.multiEval.layer.pdynamics.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrSubMeas_MultiEval_Layer_Pdynamics_Average.rst
	NrSubMeas_MultiEval_Layer_Pdynamics_Current.rst
	NrSubMeas_MultiEval_Layer_Pdynamics_Maximum.rst
	NrSubMeas_MultiEval_Layer_Pdynamics_Minimum.rst
	NrSubMeas_MultiEval_Layer_Pdynamics_StandardDev.rst