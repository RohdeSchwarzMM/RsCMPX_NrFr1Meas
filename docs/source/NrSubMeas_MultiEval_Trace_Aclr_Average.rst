Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:MEValuation:TRACe:ACLR:AVERage
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:TRACe:ACLR:AVERage

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:MEValuation:TRACe:ACLR:AVERage
	FETCh:NRSub:MEASurement<Instance>:MEValuation:TRACe:ACLR:AVERage



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.Trace.Aclr.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: