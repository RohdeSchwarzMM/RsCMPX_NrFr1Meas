Invoke
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:BWConfig:INVoke

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:BWConfig:INVoke



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.BwConfig.Invoke.InvokeCls
	:members:
	:undoc-members:
	:noindex: