Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:PRACh:TRACe:EVM:CURRent
	single: FETCh:NRSub:MEASurement<Instance>:PRACh:TRACe:EVM:CURRent

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:PRACh:TRACe:EVM:CURRent
	FETCh:NRSub:MEASurement<Instance>:PRACh:TRACe:EVM:CURRent



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.Prach.Trace.Evm.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: