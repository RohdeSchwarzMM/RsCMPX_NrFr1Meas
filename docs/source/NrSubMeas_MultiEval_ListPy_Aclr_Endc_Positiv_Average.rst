Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST:ACLR:ENDC:POSitiv:AVERage
	single: CALCulate:NRSub:MEASurement<Instance>:MEValuation:LIST:ACLR:ENDC:POSitiv:AVERage

.. code-block:: python

	FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST:ACLR:ENDC:POSitiv:AVERage
	CALCulate:NRSub:MEASurement<Instance>:MEValuation:LIST:ACLR:ENDC:POSitiv:AVERage



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.ListPy.Aclr.Endc.Positiv.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: