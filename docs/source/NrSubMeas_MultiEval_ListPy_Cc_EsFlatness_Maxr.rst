Maxr<MaxRange>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr2
	rc = driver.nrSubMeas.multiEval.listPy.cc.esFlatness.maxr.repcap_maxRange_get()
	driver.nrSubMeas.multiEval.listPy.cc.esFlatness.maxr.repcap_maxRange_set(repcap.MaxRange.Nr1)





.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.ListPy.Cc.EsFlatness.Maxr.MaxrCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrSubMeas.multiEval.listPy.cc.esFlatness.maxr.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrSubMeas_MultiEval_ListPy_Cc_EsFlatness_Maxr_Average.rst
	NrSubMeas_MultiEval_ListPy_Cc_EsFlatness_Maxr_Current.rst
	NrSubMeas_MultiEval_ListPy_Cc_EsFlatness_Maxr_Extreme.rst
	NrSubMeas_MultiEval_ListPy_Cc_EsFlatness_Maxr_StandardDev.rst