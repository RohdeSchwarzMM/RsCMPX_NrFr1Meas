DchType
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:SEMask:DCHType

.. code-block:: python

	FETCh:NRSub:MEASurement<Instance>:MEValuation:SEMask:DCHType



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.SeMask.DchType.DchTypeCls
	:members:
	:undoc-members:
	:noindex: