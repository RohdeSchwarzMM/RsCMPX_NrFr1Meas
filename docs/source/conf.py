# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import sys
sys.path.insert(0, os.path.abspath('../..'))


# -- Project information -----------------------------------------------------

project = 'RsCMPX_NrFr1Meas'
copyright = '© Rohde & Schwarz 2023'
author = 'Rohde & Schwarz'
master_doc = 'index'

# The full version, including alpha/beta/rc tags
release = '5.0.100.19'


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
	'sphinx.ext.autodoc',
	'sphinx.ext.viewcode',
	'sphinx.ext.napoleon',
	'sphinx_copybutton',
	'recommonmark'
]

html_theme_options = {
    'navigation_depth': 7
}

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = []
autoclass_content = 'both'
pygments_style = 'sphinx'

# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'sphinx_rtd_theme'

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
# so a file named "default.css" will overwrite the builtin "default.css".
add_module_names = False

# noinspection PyUnusedLocal
def skip_member(app, what, name, obj, skip, opts):
	# we can document otherwise excluded entities here by returning False
	# or skip otherwise included entities by returning True
	if what == 'class':
		# here, what is always 'class', because the autodocs are only run on classes
		if skip is True:
			return True
		if isinstance(obj, property):
			# override for internal classes
			# :special-members: enable_properties is set for classes e.g.: Utilities, Events, ScpiLogger...
			if 'special-members' in opts and 'enable_properties' in opts['special-members']:
				print (f'{name}: overriding props disable') 
				return False
			return True
		if name.startswith('repcap_'):
			return True
		if name == 'clone':
			return True
		if name == 'driver_options':
			# constant value in the root class
			return True
			
		return False
	return skip

# noinspection PyUnusedLocal
def signature_changes(app, what, name, obj, options, signature, return_annotation):
	if what == 'class':
		if name == 'RsCMPX_NrFr1Meas.RsCMPX_NrFr1Meas':
			return signature, return_annotation
		# Remove complicated types for classes
		return '',''
	if what == 'method':
		if return_annotation and return_annotation.startswith('RsCMPX_NrFr1Meas.Implementations'):
			# return args is structure, just get its name, not the whole path
			chain = return_annotation.split('.')
			return signature, chain[-1]
		if return_annotation and 'from_existing_session' in name:
			return_annotation = 'RsCMPX_NrFr1Meas'
	return signature, return_annotation

# noinspection PyUnusedLocal
def docstring_changes(app, what, name, obj, options, lines):
	"""Change the 'Snippet:' text to codeblock"""
	scpi = None
	snippet = None
	# Extract SCPI if exists
	if len(lines) > 0 and lines[0].startswith('SCPI:'):
		scpi = lines[0].strip()
	else:
		return
	
	if len(lines) > 1 and lines[1].strip() == '':
		del lines[1]
		
	if len(lines) > 1 and lines[1].startswith('Snippet with structure:'):
		# Snippet where a structure is defined. The whole snippet is intedted and ends with empty line:
		# Snippet with structure:
		# structure = new RsSmw.source.bb.power_meas.LimitStruct()
		# structure.LimitLow = -100.0
		# structure.LimitHigh = +10.0
		# smw.source.bb.power_meas.set_limit(structure)
		# Delete the 'Snippet with structure:' and change the lines up to the empty line to code-block
		lines[0] = '.. code-block:: python' # Overwrite the SCPI: with the code-block directive
		lines[1] = ''
		lines.insert(2, f'    # {scpi}')
		i = 2
		while True:
			i += 1
			lines[i] = lines[i].strip()
			if lines[i] == '':
				del lines[i]
				i -= 1
				continue
			lines[i] = '    ' + lines[i]
			if lines[i].strip().startswith('driver.'): # Last snippet line detection
				break
	
	if len(lines) > 1 and lines[1].startswith('Snippet:'):
		snippet = lines[1].replace('Snippet:', '').strip()
		if scpi and snippet:
			lines[0] = '.. code-block:: python'
			lines[1] = ''
			lines.insert(2, f'    # {scpi}')
			lines.insert(3, f'    {snippet}')
			lines.insert(4, '')

def setup(app):
	app.connect('autodoc-skip-member', skip_member)
	app.connect('autodoc-process-signature', signature_changes)
	app.connect('autodoc-process-docstring', docstring_changes)
