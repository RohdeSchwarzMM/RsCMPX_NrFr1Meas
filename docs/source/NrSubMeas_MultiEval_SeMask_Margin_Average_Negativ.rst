Negativ
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:SEMask:MARGin:AVERage:NEGativ

.. code-block:: python

	FETCh:NRSub:MEASurement<Instance>:MEValuation:SEMask:MARGin:AVERage:NEGativ



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.SeMask.Margin.Average.Negativ.NegativCls
	:members:
	:undoc-members:
	:noindex: