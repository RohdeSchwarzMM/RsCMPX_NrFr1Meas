Merror
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:LIMit:QPSK:MERRor

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:MEValuation:LIMit:QPSK:MERRor



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.MultiEval.Limit.Qpsk.Merror.MerrorCls
	:members:
	:undoc-members:
	:noindex: