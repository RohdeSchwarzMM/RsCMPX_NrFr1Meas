Result
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:SRS:RESult:MODulation
	single: CONFigure:NRSub:MEASurement<Instance>:SRS:RESult:PDYNamics
	single: CONFigure:NRSub:MEASurement<Instance>:SRS:RESult:PVSYmbol

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:SRS:RESult:MODulation
	CONFigure:NRSub:MEASurement<Instance>:SRS:RESult:PDYNamics
	CONFigure:NRSub:MEASurement<Instance>:SRS:RESult:PVSYmbol



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.Srs.Result.ResultCls
	:members:
	:undoc-members:
	:noindex: