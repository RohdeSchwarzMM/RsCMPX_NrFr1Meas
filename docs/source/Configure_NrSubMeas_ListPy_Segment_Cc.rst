Cc<CarrierComponent>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr2
	rc = driver.configure.nrSubMeas.listPy.segment.cc.repcap_carrierComponent_get()
	driver.configure.nrSubMeas.listPy.segment.cc.repcap_carrierComponent_set(repcap.CarrierComponent.Nr1)





.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.ListPy.Segment.Cc.CcCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.nrSubMeas.listPy.segment.cc.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_NrSubMeas_ListPy_Segment_Cc_Allocation.rst
	Configure_NrSubMeas_ListPy_Segment_Cc_BwPart.rst
	Configure_NrSubMeas_ListPy_Segment_Cc_Cbandwidth.rst
	Configure_NrSubMeas_ListPy_Segment_Cc_Frequency.rst
	Configure_NrSubMeas_ListPy_Segment_Cc_Nallocations.rst
	Configure_NrSubMeas_ListPy_Segment_Cc_PlcId.rst
	Configure_NrSubMeas_ListPy_Segment_Cc_TaPosition.rst
	Configure_NrSubMeas_ListPy_Segment_Cc_TxBwidth.rst