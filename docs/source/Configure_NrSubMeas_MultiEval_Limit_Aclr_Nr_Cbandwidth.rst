Cbandwidth<ChannelBw>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Bw5 .. Bw100
	rc = driver.configure.nrSubMeas.multiEval.limit.aclr.nr.cbandwidth.repcap_channelBw_get()
	driver.configure.nrSubMeas.multiEval.limit.aclr.nr.cbandwidth.repcap_channelBw_set(repcap.ChannelBw.Bw5)



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:LIMit:ACLR:NR:CBANdwidth<bw>

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:MEValuation:LIMit:ACLR:NR:CBANdwidth<bw>



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.MultiEval.Limit.Aclr.Nr.Cbandwidth.CbandwidthCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.nrSubMeas.multiEval.limit.aclr.nr.cbandwidth.clone()