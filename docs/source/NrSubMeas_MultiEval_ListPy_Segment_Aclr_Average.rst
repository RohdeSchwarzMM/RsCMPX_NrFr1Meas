Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr6g>:ACLR:AVERage
	single: CALCulate:NRSub:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr6g>:ACLR:AVERage

.. code-block:: python

	FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr6g>:ACLR:AVERage
	CALCulate:NRSub:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr6g>:ACLR:AVERage



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.ListPy.Segment.Aclr.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: