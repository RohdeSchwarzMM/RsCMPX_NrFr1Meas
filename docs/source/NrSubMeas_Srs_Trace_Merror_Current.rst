Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:SRS:TRACe:MERRor:CURRent
	single: FETCh:NRSub:MEASurement<Instance>:SRS:TRACe:MERRor:CURRent

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:SRS:TRACe:MERRor:CURRent
	FETCh:NRSub:MEASurement<Instance>:SRS:TRACe:MERRor:CURRent



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.Srs.Trace.Merror.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: