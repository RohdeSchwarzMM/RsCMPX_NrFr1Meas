Pformat<PFormat>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr13
	rc = driver.configure.nrSubMeas.prach.modulation.ewLength.pformat.repcap_pFormat_get()
	driver.configure.nrSubMeas.prach.modulation.ewLength.pformat.repcap_pFormat_set(repcap.PFormat.Nr1)



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:PRACh:MODulation:EWLength:PFORmat<no>

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:PRACh:MODulation:EWLength:PFORmat<no>



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.Prach.Modulation.EwLength.Pformat.PformatCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.nrSubMeas.prach.modulation.ewLength.pformat.clone()