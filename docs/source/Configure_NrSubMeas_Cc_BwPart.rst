BwPart
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>[:CC<no>]:BWPart

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>[:CC<no>]:BWPart



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.Cc.BwPart.BwPartCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.nrSubMeas.cc.bwPart.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_NrSubMeas_Cc_BwPart_Pucch.rst
	Configure_NrSubMeas_Cc_BwPart_Pusch.rst