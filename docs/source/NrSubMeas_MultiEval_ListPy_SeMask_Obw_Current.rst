Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST:SEMask:OBW:CURRent
	single: CALCulate:NRSub:MEASurement<Instance>:MEValuation:LIST:SEMask:OBW:CURRent

.. code-block:: python

	FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST:SEMask:OBW:CURRent
	CALCulate:NRSub:MEASurement<Instance>:MEValuation:LIST:SEMask:OBW:CURRent



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.ListPy.SeMask.Obw.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: