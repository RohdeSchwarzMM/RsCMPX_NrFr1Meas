Pdynamics
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:AMARker<No>:PDYNamics

.. code-block:: python

	FETCh:NRSub:MEASurement<Instance>:MEValuation:AMARker<No>:PDYNamics



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.Amarker.Pdynamics.PdynamicsCls
	:members:
	:undoc-members:
	:noindex: