Perror
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:LIMit:PHBPsk:PERRor

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:MEValuation:LIMit:PHBPsk:PERRor



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.MultiEval.Limit.Phbpsk.Perror.PerrorCls
	:members:
	:undoc-members:
	:noindex: