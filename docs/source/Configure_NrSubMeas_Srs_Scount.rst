Scount
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:SRS:SCOunt:MODulation
	single: CONFigure:NRSub:MEASurement<Instance>:SRS:SCOunt:PDYNamics

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:SRS:SCOunt:MODulation
	CONFigure:NRSub:MEASurement<Instance>:SRS:SCOunt:PDYNamics



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.Srs.Scount.ScountCls
	:members:
	:undoc-members:
	:noindex: