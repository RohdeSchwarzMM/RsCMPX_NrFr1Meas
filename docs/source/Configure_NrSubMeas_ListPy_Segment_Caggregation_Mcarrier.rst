Mcarrier
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:LIST:SEGMent<no>:CAGGregation:MCARrier

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:LIST:SEGMent<no>:CAGGregation:MCARrier



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.ListPy.Segment.Caggregation.Mcarrier.McarrierCls
	:members:
	:undoc-members:
	:noindex: