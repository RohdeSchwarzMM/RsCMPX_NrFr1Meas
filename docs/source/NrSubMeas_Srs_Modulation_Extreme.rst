Extreme
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:SRS:MODulation:EXTReme
	single: FETCh:NRSub:MEASurement<Instance>:SRS:MODulation:EXTReme
	single: CALCulate:NRSub:MEASurement<Instance>:SRS:MODulation:EXTReme

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:SRS:MODulation:EXTReme
	FETCh:NRSub:MEASurement<Instance>:SRS:MODulation:EXTReme
	CALCulate:NRSub:MEASurement<Instance>:SRS:MODulation:EXTReme



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.Srs.Modulation.Extreme.ExtremeCls
	:members:
	:undoc-members:
	:noindex: