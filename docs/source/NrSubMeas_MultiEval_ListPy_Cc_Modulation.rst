Modulation
----------------------------------------





.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.ListPy.Cc.Modulation.ModulationCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrSubMeas.multiEval.listPy.cc.modulation.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrSubMeas_MultiEval_ListPy_Cc_Modulation_Dallocation.rst
	NrSubMeas_MultiEval_ListPy_Cc_Modulation_DchType.rst
	NrSubMeas_MultiEval_ListPy_Cc_Modulation_Dmodulation.rst
	NrSubMeas_MultiEval_ListPy_Cc_Modulation_Evm.rst
	NrSubMeas_MultiEval_ListPy_Cc_Modulation_FreqError.rst
	NrSubMeas_MultiEval_ListPy_Cc_Modulation_IqOffset.rst
	NrSubMeas_MultiEval_ListPy_Cc_Modulation_Merror.rst
	NrSubMeas_MultiEval_ListPy_Cc_Modulation_Perror.rst
	NrSubMeas_MultiEval_ListPy_Cc_Modulation_Ppower.rst
	NrSubMeas_MultiEval_ListPy_Cc_Modulation_Psd.rst
	NrSubMeas_MultiEval_ListPy_Cc_Modulation_Terror.rst
	NrSubMeas_MultiEval_ListPy_Cc_Modulation_Tpower.rst