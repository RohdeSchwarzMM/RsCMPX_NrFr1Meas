TxBwidth
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:NETWork:CCALl:TXBWidth:SCSPacing

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:NETWork:CCALl:TXBWidth:SCSPacing



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.Network.Ccall.TxBwidth.TxBwidthCls
	:members:
	:undoc-members:
	:noindex: