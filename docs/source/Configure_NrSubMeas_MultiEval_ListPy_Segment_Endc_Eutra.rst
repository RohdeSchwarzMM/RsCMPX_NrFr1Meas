Eutra
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:LIST:SEGMent<no>:ENDC:EUTRa

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:MEValuation:LIST:SEGMent<no>:ENDC:EUTRa



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.MultiEval.ListPy.Segment.Endc.Eutra.EutraCls
	:members:
	:undoc-members:
	:noindex: