Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:PRACh:EVMSymbol:CURRent
	single: FETCh:NRSub:MEASurement<Instance>:PRACh:EVMSymbol:CURRent
	single: CALCulate:NRSub:MEASurement<Instance>:PRACh:EVMSymbol:CURRent

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:PRACh:EVMSymbol:CURRent
	FETCh:NRSub:MEASurement<Instance>:PRACh:EVMSymbol:CURRent
	CALCulate:NRSub:MEASurement<Instance>:PRACh:EVMSymbol:CURRent



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.Prach.EvmSymbol.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: