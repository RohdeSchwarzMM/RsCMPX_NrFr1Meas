Prach
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: INITiate:NRSub:MEASurement<Instance>:PRACh
	single: STOP:NRSub:MEASurement<Instance>:PRACh
	single: ABORt:NRSub:MEASurement<Instance>:PRACh

.. code-block:: python

	INITiate:NRSub:MEASurement<Instance>:PRACh
	STOP:NRSub:MEASurement<Instance>:PRACh
	ABORt:NRSub:MEASurement<Instance>:PRACh



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.Prach.PrachCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrSubMeas.prach.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrSubMeas_Prach_EvmSymbol.rst
	NrSubMeas_Prach_Modulation.rst
	NrSubMeas_Prach_Pdynamics.rst
	NrSubMeas_Prach_State.rst
	NrSubMeas_Prach_Trace.rst