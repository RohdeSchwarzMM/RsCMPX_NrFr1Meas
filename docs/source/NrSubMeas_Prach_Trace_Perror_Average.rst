Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:PRACh:TRACe:PERRor:AVERage
	single: FETCh:NRSub:MEASurement<Instance>:PRACh:TRACe:PERRor:AVERage

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:PRACh:TRACe:PERRor:AVERage
	FETCh:NRSub:MEASurement<Instance>:PRACh:TRACe:PERRor:AVERage



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.Prach.Trace.Perror.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: