State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NRSub:MEASurement<Instance>:TPC:STATe

.. code-block:: python

	FETCh:NRSub:MEASurement<Instance>:TPC:STATe



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.Tpc.State.StateCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrSubMeas.tpc.state.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrSubMeas_Tpc_State_All.rst