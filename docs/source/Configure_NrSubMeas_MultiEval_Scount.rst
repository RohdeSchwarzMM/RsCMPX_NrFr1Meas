Scount
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:SCOunt:MODulation
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:SCOunt:POWer
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:SCOunt:PDYNamics
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:SCOunt:TXPower

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:MEValuation:SCOunt:MODulation
	CONFigure:NRSub:MEASurement<Instance>:MEValuation:SCOunt:POWer
	CONFigure:NRSub:MEASurement<Instance>:MEValuation:SCOunt:PDYNamics
	CONFigure:NRSub:MEASurement<Instance>:MEValuation:SCOunt:TXPower



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.MultiEval.Scount.ScountCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.nrSubMeas.multiEval.scount.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_NrSubMeas_MultiEval_Scount_Spectrum.rst