Prach
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:NETWork:PRACh:PCINdex

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:NETWork:PRACh:PCINdex



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.Network.Prach.PrachCls
	:members:
	:undoc-members:
	:noindex: