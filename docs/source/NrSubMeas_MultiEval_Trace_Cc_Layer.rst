Layer<Layer>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr2
	rc = driver.nrSubMeas.multiEval.trace.cc.layer.repcap_layer_get()
	driver.nrSubMeas.multiEval.trace.cc.layer.repcap_layer_set(repcap.Layer.Nr1)





.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.Trace.Cc.Layer.LayerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrSubMeas.multiEval.trace.cc.layer.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrSubMeas_MultiEval_Trace_Cc_Layer_EsFlatness.rst
	NrSubMeas_MultiEval_Trace_Cc_Layer_Evmc.rst
	NrSubMeas_MultiEval_Trace_Cc_Layer_EvmSymbol.rst
	NrSubMeas_MultiEval_Trace_Cc_Layer_Iemissions.rst
	NrSubMeas_MultiEval_Trace_Cc_Layer_Iq.rst