Nallocations
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:LIST:SEGMent<no>[:CC<cc>]:NALLocations

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:LIST:SEGMent<no>[:CC<cc>]:NALLocations



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.ListPy.Segment.Cc.Nallocations.NallocationsCls
	:members:
	:undoc-members:
	:noindex: