Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST:ACLR:ENDC:CURRent
	single: CALCulate:NRSub:MEASurement<Instance>:MEValuation:LIST:ACLR:ENDC:CURRent

.. code-block:: python

	FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST:ACLR:ENDC:CURRent
	CALCulate:NRSub:MEASurement<Instance>:MEValuation:LIST:ACLR:ENDC:CURRent



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.ListPy.Aclr.Endc.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: