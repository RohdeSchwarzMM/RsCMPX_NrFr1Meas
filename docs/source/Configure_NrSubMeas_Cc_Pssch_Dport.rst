Dport
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>[:CC<no>]:PSSCh:DPORt

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>[:CC<no>]:PSSCh:DPORt



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.Cc.Pssch.Dport.DportCls
	:members:
	:undoc-members:
	:noindex: