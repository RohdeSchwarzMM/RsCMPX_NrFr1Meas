Length
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr6g>:PMONitor:ARRay:LENGth

.. code-block:: python

	FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr6g>:PMONitor:ARRay:LENGth



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.ListPy.Segment.Pmonitor.Array.Length.LengthCls
	:members:
	:undoc-members:
	:noindex: