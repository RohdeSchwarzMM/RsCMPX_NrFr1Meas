Extreme
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST:SEMask:OBW:EXTReme
	single: CALCulate:NRSub:MEASurement<Instance>:MEValuation:LIST:SEMask:OBW:EXTReme

.. code-block:: python

	FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST:SEMask:OBW:EXTReme
	CALCulate:NRSub:MEASurement<Instance>:MEValuation:LIST:SEMask:OBW:EXTReme



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.ListPy.SeMask.Obw.Extreme.ExtremeCls
	:members:
	:undoc-members:
	:noindex: