Endc
----------------------------------------





.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.ListPy.Aclr.Endc.EndcCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrSubMeas.multiEval.listPy.aclr.endc.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrSubMeas_MultiEval_ListPy_Aclr_Endc_Average.rst
	NrSubMeas_MultiEval_ListPy_Aclr_Endc_Current.rst
	NrSubMeas_MultiEval_ListPy_Aclr_Endc_Negativ.rst
	NrSubMeas_MultiEval_ListPy_Aclr_Endc_Positiv.rst