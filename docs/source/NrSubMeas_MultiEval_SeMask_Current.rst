Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:MEValuation:SEMask:CURRent
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:SEMask:CURRent
	single: CALCulate:NRSub:MEASurement<Instance>:MEValuation:SEMask:CURRent

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:MEValuation:SEMask:CURRent
	FETCh:NRSub:MEASurement<Instance>:MEValuation:SEMask:CURRent
	CALCulate:NRSub:MEASurement<Instance>:MEValuation:SEMask:CURRent



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.SeMask.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: