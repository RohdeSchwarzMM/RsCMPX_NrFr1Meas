StandardDev
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:MEValuation[:LAYer<layer>]:PDYNamics:SDEViation
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation[:LAYer<layer>]:PDYNamics:SDEViation

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:MEValuation[:LAYer<layer>]:PDYNamics:SDEViation
	FETCh:NRSub:MEASurement<Instance>:MEValuation[:LAYer<layer>]:PDYNamics:SDEViation



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.Layer.Pdynamics.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: