Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:TPC:TRACe:UEPower:CURRent
	single: FETCh:NRSub:MEASurement<Instance>:TPC:TRACe:UEPower:CURRent

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:TPC:TRACe:UEPower:CURRent
	FETCh:NRSub:MEASurement<Instance>:TPC:TRACe:UEPower:CURRent



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.Tpc.Trace.UePower.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: