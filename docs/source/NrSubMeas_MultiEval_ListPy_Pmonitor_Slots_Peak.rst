Peak
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST:PMONitor:SLOTs:PEAK

.. code-block:: python

	FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST:PMONitor:SLOTs:PEAK



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.ListPy.Pmonitor.Slots.Peak.PeakCls
	:members:
	:undoc-members:
	:noindex: