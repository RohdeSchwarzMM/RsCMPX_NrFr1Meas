Pscch
----------------------------------------





.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.Cc.Layer.Modulation.Pscch.PscchCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrSubMeas.multiEval.cc.layer.modulation.pscch.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrSubMeas_MultiEval_Cc_Layer_Modulation_Pscch_Average.rst
	NrSubMeas_MultiEval_Cc_Layer_Modulation_Pscch_Current.rst
	NrSubMeas_MultiEval_Cc_Layer_Modulation_Pscch_Extreme.rst
	NrSubMeas_MultiEval_Cc_Layer_Modulation_Pscch_StandardDev.rst