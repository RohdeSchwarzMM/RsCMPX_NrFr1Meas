Tpc
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: TRIGger:NRSub:MEASurement<Instance>:TPC:THReshold
	single: TRIGger:NRSub:MEASurement<Instance>:TPC:SLOPe
	single: TRIGger:NRSub:MEASurement<Instance>:TPC:TOUT
	single: TRIGger:NRSub:MEASurement<Instance>:TPC:MGAP

.. code-block:: python

	TRIGger:NRSub:MEASurement<Instance>:TPC:THReshold
	TRIGger:NRSub:MEASurement<Instance>:TPC:SLOPe
	TRIGger:NRSub:MEASurement<Instance>:TPC:TOUT
	TRIGger:NRSub:MEASurement<Instance>:TPC:MGAP



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Trigger.NrSubMeas.Tpc.TpcCls
	:members:
	:undoc-members:
	:noindex: