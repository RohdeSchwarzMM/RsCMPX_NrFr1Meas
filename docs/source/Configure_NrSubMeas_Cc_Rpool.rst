Rpool
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>[:CC<no>]:RPOol

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>[:CC<no>]:RPOol



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.Cc.Rpool.RpoolCls
	:members:
	:undoc-members:
	:noindex: