Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:PRACh:MODulation:AVERage
	single: FETCh:NRSub:MEASurement<Instance>:PRACh:MODulation:AVERage
	single: CALCulate:NRSub:MEASurement<Instance>:PRACh:MODulation:AVERage

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:PRACh:MODulation:AVERage
	FETCh:NRSub:MEASurement<Instance>:PRACh:MODulation:AVERage
	CALCulate:NRSub:MEASurement<Instance>:PRACh:MODulation:AVERage



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.Prach.Modulation.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: