EvmSymbol
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:RESult:EVMagnitude:EVMSymbol

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:MEValuation:RESult:EVMagnitude:EVMSymbol



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.MultiEval.Result.EvMagnitude.EvmSymbol.EvmSymbolCls
	:members:
	:undoc-members:
	:noindex: