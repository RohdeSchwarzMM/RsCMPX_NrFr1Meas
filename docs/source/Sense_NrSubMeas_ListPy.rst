ListPy
----------------------------------------





.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Sense.NrSubMeas.ListPy.ListPyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.nrSubMeas.listPy.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_NrSubMeas_ListPy_Segment.rst