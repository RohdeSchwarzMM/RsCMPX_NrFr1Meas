ListPy
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: TRIGger:NRSub:MEASurement<Instance>:LIST:MODE
	single: TRIGger:NRSub:MEASurement<Instance>:LIST:NBANdwidth

.. code-block:: python

	TRIGger:NRSub:MEASurement<Instance>:LIST:MODE
	TRIGger:NRSub:MEASurement<Instance>:LIST:NBANdwidth



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Trigger.NrSubMeas.ListPy.ListPyCls
	:members:
	:undoc-members:
	:noindex: