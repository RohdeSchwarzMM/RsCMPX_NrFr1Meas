Pmonitor
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:REFMarker:PMONitor

.. code-block:: python

	FETCh:NRSub:MEASurement<Instance>:MEValuation:REFMarker:PMONitor



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.ReferenceMarker.Pmonitor.PmonitorCls
	:members:
	:undoc-members:
	:noindex: