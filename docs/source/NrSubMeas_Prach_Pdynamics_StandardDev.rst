StandardDev
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:PRACh:PDYNamics:SDEViation
	single: FETCh:NRSub:MEASurement<Instance>:PRACh:PDYNamics:SDEViation

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:PRACh:PDYNamics:SDEViation
	FETCh:NRSub:MEASurement<Instance>:PRACh:PDYNamics:SDEViation



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.Prach.Pdynamics.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: