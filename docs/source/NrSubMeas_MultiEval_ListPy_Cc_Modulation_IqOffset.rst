IqOffset
----------------------------------------





.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.ListPy.Cc.Modulation.IqOffset.IqOffsetCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrSubMeas.multiEval.listPy.cc.modulation.iqOffset.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrSubMeas_MultiEval_ListPy_Cc_Modulation_IqOffset_Average.rst
	NrSubMeas_MultiEval_ListPy_Cc_Modulation_IqOffset_Current.rst
	NrSubMeas_MultiEval_ListPy_Cc_Modulation_IqOffset_Extreme.rst
	NrSubMeas_MultiEval_ListPy_Cc_Modulation_IqOffset_StandardDev.rst