Pdynamics
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:PDYNamics:TMASk
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:PDYNamics:HDMode

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:MEValuation:PDYNamics:TMASk
	CONFigure:NRSub:MEASurement<Instance>:MEValuation:PDYNamics:HDMode



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.MultiEval.Pdynamics.PdynamicsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.nrSubMeas.multiEval.pdynamics.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_NrSubMeas_MultiEval_Pdynamics_AeoPower.rst