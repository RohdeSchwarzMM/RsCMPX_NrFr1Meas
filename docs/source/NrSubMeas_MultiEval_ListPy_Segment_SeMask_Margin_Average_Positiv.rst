Positiv
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr6g>:SEMask:MARGin:AVERage:POSitiv

.. code-block:: python

	FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr6g>:SEMask:MARGin:AVERage:POSitiv



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.ListPy.Segment.SeMask.Margin.Average.Positiv.PositivCls
	:members:
	:undoc-members:
	:noindex: