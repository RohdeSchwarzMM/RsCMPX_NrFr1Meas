StandardDev
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr6g>:SEMask:SDEViation

.. code-block:: python

	FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr6g>:SEMask:SDEViation



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.ListPy.Segment.SeMask.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: