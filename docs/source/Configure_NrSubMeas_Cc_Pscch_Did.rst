Did
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>[:CC<no>]:PSCCh:DID

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>[:CC<no>]:PSCCh:DID



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.Cc.Pscch.Did.DidCls
	:members:
	:undoc-members:
	:noindex: