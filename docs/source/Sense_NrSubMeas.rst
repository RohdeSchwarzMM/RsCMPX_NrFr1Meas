NrSubMeas
----------------------------------------





.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Sense.NrSubMeas.NrSubMeasCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.nrSubMeas.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_NrSubMeas_ListPy.rst