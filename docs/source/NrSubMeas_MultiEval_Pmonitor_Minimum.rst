Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:MEValuation:PMONitor:MINimum
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:PMONitor:MINimum

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:MEValuation:PMONitor:MINimum
	FETCh:NRSub:MEASurement<Instance>:MEValuation:PMONitor:MINimum



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.Pmonitor.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: