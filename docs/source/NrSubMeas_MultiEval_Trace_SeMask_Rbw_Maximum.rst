Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:MEValuation:TRACe:SEMask:RBW<kHz>:MAXimum
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:TRACe:SEMask:RBW<kHz>:MAXimum

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:MEValuation:TRACe:SEMask:RBW<kHz>:MAXimum
	FETCh:NRSub:MEASurement<Instance>:MEValuation:TRACe:SEMask:RBW<kHz>:MAXimum



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.Trace.SeMask.Rbw.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: