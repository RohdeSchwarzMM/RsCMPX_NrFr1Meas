Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:MEValuation:ACLR:AVERage
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:ACLR:AVERage
	single: CALCulate:NRSub:MEASurement<Instance>:MEValuation:ACLR:AVERage

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:MEValuation:ACLR:AVERage
	FETCh:NRSub:MEASurement<Instance>:MEValuation:ACLR:AVERage
	CALCulate:NRSub:MEASurement<Instance>:MEValuation:ACLR:AVERage



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.Aclr.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: