Nrb
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>[:CC<no>]:PSCCh:NRB

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>[:CC<no>]:PSCCh:NRB



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.Cc.Pscch.Nrb.NrbCls
	:members:
	:undoc-members:
	:noindex: