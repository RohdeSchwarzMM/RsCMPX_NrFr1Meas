Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:MEValuation[:LAYer<layer>]:PDYNamics:AVERage
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation[:LAYer<layer>]:PDYNamics:AVERage
	single: CALCulate:NRSub:MEASurement<Instance>:MEValuation[:LAYer<layer>]:PDYNamics:AVERage

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:MEValuation[:LAYer<layer>]:PDYNamics:AVERage
	FETCh:NRSub:MEASurement<Instance>:MEValuation[:LAYer<layer>]:PDYNamics:AVERage
	CALCulate:NRSub:MEASurement<Instance>:MEValuation[:LAYer<layer>]:PDYNamics:AVERage



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.Layer.Pdynamics.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: