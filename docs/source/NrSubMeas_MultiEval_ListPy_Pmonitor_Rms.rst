Rms
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST:PMONitor:RMS

.. code-block:: python

	FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST:PMONitor:RMS



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.ListPy.Pmonitor.Rms.RmsCls
	:members:
	:undoc-members:
	:noindex: