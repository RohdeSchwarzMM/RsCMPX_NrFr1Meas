Pdynamics
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:REFMarker:PDYNamics

.. code-block:: python

	FETCh:NRSub:MEASurement<Instance>:MEValuation:REFMarker:PDYNamics



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.ReferenceMarker.Pdynamics.PdynamicsCls
	:members:
	:undoc-members:
	:noindex: