SeMask
----------------------------------------





.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.Trace.SeMask.SeMaskCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrSubMeas.multiEval.trace.seMask.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrSubMeas_MultiEval_Trace_SeMask_Area.rst
	NrSubMeas_MultiEval_Trace_SeMask_Rbw.rst