Eutra
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:ENDC:EUTRa

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:MEValuation:ENDC:EUTRa



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.MultiEval.Endc.Eutra.EutraCls
	:members:
	:undoc-members:
	:noindex: