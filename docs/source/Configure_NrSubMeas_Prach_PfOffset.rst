PfOffset
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:PRACh:PFOFfset:AUTO
	single: CONFigure:NRSub:MEASurement<Instance>:PRACh:PFOFfset

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:PRACh:PFOFfset:AUTO
	CONFigure:NRSub:MEASurement<Instance>:PRACh:PFOFfset



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.Prach.PfOffset.PfOffsetCls
	:members:
	:undoc-members:
	:noindex: