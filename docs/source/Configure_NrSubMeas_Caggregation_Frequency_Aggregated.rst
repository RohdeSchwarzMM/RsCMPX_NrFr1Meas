Aggregated
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:CAGGregation:FREQuency:AGGRegated:LOW
	single: CONFigure:NRSub:MEASurement<Instance>:CAGGregation:FREQuency:AGGRegated:CENTer
	single: CONFigure:NRSub:MEASurement<Instance>:CAGGregation:FREQuency:AGGRegated:HIGH

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:CAGGregation:FREQuency:AGGRegated:LOW
	CONFigure:NRSub:MEASurement<Instance>:CAGGregation:FREQuency:AGGRegated:CENTer
	CONFigure:NRSub:MEASurement<Instance>:CAGGregation:FREQuency:AGGRegated:HIGH



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.Caggregation.Frequency.Aggregated.AggregatedCls
	:members:
	:undoc-members:
	:noindex: