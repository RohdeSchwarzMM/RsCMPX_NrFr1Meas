Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:SRS:EVMSymbol:PEAK:AVERage
	single: FETCh:NRSub:MEASurement<Instance>:SRS:EVMSymbol:PEAK:AVERage

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:SRS:EVMSymbol:PEAK:AVERage
	FETCh:NRSub:MEASurement<Instance>:SRS:EVMSymbol:PEAK:AVERage



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.Srs.EvmSymbol.Peak.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: