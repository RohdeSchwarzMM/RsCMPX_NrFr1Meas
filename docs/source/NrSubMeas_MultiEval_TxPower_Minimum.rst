Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:MEValuation:TXPower:MINimum
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:TXPower:MINimum

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:MEValuation:TXPower:MINimum
	FETCh:NRSub:MEASurement<Instance>:MEValuation:TXPower:MINimum



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.TxPower.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: