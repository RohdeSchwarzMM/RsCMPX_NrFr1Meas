Pusch
----------------------------------------





.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.ListPy.Segment.Cc.BwPart.Pusch.PuschCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.nrSubMeas.listPy.segment.cc.bwPart.pusch.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_NrSubMeas_ListPy_Segment_Cc_BwPart_Pusch_DftPrecoding.rst
	Configure_NrSubMeas_ListPy_Segment_Cc_BwPart_Pusch_Dmta.rst
	Configure_NrSubMeas_ListPy_Segment_Cc_BwPart_Pusch_Dmtb.rst