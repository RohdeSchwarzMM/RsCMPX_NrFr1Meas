Modulation
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:MODulation:TDLoffset
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:MODulation:DAReceiver

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:MEValuation:MODulation:TDLoffset
	CONFigure:NRSub:MEASurement<Instance>:MEValuation:MODulation:DAReceiver



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.MultiEval.Modulation.ModulationCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.nrSubMeas.multiEval.modulation.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_NrSubMeas_MultiEval_Modulation_EePeriods.rst
	Configure_NrSubMeas_MultiEval_Modulation_EwLength.rst
	Configure_NrSubMeas_MultiEval_Modulation_Tracking.rst