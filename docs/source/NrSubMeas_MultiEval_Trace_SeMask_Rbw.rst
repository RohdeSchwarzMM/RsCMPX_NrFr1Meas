Rbw<Rbw>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Bw1 .. Bw1000
	rc = driver.nrSubMeas.multiEval.trace.seMask.rbw.repcap_rbw_get()
	driver.nrSubMeas.multiEval.trace.seMask.rbw.repcap_rbw_set(repcap.Rbw.Bw1)





.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.Trace.SeMask.Rbw.RbwCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrSubMeas.multiEval.trace.seMask.rbw.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrSubMeas_MultiEval_Trace_SeMask_Rbw_Average.rst
	NrSubMeas_MultiEval_Trace_SeMask_Rbw_Current.rst
	NrSubMeas_MultiEval_Trace_SeMask_Rbw_Maximum.rst