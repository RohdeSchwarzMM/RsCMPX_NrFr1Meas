DftPrecoding
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>[:CC<no>]:BWPart:PUSCh:DFTPrecoding

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>[:CC<no>]:BWPart:PUSCh:DFTPrecoding



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.Cc.BwPart.Pusch.DftPrecoding.DftPrecodingCls
	:members:
	:undoc-members:
	:noindex: