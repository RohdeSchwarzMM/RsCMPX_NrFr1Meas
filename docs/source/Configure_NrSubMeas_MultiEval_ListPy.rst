ListPy
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:LIST:OSINdex
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:LIST:CMODe
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:LIST:NCONnections
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:LIST

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:MEValuation:LIST:OSINdex
	CONFigure:NRSub:MEASurement<Instance>:MEValuation:LIST:CMODe
	CONFigure:NRSub:MEASurement<Instance>:MEValuation:LIST:NCONnections
	CONFigure:NRSub:MEASurement<Instance>:MEValuation:LIST



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.MultiEval.ListPy.ListPyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.nrSubMeas.multiEval.listPy.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_NrSubMeas_MultiEval_ListPy_Lrange.rst
	Configure_NrSubMeas_MultiEval_ListPy_Segment.rst
	Configure_NrSubMeas_MultiEval_ListPy_SingleCmw.rst