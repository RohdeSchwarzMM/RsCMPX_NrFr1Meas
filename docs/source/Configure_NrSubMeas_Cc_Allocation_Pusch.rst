Pusch
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>[:CC<no>]:ALLocation<Allocation>:PUSCh

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>[:CC<no>]:ALLocation<Allocation>:PUSCh



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.Cc.Allocation.Pusch.PuschCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.nrSubMeas.cc.allocation.pusch.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_NrSubMeas_Cc_Allocation_Pusch_Acontiguous.rst
	Configure_NrSubMeas_Cc_Allocation_Pusch_Additional.rst
	Configure_NrSubMeas_Cc_Allocation_Pusch_Mscheme.rst
	Configure_NrSubMeas_Cc_Allocation_Pusch_Nlayers.rst
	Configure_NrSubMeas_Cc_Allocation_Pusch_Rb.rst
	Configure_NrSubMeas_Cc_Allocation_Pusch_Sgeneration.rst
	Configure_NrSubMeas_Cc_Allocation_Pusch_Tdomain.rst