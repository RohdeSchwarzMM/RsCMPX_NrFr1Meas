EsFlatness
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:LIMit:BPWShaping:ESFLatness

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:MEValuation:LIMit:BPWShaping:ESFLatness



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.MultiEval.Limit.BpwShaping.EsFlatness.EsFlatnessCls
	:members:
	:undoc-members:
	:noindex: