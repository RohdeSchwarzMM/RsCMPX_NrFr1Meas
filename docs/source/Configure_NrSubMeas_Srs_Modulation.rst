Modulation
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:SRS:MODulation:SSYMbol
	single: CONFigure:NRSub:MEASurement<Instance>:SRS:MODulation:EWPosition

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:SRS:MODulation:SSYMbol
	CONFigure:NRSub:MEASurement<Instance>:SRS:MODulation:EWPosition



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.Srs.Modulation.ModulationCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.nrSubMeas.srs.modulation.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_NrSubMeas_Srs_Modulation_EwLength.rst