MultiEval
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: TRIGger:NRSub:MEASurement<Instance>:MEValuation:THReshold
	single: TRIGger:NRSub:MEASurement<Instance>:MEValuation:SLOPe
	single: TRIGger:NRSub:MEASurement<Instance>:MEValuation:DELay
	single: TRIGger:NRSub:MEASurement<Instance>:MEValuation:TOUT
	single: TRIGger:NRSub:MEASurement<Instance>:MEValuation:MGAP
	single: TRIGger:NRSub:MEASurement<Instance>:MEValuation:SMODe
	single: TRIGger:NRSub:MEASurement<Instance>:MEValuation:FSYNc
	single: TRIGger:NRSub:MEASurement<Instance>:MEValuation:SOURce

.. code-block:: python

	TRIGger:NRSub:MEASurement<Instance>:MEValuation:THReshold
	TRIGger:NRSub:MEASurement<Instance>:MEValuation:SLOPe
	TRIGger:NRSub:MEASurement<Instance>:MEValuation:DELay
	TRIGger:NRSub:MEASurement<Instance>:MEValuation:TOUT
	TRIGger:NRSub:MEASurement<Instance>:MEValuation:MGAP
	TRIGger:NRSub:MEASurement<Instance>:MEValuation:SMODe
	TRIGger:NRSub:MEASurement<Instance>:MEValuation:FSYNc
	TRIGger:NRSub:MEASurement<Instance>:MEValuation:SOURce



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Trigger.NrSubMeas.MultiEval.MultiEvalCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.nrSubMeas.multiEval.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_NrSubMeas_MultiEval_Catalog.rst