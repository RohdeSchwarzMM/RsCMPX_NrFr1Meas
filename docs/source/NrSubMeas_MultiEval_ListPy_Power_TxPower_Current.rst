Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST:POWer:TXPower:CURRent
	single: CALCulate:NRSub:MEASurement<Instance>:MEValuation:LIST:POWer:TXPower:CURRent

.. code-block:: python

	FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST:POWer:TXPower:CURRent
	CALCulate:NRSub:MEASurement<Instance>:MEValuation:LIST:POWer:TXPower:CURRent



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.ListPy.Power.TxPower.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: