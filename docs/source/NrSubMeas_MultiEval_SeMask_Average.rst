Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:MEValuation:SEMask:AVERage
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:SEMask:AVERage
	single: CALCulate:NRSub:MEASurement<Instance>:MEValuation:SEMask:AVERage

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:MEValuation:SEMask:AVERage
	FETCh:NRSub:MEASurement<Instance>:MEValuation:SEMask:AVERage
	CALCulate:NRSub:MEASurement<Instance>:MEValuation:SEMask:AVERage



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.SeMask.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: