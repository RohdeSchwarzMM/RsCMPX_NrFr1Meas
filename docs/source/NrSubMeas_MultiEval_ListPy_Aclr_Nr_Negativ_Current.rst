Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST:ACLR:NR:NEGativ:CURRent
	single: CALCulate:NRSub:MEASurement<Instance>:MEValuation:LIST:ACLR:NR:NEGativ:CURRent

.. code-block:: python

	FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST:ACLR:NR:NEGativ:CURRent
	CALCulate:NRSub:MEASurement<Instance>:MEValuation:LIST:ACLR:NR:NEGativ:CURRent



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.ListPy.Aclr.Nr.Negativ.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: