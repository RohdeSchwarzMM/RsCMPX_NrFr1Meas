Scount
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:PRACh:SCOunt:MODulation
	single: CONFigure:NRSub:MEASurement<Instance>:PRACh:SCOunt:PDYNamics

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:PRACh:SCOunt:MODulation
	CONFigure:NRSub:MEASurement<Instance>:PRACh:SCOunt:PDYNamics



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.Prach.Scount.ScountCls
	:members:
	:undoc-members:
	:noindex: