Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST:SEMask:OBW:AVERage
	single: CALCulate:NRSub:MEASurement<Instance>:MEValuation:LIST:SEMask:OBW:AVERage

.. code-block:: python

	FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST:SEMask:OBW:AVERage
	CALCulate:NRSub:MEASurement<Instance>:MEValuation:LIST:SEMask:OBW:AVERage



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.ListPy.SeMask.Obw.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: