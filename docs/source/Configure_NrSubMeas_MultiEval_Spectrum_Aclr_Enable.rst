Enable
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:SPECtrum:ACLR:ENABle
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:SPECtrum:ACLR:ENABle:ENDC

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:MEValuation:SPECtrum:ACLR:ENABle
	CONFigure:NRSub:MEASurement<Instance>:MEValuation:SPECtrum:ACLR:ENABle:ENDC



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.MultiEval.Spectrum.Aclr.Enable.EnableCls
	:members:
	:undoc-members:
	:noindex: