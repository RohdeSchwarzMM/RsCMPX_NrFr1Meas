Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST:ACLR:ENDC:NEGativ:CURRent
	single: CALCulate:NRSub:MEASurement<Instance>:MEValuation:LIST:ACLR:ENDC:NEGativ:CURRent

.. code-block:: python

	FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST:ACLR:ENDC:NEGativ:CURRent
	CALCulate:NRSub:MEASurement<Instance>:MEValuation:LIST:ACLR:ENDC:NEGativ:CURRent



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.ListPy.Aclr.Endc.Negativ.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: