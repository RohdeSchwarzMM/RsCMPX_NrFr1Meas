Caggregation
----------------------------------------





.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.ListPy.Segment.Caggregation.CaggregationCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.nrSubMeas.listPy.segment.caggregation.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_NrSubMeas_ListPy_Segment_Caggregation_AcSpacing.rst
	Configure_NrSubMeas_ListPy_Segment_Caggregation_Mcarrier.rst