Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:PRACh:TRACe:MERRor:AVERage
	single: FETCh:NRSub:MEASurement<Instance>:PRACh:TRACe:MERRor:AVERage

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:PRACh:TRACe:MERRor:AVERage
	FETCh:NRSub:MEASurement<Instance>:PRACh:TRACe:MERRor:AVERage



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.Prach.Trace.Merror.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: