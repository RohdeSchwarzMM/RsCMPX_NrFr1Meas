Minr<MinRange>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr2
	rc = driver.nrSubMeas.multiEval.listPy.cc.esFlatness.minr.repcap_minRange_get()
	driver.nrSubMeas.multiEval.listPy.cc.esFlatness.minr.repcap_minRange_set(repcap.MinRange.Nr1)





.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.ListPy.Cc.EsFlatness.Minr.MinrCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrSubMeas.multiEval.listPy.cc.esFlatness.minr.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrSubMeas_MultiEval_ListPy_Cc_EsFlatness_Minr_Average.rst
	NrSubMeas_MultiEval_ListPy_Cc_EsFlatness_Minr_Current.rst
	NrSubMeas_MultiEval_ListPy_Cc_EsFlatness_Minr_Extreme.rst
	NrSubMeas_MultiEval_ListPy_Cc_EsFlatness_Minr_StandardDev.rst