StandardDev
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:MEValuation:TXPower:SDEViation
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:TXPower:SDEViation

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:MEValuation:TXPower:SDEViation
	FETCh:NRSub:MEASurement<Instance>:MEValuation:TXPower:SDEViation



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.TxPower.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: