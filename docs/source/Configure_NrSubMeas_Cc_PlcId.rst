PlcId
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>[:CC<no>]:PLCid

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>[:CC<no>]:PLCid



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.Cc.PlcId.PlcIdCls
	:members:
	:undoc-members:
	:noindex: