Psd
----------------------------------------





.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.ListPy.Cc.Modulation.Psd.PsdCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrSubMeas.multiEval.listPy.cc.modulation.psd.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrSubMeas_MultiEval_ListPy_Cc_Modulation_Psd_Average.rst
	NrSubMeas_MultiEval_ListPy_Cc_Modulation_Psd_Current.rst
	NrSubMeas_MultiEval_ListPy_Cc_Modulation_Psd_Maximum.rst
	NrSubMeas_MultiEval_ListPy_Cc_Modulation_Psd_Minimum.rst
	NrSubMeas_MultiEval_ListPy_Cc_Modulation_Psd_StandardDev.rst