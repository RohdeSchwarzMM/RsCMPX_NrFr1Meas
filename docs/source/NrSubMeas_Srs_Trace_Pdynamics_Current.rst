Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:SRS:TRACe:PDYNamics:CURRent
	single: FETCh:NRSub:MEASurement<Instance>:SRS:TRACe:PDYNamics:CURRent

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:SRS:TRACe:PDYNamics:CURRent
	FETCh:NRSub:MEASurement<Instance>:SRS:TRACe:PDYNamics:CURRent



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.Srs.Trace.Pdynamics.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: