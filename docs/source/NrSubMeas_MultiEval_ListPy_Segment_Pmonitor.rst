Pmonitor
----------------------------------------





.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.ListPy.Segment.Pmonitor.PmonitorCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrSubMeas.multiEval.listPy.segment.pmonitor.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrSubMeas_MultiEval_ListPy_Segment_Pmonitor_Array.rst
	NrSubMeas_MultiEval_ListPy_Segment_Pmonitor_Peak.rst
	NrSubMeas_MultiEval_ListPy_Segment_Pmonitor_Rms.rst
	NrSubMeas_MultiEval_ListPy_Segment_Pmonitor_Slots.rst