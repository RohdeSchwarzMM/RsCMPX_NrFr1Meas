Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:MEValuation[:LAYer<layer>]:PDYNamics:MINimum
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation[:LAYer<layer>]:PDYNamics:MINimum
	single: CALCulate:NRSub:MEASurement<Instance>:MEValuation[:LAYer<layer>]:PDYNamics:MINimum

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:MEValuation[:LAYer<layer>]:PDYNamics:MINimum
	FETCh:NRSub:MEASurement<Instance>:MEValuation[:LAYer<layer>]:PDYNamics:MINimum
	CALCulate:NRSub:MEASurement<Instance>:MEValuation[:LAYer<layer>]:PDYNamics:MINimum



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.Layer.Pdynamics.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: