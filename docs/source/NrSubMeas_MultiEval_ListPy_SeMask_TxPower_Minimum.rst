Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST:SEMask:TXPower:MINimum
	single: CALCulate:NRSub:MEASurement<Instance>:MEValuation:LIST:SEMask:TXPower:MINimum

.. code-block:: python

	FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST:SEMask:TXPower:MINimum
	CALCulate:NRSub:MEASurement<Instance>:MEValuation:LIST:SEMask:TXPower:MINimum



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.ListPy.SeMask.TxPower.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: