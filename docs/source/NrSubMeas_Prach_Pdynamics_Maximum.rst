Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:PRACh:PDYNamics:MAXimum
	single: FETCh:NRSub:MEASurement<Instance>:PRACh:PDYNamics:MAXimum
	single: CALCulate:NRSub:MEASurement<Instance>:PRACh:PDYNamics:MAXimum

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:PRACh:PDYNamics:MAXimum
	FETCh:NRSub:MEASurement<Instance>:PRACh:PDYNamics:MAXimum
	CALCulate:NRSub:MEASurement<Instance>:PRACh:PDYNamics:MAXimum



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.Prach.Pdynamics.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: