ScIndex
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation[:CC<no>][:LAYer<layer>]:ESFLatness:CURRent:SCINdex

.. code-block:: python

	FETCh:NRSub:MEASurement<Instance>:MEValuation[:CC<no>][:LAYer<layer>]:ESFLatness:CURRent:SCINdex



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.Cc.Layer.EsFlatness.Current.ScIndex.ScIndexCls
	:members:
	:undoc-members:
	:noindex: