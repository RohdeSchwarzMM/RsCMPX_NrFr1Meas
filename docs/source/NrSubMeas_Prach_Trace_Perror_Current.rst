Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:PRACh:TRACe:PERRor:CURRent
	single: FETCh:NRSub:MEASurement<Instance>:PRACh:TRACe:PERRor:CURRent

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:PRACh:TRACe:PERRor:CURRent
	FETCh:NRSub:MEASurement<Instance>:PRACh:TRACe:PERRor:CURRent



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.Prach.Trace.Perror.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: