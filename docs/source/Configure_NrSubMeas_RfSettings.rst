RfSettings
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:RFSettings:UMARgin
	single: CONFigure:NRSub:MEASurement<Instance>:RFSettings:ENPower
	single: CONFigure:NRSub:MEASurement<Instance>:RFSettings:FREQuency
	single: CONFigure:NRSub:MEASurement<Instance>:RFSettings:FOFFset
	single: CONFigure:NRSub:MEASurement<Instance>:RFSettings:MLOFfset
	single: CONFigure:NRSub:MEASurement<Instance>:RFSettings:LRINterval

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:RFSettings:UMARgin
	CONFigure:NRSub:MEASurement<Instance>:RFSettings:ENPower
	CONFigure:NRSub:MEASurement<Instance>:RFSettings:FREQuency
	CONFigure:NRSub:MEASurement<Instance>:RFSettings:FOFFset
	CONFigure:NRSub:MEASurement<Instance>:RFSettings:MLOFfset
	CONFigure:NRSub:MEASurement<Instance>:RFSettings:LRINterval



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.RfSettings.RfSettingsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.nrSubMeas.rfSettings.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_NrSubMeas_RfSettings_Eattenuation.rst
	Configure_NrSubMeas_RfSettings_LrStart.rst