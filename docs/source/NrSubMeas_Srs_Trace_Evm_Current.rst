Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:SRS:TRACe:EVM:CURRent
	single: FETCh:NRSub:MEASurement<Instance>:SRS:TRACe:EVM:CURRent

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:SRS:TRACe:EVM:CURRent
	FETCh:NRSub:MEASurement<Instance>:SRS:TRACe:EVM:CURRent



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.Srs.Trace.Evm.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: