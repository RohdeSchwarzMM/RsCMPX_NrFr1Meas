Perror
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:PRACh:LIMit:PERRor

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:PRACh:LIMit:PERRor



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.Prach.Limit.Perror.PerrorCls
	:members:
	:undoc-members:
	:noindex: