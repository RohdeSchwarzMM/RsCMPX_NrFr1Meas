Extreme
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:MEValuation[:CC<no>][:LAYer<layer>]:MODulation:EXTReme
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation[:CC<no>][:LAYer<layer>]:MODulation:EXTReme
	single: CALCulate:NRSub:MEASurement<Instance>:MEValuation[:CC<no>][:LAYer<layer>]:MODulation:EXTReme

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:MEValuation[:CC<no>][:LAYer<layer>]:MODulation:EXTReme
	FETCh:NRSub:MEASurement<Instance>:MEValuation[:CC<no>][:LAYer<layer>]:MODulation:EXTReme
	CALCulate:NRSub:MEASurement<Instance>:MEValuation[:CC<no>][:LAYer<layer>]:MODulation:EXTReme



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.Cc.Layer.Modulation.Extreme.ExtremeCls
	:members:
	:undoc-members:
	:noindex: