Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:SRS:EVMSymbol:PEAK:CURRent
	single: FETCh:NRSub:MEASurement<Instance>:SRS:EVMSymbol:PEAK:CURRent

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:SRS:EVMSymbol:PEAK:CURRent
	FETCh:NRSub:MEASurement<Instance>:SRS:EVMSymbol:PEAK:CURRent



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.Srs.EvmSymbol.Peak.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: