NrSubMeas
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:NANTenna
	single: CONFigure:NRSub:MEASurement<Instance>:STYPe
	single: CONFigure:NRSub:MEASurement<Instance>:BAND
	single: CONFigure:NRSub:MEASurement<Instance>:SPATh
	single: CONFigure:NRSub:MEASurement<Instance>:NTN
	single: CONFigure:NRSub:MEASurement<Instance>:NCARrier

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:NANTenna
	CONFigure:NRSub:MEASurement<Instance>:STYPe
	CONFigure:NRSub:MEASurement<Instance>:BAND
	CONFigure:NRSub:MEASurement<Instance>:SPATh
	CONFigure:NRSub:MEASurement<Instance>:NTN
	CONFigure:NRSub:MEASurement<Instance>:NCARrier



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.NrSubMeasCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.nrSubMeas.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_NrSubMeas_BwConfig.rst
	Configure_NrSubMeas_Caggregation.rst
	Configure_NrSubMeas_Cc.rst
	Configure_NrSubMeas_Ccall.rst
	Configure_NrSubMeas_ListPy.rst
	Configure_NrSubMeas_MultiEval.rst
	Configure_NrSubMeas_Network.rst
	Configure_NrSubMeas_Prach.rst
	Configure_NrSubMeas_RfSettings.rst
	Configure_NrSubMeas_Srs.rst
	Configure_NrSubMeas_Tpc.rst
	Configure_NrSubMeas_UlDl.rst