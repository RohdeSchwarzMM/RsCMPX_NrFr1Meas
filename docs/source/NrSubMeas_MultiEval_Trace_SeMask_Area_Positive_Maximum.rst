Maximum
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:TRACe:SEMask:AREA<area>:POSitive:MAXimum

.. code-block:: python

	FETCh:NRSub:MEASurement<Instance>:MEValuation:TRACe:SEMask:AREA<area>:POSitive:MAXimum



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.Trace.SeMask.Area.Positive.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: