Limit
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:MEValuation:TRACe[:CC<no>][:LAYer<layer>]:IEMissions:LIMit
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:TRACe[:CC<no>][:LAYer<layer>]:IEMissions:LIMit

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:MEValuation:TRACe[:CC<no>][:LAYer<layer>]:IEMissions:LIMit
	FETCh:NRSub:MEASurement<Instance>:MEValuation:TRACe[:CC<no>][:LAYer<layer>]:IEMissions:LIMit



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.Trace.Cc.Layer.Iemissions.Limit.LimitCls
	:members:
	:undoc-members:
	:noindex: