Extreme
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST[:CC<Carrier>]:MODulation:MERRor:DMRS:HIGH:EXTReme
	single: CALCulate:NRSub:MEASurement<Instance>:MEValuation:LIST[:CC<Carrier>]:MODulation:MERRor:DMRS:HIGH:EXTReme

.. code-block:: python

	FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST[:CC<Carrier>]:MODulation:MERRor:DMRS:HIGH:EXTReme
	CALCulate:NRSub:MEASurement<Instance>:MEValuation:LIST[:CC<Carrier>]:MODulation:MERRor:DMRS:HIGH:EXTReme



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.ListPy.Cc.Modulation.Merror.Dmrs.High.Extreme.ExtremeCls
	:members:
	:undoc-members:
	:noindex: