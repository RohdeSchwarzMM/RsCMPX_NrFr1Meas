Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST[:CC<Carrier>]:MODulation:MERRor:DMRS:LOW:CURRent
	single: CALCulate:NRSub:MEASurement<Instance>:MEValuation:LIST[:CC<Carrier>]:MODulation:MERRor:DMRS:LOW:CURRent

.. code-block:: python

	FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST[:CC<Carrier>]:MODulation:MERRor:DMRS:LOW:CURRent
	CALCulate:NRSub:MEASurement<Instance>:MEValuation:LIST[:CC<Carrier>]:MODulation:MERRor:DMRS:LOW:CURRent



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.ListPy.Cc.Modulation.Merror.Dmrs.Low.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: