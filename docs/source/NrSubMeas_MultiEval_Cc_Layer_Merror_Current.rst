Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:MEValuation[:CC<no>][:LAYer<layer>]:MERRor:CURRent
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation[:CC<no>][:LAYer<layer>]:MERRor:CURRent
	single: CALCulate:NRSub:MEASurement<Instance>:MEValuation[:CC<no>][:LAYer<layer>]:MERRor:CURRent

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:MEValuation[:CC<no>][:LAYer<layer>]:MERRor:CURRent
	FETCh:NRSub:MEASurement<Instance>:MEValuation[:CC<no>][:LAYer<layer>]:MERRor:CURRent
	CALCulate:NRSub:MEASurement<Instance>:MEValuation[:CC<no>][:LAYer<layer>]:MERRor:CURRent



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.Cc.Layer.Merror.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: