EvMagnitude
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:PRACh:LIMit:EVMagnitude

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:PRACh:LIMit:EVMagnitude



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.Prach.Limit.EvMagnitude.EvMagnitudeCls
	:members:
	:undoc-members:
	:noindex: