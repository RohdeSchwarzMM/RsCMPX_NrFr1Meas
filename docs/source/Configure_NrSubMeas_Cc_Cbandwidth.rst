Cbandwidth
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>[:CC<no>]:CBANdwidth

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>[:CC<no>]:CBANdwidth



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.Cc.Cbandwidth.CbandwidthCls
	:members:
	:undoc-members:
	:noindex: