Extreme
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:MEValuation:SEMask:EXTReme
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:SEMask:EXTReme
	single: CALCulate:NRSub:MEASurement<Instance>:MEValuation:SEMask:EXTReme

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:MEValuation:SEMask:EXTReme
	FETCh:NRSub:MEASurement<Instance>:MEValuation:SEMask:EXTReme
	CALCulate:NRSub:MEASurement<Instance>:MEValuation:SEMask:EXTReme



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.SeMask.Extreme.ExtremeCls
	:members:
	:undoc-members:
	:noindex: