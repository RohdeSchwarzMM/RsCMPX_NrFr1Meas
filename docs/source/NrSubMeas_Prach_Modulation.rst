Modulation
----------------------------------------





.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.Prach.Modulation.ModulationCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrSubMeas.prach.modulation.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrSubMeas_Prach_Modulation_Average.rst
	NrSubMeas_Prach_Modulation_Current.rst
	NrSubMeas_Prach_Modulation_DpfOffset.rst
	NrSubMeas_Prach_Modulation_DsIndex.rst
	NrSubMeas_Prach_Modulation_Extreme.rst
	NrSubMeas_Prach_Modulation_Nsymbol.rst
	NrSubMeas_Prach_Modulation_Preamble.rst
	NrSubMeas_Prach_Modulation_Scorrelation.rst
	NrSubMeas_Prach_Modulation_StandardDev.rst