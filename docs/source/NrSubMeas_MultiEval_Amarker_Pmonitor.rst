Pmonitor
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:AMARker<No>:PMONitor

.. code-block:: python

	FETCh:NRSub:MEASurement<Instance>:MEValuation:AMARker<No>:PMONitor



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.Amarker.Pmonitor.PmonitorCls
	:members:
	:undoc-members:
	:noindex: