Positive
----------------------------------------





.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.Trace.SeMask.Area.Positive.PositiveCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrSubMeas.multiEval.trace.seMask.area.positive.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrSubMeas_MultiEval_Trace_SeMask_Area_Positive_Average.rst
	NrSubMeas_MultiEval_Trace_SeMask_Area_Positive_Current.rst
	NrSubMeas_MultiEval_Trace_SeMask_Area_Positive_Frequency.rst
	NrSubMeas_MultiEval_Trace_SeMask_Area_Positive_Maximum.rst