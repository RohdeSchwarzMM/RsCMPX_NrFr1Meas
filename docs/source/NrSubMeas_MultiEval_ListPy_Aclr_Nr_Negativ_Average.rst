Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST:ACLR:NR:NEGativ:AVERage
	single: CALCulate:NRSub:MEASurement<Instance>:MEValuation:LIST:ACLR:NR:NEGativ:AVERage

.. code-block:: python

	FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST:ACLR:NR:NEGativ:AVERage
	CALCulate:NRSub:MEASurement<Instance>:MEValuation:LIST:ACLR:NR:NEGativ:AVERage



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.ListPy.Aclr.Nr.Negativ.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: