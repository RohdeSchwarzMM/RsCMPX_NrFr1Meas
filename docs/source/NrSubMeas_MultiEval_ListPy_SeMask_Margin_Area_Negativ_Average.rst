Average
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST:SEMask:MARGin:AREA<nr6g>:NEGativ:AVERage

.. code-block:: python

	FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST:SEMask:MARGin:AREA<nr6g>:NEGativ:AVERage



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.ListPy.SeMask.Margin.Area.Negativ.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: