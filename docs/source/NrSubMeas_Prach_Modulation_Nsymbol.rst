Nsymbol
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NRSub:MEASurement<Instance>:PRACh:MODulation:NSYMbol

.. code-block:: python

	FETCh:NRSub:MEASurement<Instance>:PRACh:MODulation:NSYMbol



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.Prach.Modulation.Nsymbol.NsymbolCls
	:members:
	:undoc-members:
	:noindex: