Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:SRS:MODulation:CURRent
	single: FETCh:NRSub:MEASurement<Instance>:SRS:MODulation:CURRent
	single: CALCulate:NRSub:MEASurement<Instance>:SRS:MODulation:CURRent

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:SRS:MODulation:CURRent
	FETCh:NRSub:MEASurement<Instance>:SRS:MODulation:CURRent
	CALCulate:NRSub:MEASurement<Instance>:SRS:MODulation:CURRent



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.Srs.Modulation.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: