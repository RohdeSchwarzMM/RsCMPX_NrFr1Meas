Additional<AddTable>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr4
	rc = driver.configure.nrSubMeas.multiEval.limit.seMask.area.additional.repcap_addTable_get()
	driver.configure.nrSubMeas.multiEval.limit.seMask.area.additional.repcap_addTable_set(repcap.AddTable.Nr1)





.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.MultiEval.Limit.SeMask.Area.Additional.AdditionalCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.nrSubMeas.multiEval.limit.seMask.area.additional.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_NrSubMeas_MultiEval_Limit_SeMask_Area_Additional_Cbandwidth.rst