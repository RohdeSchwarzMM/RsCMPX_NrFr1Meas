Cbandwidth
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:NETWork[:CC<no>]:CBANdwidth

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:NETWork[:CC<no>]:CBANdwidth



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.Network.Cc.Cbandwidth.CbandwidthCls
	:members:
	:undoc-members:
	:noindex: