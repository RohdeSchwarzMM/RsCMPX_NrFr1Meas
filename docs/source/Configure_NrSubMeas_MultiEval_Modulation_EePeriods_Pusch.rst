Pusch
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:MODulation:EEPeriods:PUSCh:LEADing
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:MODulation:EEPeriods:PUSCh:LAGGing

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:MEValuation:MODulation:EEPeriods:PUSCh:LEADing
	CONFigure:NRSub:MEASurement<Instance>:MEValuation:MODulation:EEPeriods:PUSCh:LAGGing



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.MultiEval.Modulation.EePeriods.Pusch.PuschCls
	:members:
	:undoc-members:
	:noindex: