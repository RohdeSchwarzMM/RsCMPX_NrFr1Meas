Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:SRS:PDYNamics:CURRent
	single: FETCh:NRSub:MEASurement<Instance>:SRS:PDYNamics:CURRent
	single: CALCulate:NRSub:MEASurement<Instance>:SRS:PDYNamics:CURRent

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:SRS:PDYNamics:CURRent
	FETCh:NRSub:MEASurement<Instance>:SRS:PDYNamics:CURRent
	CALCulate:NRSub:MEASurement<Instance>:SRS:PDYNamics:CURRent



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.Srs.Pdynamics.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: