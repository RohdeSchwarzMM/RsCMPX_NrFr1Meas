Area<Area>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr12
	rc = driver.nrSubMeas.multiEval.trace.seMask.area.repcap_area_get()
	driver.nrSubMeas.multiEval.trace.seMask.area.repcap_area_set(repcap.Area.Nr1)





.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.Trace.SeMask.Area.AreaCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrSubMeas.multiEval.trace.seMask.area.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrSubMeas_MultiEval_Trace_SeMask_Area_Negative.rst
	NrSubMeas_MultiEval_Trace_SeMask_Area_Positive.rst