EvPreamble
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:PRACh:TRACe:EVPReamble
	single: FETCh:NRSub:MEASurement<Instance>:PRACh:TRACe:EVPReamble

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:PRACh:TRACe:EVPReamble
	FETCh:NRSub:MEASurement<Instance>:PRACh:TRACe:EVPReamble



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.Prach.Trace.EvPreamble.EvPreambleCls
	:members:
	:undoc-members:
	:noindex: