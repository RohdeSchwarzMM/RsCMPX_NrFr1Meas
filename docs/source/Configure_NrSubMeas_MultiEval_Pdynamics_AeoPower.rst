AeoPower
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:PDYNamics:AEOPower:LEADing
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:PDYNamics:AEOPower:LAGGing

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:MEValuation:PDYNamics:AEOPower:LEADing
	CONFigure:NRSub:MEASurement<Instance>:MEValuation:PDYNamics:AEOPower:LAGGing



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.MultiEval.Pdynamics.AeoPower.AeoPowerCls
	:members:
	:undoc-members:
	:noindex: