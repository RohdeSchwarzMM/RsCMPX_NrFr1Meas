Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:MEValuation:TRACe[:LAYer<layer>]:PDYNamics:MAXimum
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:TRACe[:LAYer<layer>]:PDYNamics:MAXimum

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:MEValuation:TRACe[:LAYer<layer>]:PDYNamics:MAXimum
	FETCh:NRSub:MEASurement<Instance>:MEValuation:TRACe[:LAYer<layer>]:PDYNamics:MAXimum



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.Trace.Layer.Pdynamics.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: