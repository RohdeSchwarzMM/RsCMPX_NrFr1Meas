Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:MEValuation[:CC<no>][:LAYer<layer>]:EVMagnitude:CURRent
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation[:CC<no>][:LAYer<layer>]:EVMagnitude:CURRent
	single: CALCulate:NRSub:MEASurement<Instance>:MEValuation[:CC<no>][:LAYer<layer>]:EVMagnitude:CURRent

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:MEValuation[:CC<no>][:LAYer<layer>]:EVMagnitude:CURRent
	FETCh:NRSub:MEASurement<Instance>:MEValuation[:CC<no>][:LAYer<layer>]:EVMagnitude:CURRent
	CALCulate:NRSub:MEASurement<Instance>:MEValuation[:CC<no>][:LAYer<layer>]:EVMagnitude:CURRent



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.Cc.Layer.EvMagnitude.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: