Amarker<AbsoluteMarker>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr2
	rc = driver.nrSubMeas.multiEval.cc.layer.amarker.repcap_absoluteMarker_get()
	driver.nrSubMeas.multiEval.cc.layer.amarker.repcap_absoluteMarker_set(repcap.AbsoluteMarker.Nr1)





.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.Cc.Layer.Amarker.AmarkerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrSubMeas.multiEval.cc.layer.amarker.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrSubMeas_MultiEval_Cc_Layer_Amarker_EvMagnitude.rst
	NrSubMeas_MultiEval_Cc_Layer_Amarker_Merror.rst
	NrSubMeas_MultiEval_Cc_Layer_Amarker_Perror.rst