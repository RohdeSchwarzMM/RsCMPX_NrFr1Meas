Cbandwidth
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:CAGGregation:CBANdwidth:AGGRegated

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:CAGGregation:CBANdwidth:AGGRegated



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.Caggregation.Cbandwidth.CbandwidthCls
	:members:
	:undoc-members:
	:noindex: