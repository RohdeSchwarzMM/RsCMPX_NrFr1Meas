MsubFrames
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:MSUBframes

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:MEValuation:MSUBframes



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.MultiEval.MsubFrames.MsubFramesCls
	:members:
	:undoc-members:
	:noindex: