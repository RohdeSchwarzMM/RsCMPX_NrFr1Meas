Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:TPC:TRACe:PSTeps:CURRent
	single: FETCh:NRSub:MEASurement<Instance>:TPC:TRACe:PSTeps:CURRent

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:TPC:TRACe:PSTeps:CURRent
	FETCh:NRSub:MEASurement<Instance>:TPC:TRACe:PSTeps:CURRent



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.Tpc.Trace.Psteps.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: