Vresults
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:TRACe:IEMissions:VRESults

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:MEValuation:TRACe:IEMissions:VRESults



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.MultiEval.Trace.Iemissions.Vresults.VresultsCls
	:members:
	:undoc-members:
	:noindex: