Dallocation
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:SEMask:DALLocation

.. code-block:: python

	FETCh:NRSub:MEASurement<Instance>:MEValuation:SEMask:DALLocation



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.SeMask.Dallocation.DallocationCls
	:members:
	:undoc-members:
	:noindex: