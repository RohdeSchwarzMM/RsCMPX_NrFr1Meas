Nbits
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>[:CC<no>]:PSCCh:NBITs

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>[:CC<no>]:PSCCh:NBITs



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.Cc.Pscch.Nbits.NbitsCls
	:members:
	:undoc-members:
	:noindex: