All
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NRSub:MEASurement<Instance>:TPC:STATe:ALL

.. code-block:: python

	FETCh:NRSub:MEASurement<Instance>:TPC:STATe:ALL



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.Tpc.State.All.AllCls
	:members:
	:undoc-members:
	:noindex: