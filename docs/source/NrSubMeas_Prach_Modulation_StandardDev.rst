StandardDev
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:PRACh:MODulation:SDEViation
	single: FETCh:NRSub:MEASurement<Instance>:PRACh:MODulation:SDEViation

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:PRACh:MODulation:SDEViation
	FETCh:NRSub:MEASurement<Instance>:PRACh:MODulation:SDEViation



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.Prach.Modulation.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: