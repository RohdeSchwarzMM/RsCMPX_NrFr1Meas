Trace
----------------------------------------





.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.Trace.TraceCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrSubMeas.multiEval.trace.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrSubMeas_MultiEval_Trace_Aclr.rst
	NrSubMeas_MultiEval_Trace_Cc.rst
	NrSubMeas_MultiEval_Trace_Layer.rst
	NrSubMeas_MultiEval_Trace_Pmonitor.rst
	NrSubMeas_MultiEval_Trace_SeMask.rst