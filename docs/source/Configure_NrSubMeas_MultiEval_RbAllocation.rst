RbAllocation
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:RBALlocation:AUTO
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:RBALlocation:NRB
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:RBALlocation:SRB

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:MEValuation:RBALlocation:AUTO
	CONFigure:NRSub:MEASurement<Instance>:MEValuation:RBALlocation:NRB
	CONFigure:NRSub:MEASurement<Instance>:MEValuation:RBALlocation:SRB



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.MultiEval.RbAllocation.RbAllocationCls
	:members:
	:undoc-members:
	:noindex: