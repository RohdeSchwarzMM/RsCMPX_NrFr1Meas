Cbgt
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:LIMit:PDYNamics:TTOLerance:CBGT

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:MEValuation:LIMit:PDYNamics:TTOLerance:CBGT



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.MultiEval.Limit.Pdynamics.Ttolerance.Cbgt.CbgtCls
	:members:
	:undoc-members:
	:noindex: