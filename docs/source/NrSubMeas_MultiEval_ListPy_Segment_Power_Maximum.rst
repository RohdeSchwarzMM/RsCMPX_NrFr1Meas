Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr6g>:POWer:MAXimum
	single: CALCulate:NRSub:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr6g>:POWer:MAXimum

.. code-block:: python

	FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr6g>:POWer:MAXimum
	CALCulate:NRSub:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr6g>:POWer:MAXimum



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.ListPy.Segment.Power.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: