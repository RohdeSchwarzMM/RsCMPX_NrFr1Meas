Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:SRS:PDYNamics:AVERage
	single: FETCh:NRSub:MEASurement<Instance>:SRS:PDYNamics:AVERage
	single: CALCulate:NRSub:MEASurement<Instance>:SRS:PDYNamics:AVERage

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:SRS:PDYNamics:AVERage
	FETCh:NRSub:MEASurement<Instance>:SRS:PDYNamics:AVERage
	CALCulate:NRSub:MEASurement<Instance>:SRS:PDYNamics:AVERage



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.Srs.Pdynamics.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: