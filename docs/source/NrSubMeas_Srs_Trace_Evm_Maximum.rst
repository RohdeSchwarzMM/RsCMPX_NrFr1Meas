Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:SRS:TRACe:EVM:MAXimum
	single: FETCh:NRSub:MEASurement<Instance>:SRS:TRACe:EVM:MAXimum

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:SRS:TRACe:EVM:MAXimum
	FETCh:NRSub:MEASurement<Instance>:SRS:TRACe:EVM:MAXimum



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.Srs.Trace.Evm.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: