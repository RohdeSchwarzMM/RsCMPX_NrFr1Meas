Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:PRACh:PDYNamics:AVERage
	single: FETCh:NRSub:MEASurement<Instance>:PRACh:PDYNamics:AVERage
	single: CALCulate:NRSub:MEASurement<Instance>:PRACh:PDYNamics:AVERage

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:PRACh:PDYNamics:AVERage
	FETCh:NRSub:MEASurement<Instance>:PRACh:PDYNamics:AVERage
	CALCulate:NRSub:MEASurement<Instance>:PRACh:PDYNamics:AVERage



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.Prach.Pdynamics.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: