Setup
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:LIST:SEGMent<no>:SETup

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:MEValuation:LIST:SEGMent<no>:SETup



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.MultiEval.ListPy.Segment.Setup.SetupCls
	:members:
	:undoc-members:
	:noindex: