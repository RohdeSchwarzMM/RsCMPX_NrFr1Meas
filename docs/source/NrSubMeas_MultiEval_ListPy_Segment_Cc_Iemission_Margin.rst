Margin
----------------------------------------





.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.ListPy.Segment.Cc.Iemission.Margin.MarginCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrSubMeas.multiEval.listPy.segment.cc.iemission.margin.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrSubMeas_MultiEval_ListPy_Segment_Cc_Iemission_Margin_Average.rst
	NrSubMeas_MultiEval_ListPy_Segment_Cc_Iemission_Margin_Current.rst
	NrSubMeas_MultiEval_ListPy_Segment_Cc_Iemission_Margin_Extreme.rst
	NrSubMeas_MultiEval_ListPy_Segment_Cc_Iemission_Margin_StandardDev.rst