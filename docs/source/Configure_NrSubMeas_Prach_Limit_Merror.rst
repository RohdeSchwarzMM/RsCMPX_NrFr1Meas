Merror
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:PRACh:LIMit:MERRor

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:PRACh:LIMit:MERRor



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.Prach.Limit.Merror.MerrorCls
	:members:
	:undoc-members:
	:noindex: