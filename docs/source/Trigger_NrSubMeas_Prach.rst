Prach
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: TRIGger:NRSub:MEASurement<Instance>:PRACh:THReshold
	single: TRIGger:NRSub:MEASurement<Instance>:PRACh:SLOPe
	single: TRIGger:NRSub:MEASurement<Instance>:PRACh:TOUT
	single: TRIGger:NRSub:MEASurement<Instance>:PRACh:MGAP

.. code-block:: python

	TRIGger:NRSub:MEASurement<Instance>:PRACh:THReshold
	TRIGger:NRSub:MEASurement<Instance>:PRACh:SLOPe
	TRIGger:NRSub:MEASurement<Instance>:PRACh:TOUT
	TRIGger:NRSub:MEASurement<Instance>:PRACh:MGAP



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Trigger.NrSubMeas.Prach.PrachCls
	:members:
	:undoc-members:
	:noindex: