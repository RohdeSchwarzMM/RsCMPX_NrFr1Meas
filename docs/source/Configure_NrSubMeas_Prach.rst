Prach
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:PRACh:TOUT
	single: CONFigure:NRSub:MEASurement<Instance>:PRACh:REPetition
	single: CONFigure:NRSub:MEASurement<Instance>:PRACh:SCONdition
	single: CONFigure:NRSub:MEASurement<Instance>:PRACh:MOEXception
	single: CONFigure:NRSub:MEASurement<Instance>:PRACh:PCINdex
	single: CONFigure:NRSub:MEASurement<Instance>:PRACh:PFORmat
	single: CONFigure:NRSub:MEASurement<Instance>:PRACh:SSYMbol
	single: CONFigure:NRSub:MEASurement<Instance>:PRACh:SCSPacing
	single: CONFigure:NRSub:MEASurement<Instance>:PRACh:NOPReambles
	single: CONFigure:NRSub:MEASurement<Instance>:PRACh:POPReambles
	single: CONFigure:NRSub:MEASurement<Instance>:PRACh:LRSindex
	single: CONFigure:NRSub:MEASurement<Instance>:PRACh:ZCZone

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:PRACh:TOUT
	CONFigure:NRSub:MEASurement<Instance>:PRACh:REPetition
	CONFigure:NRSub:MEASurement<Instance>:PRACh:SCONdition
	CONFigure:NRSub:MEASurement<Instance>:PRACh:MOEXception
	CONFigure:NRSub:MEASurement<Instance>:PRACh:PCINdex
	CONFigure:NRSub:MEASurement<Instance>:PRACh:PFORmat
	CONFigure:NRSub:MEASurement<Instance>:PRACh:SSYMbol
	CONFigure:NRSub:MEASurement<Instance>:PRACh:SCSPacing
	CONFigure:NRSub:MEASurement<Instance>:PRACh:NOPReambles
	CONFigure:NRSub:MEASurement<Instance>:PRACh:POPReambles
	CONFigure:NRSub:MEASurement<Instance>:PRACh:LRSindex
	CONFigure:NRSub:MEASurement<Instance>:PRACh:ZCZone



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.Prach.PrachCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.nrSubMeas.prach.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_NrSubMeas_Prach_Limit.rst
	Configure_NrSubMeas_Prach_Modulation.rst
	Configure_NrSubMeas_Prach_PfOffset.rst
	Configure_NrSubMeas_Prach_Result.rst
	Configure_NrSubMeas_Prach_Rsetting.rst
	Configure_NrSubMeas_Prach_Scount.rst
	Configure_NrSubMeas_Prach_Sindex.rst