SeMask
----------------------------------------





.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.SeMask.SeMaskCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrSubMeas.multiEval.seMask.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrSubMeas_MultiEval_SeMask_Alength.rst
	NrSubMeas_MultiEval_SeMask_Aoffset.rst
	NrSubMeas_MultiEval_SeMask_Average.rst
	NrSubMeas_MultiEval_SeMask_Current.rst
	NrSubMeas_MultiEval_SeMask_Dallocation.rst
	NrSubMeas_MultiEval_SeMask_DchType.rst
	NrSubMeas_MultiEval_SeMask_Extreme.rst
	NrSubMeas_MultiEval_SeMask_Margin.rst
	NrSubMeas_MultiEval_SeMask_StandardDev.rst