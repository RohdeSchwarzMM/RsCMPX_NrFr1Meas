Sindex
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:PRACh:SINDex:AUTO
	single: CONFigure:NRSub:MEASurement<Instance>:PRACh:SINDex

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:PRACh:SINDex:AUTO
	CONFigure:NRSub:MEASurement<Instance>:PRACh:SINDex



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.Prach.Sindex.SindexCls
	:members:
	:undoc-members:
	:noindex: