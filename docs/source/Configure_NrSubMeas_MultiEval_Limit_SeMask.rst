SeMask
----------------------------------------





.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.MultiEval.Limit.SeMask.SeMaskCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.nrSubMeas.multiEval.limit.seMask.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_NrSubMeas_MultiEval_Limit_SeMask_ActLimit.rst
	Configure_NrSubMeas_MultiEval_Limit_SeMask_Area.rst
	Configure_NrSubMeas_MultiEval_Limit_SeMask_ObwLimit.rst
	Configure_NrSubMeas_MultiEval_Limit_SeMask_Standard.rst
	Configure_NrSubMeas_MultiEval_Limit_SeMask_Ttolerance.rst
	Configure_NrSubMeas_MultiEval_Limit_SeMask_UserDefined.rst