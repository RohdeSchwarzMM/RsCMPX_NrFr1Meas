Slots
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:MEValuation:TRACe:PMONitor:SLOTs
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:TRACe:PMONitor:SLOTs

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:MEValuation:TRACe:PMONitor:SLOTs
	FETCh:NRSub:MEASurement<Instance>:MEValuation:TRACe:PMONitor:SLOTs



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.Trace.Pmonitor.Slots.SlotsCls
	:members:
	:undoc-members:
	:noindex: