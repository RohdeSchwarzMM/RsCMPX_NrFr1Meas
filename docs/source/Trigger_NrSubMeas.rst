NrSubMeas
----------------------------------------





.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Trigger.NrSubMeas.NrSubMeasCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.nrSubMeas.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_NrSubMeas_ListPy.rst
	Trigger_NrSubMeas_MultiEval.rst
	Trigger_NrSubMeas_Prach.rst
	Trigger_NrSubMeas_Srs.rst
	Trigger_NrSubMeas_Tpc.rst