EvMagnitude
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:SRS:LIMit:EVMagnitude

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:SRS:LIMit:EVMagnitude



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.Srs.Limit.EvMagnitude.EvMagnitudeCls
	:members:
	:undoc-members:
	:noindex: