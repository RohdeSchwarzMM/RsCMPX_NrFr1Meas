SingleCmw
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:LIST:CMWS:CMODe

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:MEValuation:LIST:CMWS:CMODe



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.MultiEval.ListPy.SingleCmw.SingleCmwCls
	:members:
	:undoc-members:
	:noindex: