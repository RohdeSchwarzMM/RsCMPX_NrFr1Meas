Cc<CarrierComponent>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr2
	rc = driver.nrSubMeas.multiEval.listPy.cc.repcap_carrierComponent_get()
	driver.nrSubMeas.multiEval.listPy.cc.repcap_carrierComponent_set(repcap.CarrierComponent.Nr1)





.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.ListPy.Cc.CcCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrSubMeas.multiEval.listPy.cc.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrSubMeas_MultiEval_ListPy_Cc_EsFlatness.rst
	NrSubMeas_MultiEval_ListPy_Cc_Iemission.rst
	NrSubMeas_MultiEval_ListPy_Cc_Modulation.rst