Pdynamics
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:SRS:PDYNamics:HDMode

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:SRS:PDYNamics:HDMode



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.Srs.Pdynamics.PdynamicsCls
	:members:
	:undoc-members:
	:noindex: