Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST:ACLR:NR:POSitiv:CURRent
	single: CALCulate:NRSub:MEASurement<Instance>:MEValuation:LIST:ACLR:NR:POSitiv:CURRent

.. code-block:: python

	FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST:ACLR:NR:POSitiv:CURRent
	CALCulate:NRSub:MEASurement<Instance>:MEValuation:LIST:ACLR:NR:POSitiv:CURRent



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.ListPy.Aclr.Nr.Positiv.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: