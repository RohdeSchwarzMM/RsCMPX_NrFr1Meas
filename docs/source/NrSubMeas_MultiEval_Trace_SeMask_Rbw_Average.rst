Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:MEValuation:TRACe:SEMask:RBW<kHz>:AVERage
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:TRACe:SEMask:RBW<kHz>:AVERage

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:MEValuation:TRACe:SEMask:RBW<kHz>:AVERage
	FETCh:NRSub:MEASurement<Instance>:MEValuation:TRACe:SEMask:RBW<kHz>:AVERage



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.Trace.SeMask.Rbw.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: