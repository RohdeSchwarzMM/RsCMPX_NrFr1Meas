Rtype
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:SRS:RTYPe

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:SRS:RTYPe



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.Srs.Rtype.RtypeCls
	:members:
	:undoc-members:
	:noindex: