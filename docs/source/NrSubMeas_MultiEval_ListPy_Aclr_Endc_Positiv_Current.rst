Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST:ACLR:ENDC:POSitiv:CURRent
	single: CALCulate:NRSub:MEASurement<Instance>:MEValuation:LIST:ACLR:ENDC:POSitiv:CURRent

.. code-block:: python

	FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST:ACLR:ENDC:POSitiv:CURRent
	CALCulate:NRSub:MEASurement<Instance>:MEValuation:LIST:ACLR:ENDC:POSitiv:CURRent



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.ListPy.Aclr.Endc.Positiv.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: