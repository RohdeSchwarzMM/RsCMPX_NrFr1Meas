Nsymbols
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>[:CC<no>]:PSCCh:NSYMbols

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>[:CC<no>]:PSCCh:NSYMbols



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.Cc.Pscch.Nsymbols.NsymbolsCls
	:members:
	:undoc-members:
	:noindex: