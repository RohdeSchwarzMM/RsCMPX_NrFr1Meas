Perror
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:SRS:LIMit:PERRor

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:SRS:LIMit:PERRor



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.Srs.Limit.Perror.PerrorCls
	:members:
	:undoc-members:
	:noindex: