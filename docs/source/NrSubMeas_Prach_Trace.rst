Trace
----------------------------------------





.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.Prach.Trace.TraceCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrSubMeas.prach.trace.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrSubMeas_Prach_Trace_Evm.rst
	NrSubMeas_Prach_Trace_EvPreamble.rst
	NrSubMeas_Prach_Trace_Iq.rst
	NrSubMeas_Prach_Trace_Merror.rst
	NrSubMeas_Prach_Trace_Pdynamics.rst
	NrSubMeas_Prach_Trace_Perror.rst
	NrSubMeas_Prach_Trace_PvPreamble.rst