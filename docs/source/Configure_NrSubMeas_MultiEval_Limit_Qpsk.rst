Qpsk
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:LIMit:QPSK:FERRor
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:LIMit:QPSK:ESFLatness

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:MEValuation:LIMit:QPSK:FERRor
	CONFigure:NRSub:MEASurement<Instance>:MEValuation:LIMit:QPSK:ESFLatness



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.MultiEval.Limit.Qpsk.QpskCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.nrSubMeas.multiEval.limit.qpsk.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_NrSubMeas_MultiEval_Limit_Qpsk_EvMagnitude.rst
	Configure_NrSubMeas_MultiEval_Limit_Qpsk_Ibe.rst
	Configure_NrSubMeas_MultiEval_Limit_Qpsk_IqOffset.rst
	Configure_NrSubMeas_MultiEval_Limit_Qpsk_Merror.rst
	Configure_NrSubMeas_MultiEval_Limit_Qpsk_Perror.rst