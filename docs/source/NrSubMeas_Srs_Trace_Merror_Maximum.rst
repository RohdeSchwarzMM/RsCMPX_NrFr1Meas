Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:SRS:TRACe:MERRor:MAXimum
	single: FETCh:NRSub:MEASurement<Instance>:SRS:TRACe:MERRor:MAXimum

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:SRS:TRACe:MERRor:MAXimum
	FETCh:NRSub:MEASurement<Instance>:SRS:TRACe:MERRor:MAXimum



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.Srs.Trace.Merror.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: