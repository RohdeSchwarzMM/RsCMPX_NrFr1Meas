Tcomb
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:SRS:TCOMb

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:SRS:TCOMb



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.Srs.Tcomb.TcombCls
	:members:
	:undoc-members:
	:noindex: