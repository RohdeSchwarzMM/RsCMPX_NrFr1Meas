Iemissions
----------------------------------------





.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.Trace.Cc.Layer.Iemissions.IemissionsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrSubMeas.multiEval.trace.cc.layer.iemissions.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrSubMeas_MultiEval_Trace_Cc_Layer_Iemissions_Average.rst
	NrSubMeas_MultiEval_Trace_Cc_Layer_Iemissions_Current.rst
	NrSubMeas_MultiEval_Trace_Cc_Layer_Iemissions_Limit.rst
	NrSubMeas_MultiEval_Trace_Cc_Layer_Iemissions_Maximum.rst