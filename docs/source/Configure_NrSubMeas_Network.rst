Network
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:NETWork:FREQuency
	single: CONFigure:NRSub:MEASurement<Instance>:NETWork:BAND
	single: CONFigure:NRSub:MEASurement<Instance>:NETWork:DMODe
	single: CONFigure:NRSub:MEASurement<Instance>:NETWork:NSValue
	single: CONFigure:NRSub:MEASurement<Instance>:NETWork:NANTenna
	single: CONFigure:NRSub:MEASurement<Instance>:NETWork:RFPSharing

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:NETWork:FREQuency
	CONFigure:NRSub:MEASurement<Instance>:NETWork:BAND
	CONFigure:NRSub:MEASurement<Instance>:NETWork:DMODe
	CONFigure:NRSub:MEASurement<Instance>:NETWork:NSValue
	CONFigure:NRSub:MEASurement<Instance>:NETWork:NANTenna
	CONFigure:NRSub:MEASurement<Instance>:NETWork:RFPSharing



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.Network.NetworkCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.nrSubMeas.network.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_NrSubMeas_Network_Cc.rst
	Configure_NrSubMeas_Network_Ccall.rst
	Configure_NrSubMeas_Network_Prach.rst
	Configure_NrSubMeas_Network_UlDl.rst