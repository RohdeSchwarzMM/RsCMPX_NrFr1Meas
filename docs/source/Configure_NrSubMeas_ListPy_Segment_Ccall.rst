Ccall
----------------------------------------





.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.ListPy.Segment.Ccall.CcallCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.nrSubMeas.listPy.segment.ccall.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_NrSubMeas_ListPy_Segment_Ccall_TxBwidth.rst