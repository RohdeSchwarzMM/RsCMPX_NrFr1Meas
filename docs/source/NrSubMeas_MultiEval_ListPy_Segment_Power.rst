Power
----------------------------------------





.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.ListPy.Segment.Power.PowerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrSubMeas.multiEval.listPy.segment.power.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrSubMeas_MultiEval_ListPy_Segment_Power_Average.rst
	NrSubMeas_MultiEval_ListPy_Segment_Power_Current.rst
	NrSubMeas_MultiEval_ListPy_Segment_Power_Maximum.rst
	NrSubMeas_MultiEval_ListPy_Segment_Power_Minimum.rst
	NrSubMeas_MultiEval_ListPy_Segment_Power_StandardDev.rst