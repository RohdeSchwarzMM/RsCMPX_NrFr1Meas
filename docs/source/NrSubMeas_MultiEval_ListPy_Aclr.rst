Aclr
----------------------------------------





.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.ListPy.Aclr.AclrCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrSubMeas.multiEval.listPy.aclr.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrSubMeas_MultiEval_ListPy_Aclr_Dallocation.rst
	NrSubMeas_MultiEval_ListPy_Aclr_DchType.rst
	NrSubMeas_MultiEval_ListPy_Aclr_Endc.rst
	NrSubMeas_MultiEval_ListPy_Aclr_Nr.rst
	NrSubMeas_MultiEval_ListPy_Aclr_Utra.rst