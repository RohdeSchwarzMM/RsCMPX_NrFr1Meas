Average
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST[:CC<Carrier>]:IEMission:MARGin:AVERage

.. code-block:: python

	FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST[:CC<Carrier>]:IEMission:MARGin:AVERage



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.ListPy.Cc.Iemission.Margin.Average.AverageCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrSubMeas.multiEval.listPy.cc.iemission.margin.average.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrSubMeas_MultiEval_ListPy_Cc_Iemission_Margin_Average_RbIndex.rst