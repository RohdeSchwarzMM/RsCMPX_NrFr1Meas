Scorrelation
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NRSub:MEASurement<Instance>:PRACh:MODulation:SCORrelation

.. code-block:: python

	FETCh:NRSub:MEASurement<Instance>:PRACh:MODulation:SCORrelation



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.Prach.Modulation.Scorrelation.ScorrelationCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrSubMeas.prach.modulation.scorrelation.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrSubMeas_Prach_Modulation_Scorrelation_Preamble.rst