Cc
----------------------------------------





.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.Cc.CcCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.nrSubMeas.cc.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_NrSubMeas_Cc_Allocation.rst
	Configure_NrSubMeas_Cc_BwPart.rst
	Configure_NrSubMeas_Cc_Cbandwidth.rst
	Configure_NrSubMeas_Cc_Frequency.rst
	Configure_NrSubMeas_Cc_Nallocations.rst
	Configure_NrSubMeas_Cc_NbwParts.rst
	Configure_NrSubMeas_Cc_PlcId.rst
	Configure_NrSubMeas_Cc_Pscch.rst
	Configure_NrSubMeas_Cc_Pssch.rst
	Configure_NrSubMeas_Cc_Rpool.rst
	Configure_NrSubMeas_Cc_TaPosition.rst
	Configure_NrSubMeas_Cc_TxBwidth.rst