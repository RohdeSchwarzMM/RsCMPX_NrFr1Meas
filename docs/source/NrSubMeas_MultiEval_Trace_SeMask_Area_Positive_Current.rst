Current
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:TRACe:SEMask:AREA<area>:POSitive:CURRent

.. code-block:: python

	FETCh:NRSub:MEASurement<Instance>:MEValuation:TRACe:SEMask:AREA<area>:POSitive:CURRent



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.Trace.SeMask.Area.Positive.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: