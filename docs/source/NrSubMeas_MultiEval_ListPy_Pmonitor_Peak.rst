Peak
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST:PMONitor:PEAK

.. code-block:: python

	FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST:PMONitor:PEAK



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.ListPy.Pmonitor.Peak.PeakCls
	:members:
	:undoc-members:
	:noindex: