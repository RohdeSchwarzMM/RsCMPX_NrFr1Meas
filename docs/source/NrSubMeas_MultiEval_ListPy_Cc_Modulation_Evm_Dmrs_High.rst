High
----------------------------------------





.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.ListPy.Cc.Modulation.Evm.Dmrs.High.HighCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrSubMeas.multiEval.listPy.cc.modulation.evm.dmrs.high.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrSubMeas_MultiEval_ListPy_Cc_Modulation_Evm_Dmrs_High_Average.rst
	NrSubMeas_MultiEval_ListPy_Cc_Modulation_Evm_Dmrs_High_Current.rst
	NrSubMeas_MultiEval_ListPy_Cc_Modulation_Evm_Dmrs_High_Extreme.rst
	NrSubMeas_MultiEval_ListPy_Cc_Modulation_Evm_Dmrs_High_StandardDev.rst