Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:MEValuation[:LAYer<layer>]:PDYNamics:MAXimum
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation[:LAYer<layer>]:PDYNamics:MAXimum
	single: CALCulate:NRSub:MEASurement<Instance>:MEValuation[:LAYer<layer>]:PDYNamics:MAXimum

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:MEValuation[:LAYer<layer>]:PDYNamics:MAXimum
	FETCh:NRSub:MEASurement<Instance>:MEValuation[:LAYer<layer>]:PDYNamics:MAXimum
	CALCulate:NRSub:MEASurement<Instance>:MEValuation[:LAYer<layer>]:PDYNamics:MAXimum



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.Layer.Pdynamics.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: