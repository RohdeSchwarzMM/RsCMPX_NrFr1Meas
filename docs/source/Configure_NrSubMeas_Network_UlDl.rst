UlDl
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:NETWork:ULDL:PERiodicity

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:NETWork:ULDL:PERiodicity



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.Network.UlDl.UlDlCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.nrSubMeas.network.ulDl.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_NrSubMeas_Network_UlDl_Pattern.rst