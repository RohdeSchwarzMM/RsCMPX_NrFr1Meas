Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:MEValuation:TRACe:ACLR:ENDC:AVERage
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:TRACe:ACLR:ENDC:AVERage

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:MEValuation:TRACe:ACLR:ENDC:AVERage
	FETCh:NRSub:MEASurement<Instance>:MEValuation:TRACe:ACLR:ENDC:AVERage



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.Trace.Aclr.Endc.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: