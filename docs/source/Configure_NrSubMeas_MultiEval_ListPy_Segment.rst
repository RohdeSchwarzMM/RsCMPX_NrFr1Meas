Segment<SEGMent>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr512
	rc = driver.configure.nrSubMeas.multiEval.listPy.segment.repcap_sEGMent_get()
	driver.configure.nrSubMeas.multiEval.listPy.segment.repcap_sEGMent_set(repcap.SEGMent.Nr1)





.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.MultiEval.ListPy.Segment.SegmentCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.nrSubMeas.multiEval.listPy.segment.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_NrSubMeas_MultiEval_ListPy_Segment_Aclr.rst
	Configure_NrSubMeas_MultiEval_ListPy_Segment_Cidx.rst
	Configure_NrSubMeas_MultiEval_ListPy_Segment_Endc.rst
	Configure_NrSubMeas_MultiEval_ListPy_Segment_Fdistance.rst
	Configure_NrSubMeas_MultiEval_ListPy_Segment_Modulation.rst
	Configure_NrSubMeas_MultiEval_ListPy_Segment_Pmonitor.rst
	Configure_NrSubMeas_MultiEval_ListPy_Segment_Power.rst
	Configure_NrSubMeas_MultiEval_ListPy_Segment_PuschConfig.rst
	Configure_NrSubMeas_MultiEval_ListPy_Segment_SeMask.rst
	Configure_NrSubMeas_MultiEval_ListPy_Segment_Setup.rst
	Configure_NrSubMeas_MultiEval_ListPy_Segment_SingleCmw.rst