Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:MEValuation:TXPower:MAXimum
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:TXPower:MAXimum

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:MEValuation:TXPower:MAXimum
	FETCh:NRSub:MEASurement<Instance>:MEValuation:TXPower:MAXimum



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.TxPower.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: