Merror
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:LIMit:QAM<order>:MERRor

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:MEValuation:LIMit:QAM<order>:MERRor



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.MultiEval.Limit.Qam.Merror.MerrorCls
	:members:
	:undoc-members:
	:noindex: