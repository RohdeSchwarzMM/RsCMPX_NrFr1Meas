Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:MEValuation:PMONitor:CURRent
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:PMONitor:CURRent

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:MEValuation:PMONitor:CURRent
	FETCh:NRSub:MEASurement<Instance>:MEValuation:PMONitor:CURRent



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.Pmonitor.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: