Enums
=========

AllocatedSlots
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AllocatedSlots.ALL
	# All values (1x):
	ALL

Band
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.Band.OB1
	# Last value:
	value = enums.Band.OB99
	# All values (61x):
	OB1 | OB100 | OB101 | OB104 | OB105 | OB12 | OB13 | OB14
	OB18 | OB2 | OB20 | OB24 | OB25 | OB255 | OB256 | OB26
	OB28 | OB3 | OB30 | OB34 | OB38 | OB39 | OB40 | OB41
	OB46 | OB47 | OB48 | OB5 | OB50 | OB51 | OB53 | OB65
	OB66 | OB7 | OB70 | OB71 | OB74 | OB75 | OB76 | OB77
	OB78 | OB79 | OB8 | OB80 | OB81 | OB82 | OB83 | OB84
	OB85 | OB86 | OB89 | OB90 | OB91 | OB92 | OB93 | OB94
	OB95 | OB96 | OB97 | OB98 | OB99

BandwidthPart
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.BandwidthPart.BWP0
	# All values (1x):
	BWP0

CarrierComponent
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CarrierComponent.CC1
	# All values (2x):
	CC1 | CC2

CarrierPosition
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CarrierPosition.LONR
	# All values (2x):
	LONR | RONR

ChannelBwidth
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.ChannelBwidth.B005
	# Last value:
	value = enums.ChannelBwidth.B100
	# All values (15x):
	B005 | B010 | B015 | B020 | B025 | B030 | B035 | B040
	B045 | B050 | B060 | B070 | B080 | B090 | B100

ChannelBwidthB
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ChannelBwidthB.B005
	# All values (4x):
	B005 | B010 | B015 | B020

ChannelTypeA
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ChannelTypeA.PUCCh
	# All values (2x):
	PUCCh | PUSCh

ChannelTypeB
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ChannelTypeB.OFF
	# All values (4x):
	OFF | ON | PUCCh | PUSCh

ChannelTypeD
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ChannelTypeD.PSSCh
	# All values (3x):
	PSSCh | PUCCh | PUSCh

CmwsConnector
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.CmwsConnector.R11
	# Last value:
	value = enums.CmwsConnector.RB8
	# All values (48x):
	R11 | R12 | R13 | R14 | R15 | R16 | R17 | R18
	R21 | R22 | R23 | R24 | R25 | R26 | R27 | R28
	R31 | R32 | R33 | R34 | R35 | R36 | R37 | R38
	R41 | R42 | R43 | R44 | R45 | R46 | R47 | R48
	RA1 | RA2 | RA3 | RA4 | RA5 | RA6 | RA7 | RA8
	RB1 | RB2 | RB3 | RB4 | RB5 | RB6 | RB7 | RB8

ConfigType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ConfigType.T1
	# All values (2x):
	T1 | T2

CyclicPrefix
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CyclicPrefix.EXTended
	# All values (2x):
	EXTended | NORMal

Direction
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Direction.ALTernating
	# All values (3x):
	ALTernating | RDOWn | RUP

DmrsInit
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DmrsInit.CID
	# All values (2x):
	CID | DID

DmrsPort
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DmrsPort.ALL
	# All values (3x):
	ALL | P1000 | P1001

DuplexModeB
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DuplexModeB.FDD
	# All values (2x):
	FDD | TDD

Generator
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Generator.DID
	# All values (2x):
	DID | PHY

GhopingInit
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.GhopingInit.CID
	# All values (2x):
	CID | HID

GroupHopping
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.GroupHopping.DISable
	# All values (3x):
	DISable | ENABle | NEITher

Ktc
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Ktc.N2
	# All values (3x):
	N2 | N4 | N8

Lagging
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Lagging.MS05
	# All values (3x):
	MS05 | MS25 | OFF

Leading
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Leading.MS25
	# All values (2x):
	MS25 | OFF

ListMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ListMode.ONCE
	# All values (2x):
	ONCE | SEGMent

LowHigh
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.LowHigh.HIGH
	# All values (2x):
	HIGH | LOW

MappingType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MappingType.A
	# All values (2x):
	A | B

MaxLength
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MaxLength.DOUBle
	# All values (2x):
	DOUBle | SINGle

MeasFilter
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MeasFilter.BANDpass
	# All values (2x):
	BANDpass | GAUSs

MeasurementMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MeasurementMode.MELMode
	# All values (2x):
	MELMode | NORMal

MeasureSlot
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MeasureSlot.ALL
	# All values (6x):
	ALL | MS0 | MS1 | MS2 | MS3 | UDEF

MevLimit
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MevLimit.STD
	# All values (2x):
	STD | UDEF

Modulation
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Modulation.BPSK
	# All values (6x):
	BPSK | BPWS | Q16 | Q256 | Q64 | QPSK

ModulationScheme
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ModulationScheme.AUTO
	# All values (7x):
	AUTO | BPSK | BPWS | Q16 | Q256 | Q64 | QPSK

ModulationSchemeB
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ModulationSchemeB.Q16
	# All values (4x):
	Q16 | Q256 | Q64 | QPSK

NbTrigger
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.NbTrigger.M010
	# All values (4x):
	M010 | M020 | M040 | M080

NetworkSigVal
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.NetworkSigVal.NS01
	# Last value:
	value = enums.NetworkSigVal.NSU43
	# All values (103x):
	NS01 | NS02 | NS03 | NS04 | NS05 | NS06 | NS07 | NS08
	NS09 | NS10 | NS100 | NS11 | NS12 | NS13 | NS14 | NS15
	NS16 | NS17 | NS18 | NS19 | NS20 | NS21 | NS22 | NS23
	NS24 | NS25 | NS26 | NS27 | NS28 | NS29 | NS30 | NS31
	NS32 | NS33 | NS34 | NS35 | NS36 | NS37 | NS38 | NS39
	NS40 | NS41 | NS42 | NS43 | NS44 | NS45 | NS46 | NS47
	NS48 | NS49 | NS50 | NS51 | NS52 | NS53 | NS54 | NS55
	NS56 | NS57 | NS58 | NS59 | NS60 | NS61 | NS62 | NS63
	NS64 | NS65 | NS66 | NS67 | NS68 | NS69 | NS70 | NS71
	NS72 | NS73 | NS74 | NS75 | NS76 | NS77 | NS78 | NS79
	NS80 | NS81 | NS82 | NS83 | NS84 | NS85 | NS86 | NS87
	NS88 | NS89 | NS90 | NS91 | NS92 | NS93 | NS94 | NS95
	NS96 | NS97 | NS98 | NS99 | NSU03 | NSU05 | NSU43

NumberSymbols
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.NumberSymbols.N1
	# All values (7x):
	N1 | N10 | N12 | N14 | N2 | N4 | N8

ParameterSetMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ParameterSetMode.GLOBal
	# All values (2x):
	GLOBal | LIST

Pattern
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Pattern.A
	# All values (3x):
	A | B | C

Periodicity
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.Periodicity.MS05
	# Last value:
	value = enums.Periodicity.MS5
	# All values (9x):
	MS05 | MS1 | MS10 | MS125 | MS2 | MS25 | MS3 | MS4
	MS5

PeriodPreamble
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PeriodPreamble.MS05
	# All values (3x):
	MS05 | MS10 | MS20

PhaseComp
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PhaseComp.CAF
	# All values (3x):
	CAF | OFF | UDEF

PreambleFormat
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.PreambleFormat.PF0
	# Last value:
	value = enums.PreambleFormat.PFC2
	# All values (13x):
	PF0 | PF1 | PF2 | PF3 | PFA1 | PFA2 | PFA3 | PFB1
	PFB2 | PFB3 | PFB4 | PFC0 | PFC2

PucchFormat
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PucchFormat.F0
	# All values (5x):
	F0 | F1 | F2 | F3 | F4

RbwA
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.RbwA.K030
	# All values (3x):
	K030 | M1 | PC1

RbwB
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.RbwB.K030
	# All values (6x):
	K030 | K100 | K400 | M1 | PC1 | PC2

RbwC
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.RbwC.K030
	# All values (3x):
	K030 | K400 | M1

Repeat
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Repeat.CONTinuous
	# All values (2x):
	CONTinuous | SINGleshot

ResourceState
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ResourceState.ACTive
	# All values (8x):
	ACTive | ADJusted | INValid | OFF | PENDing | QUEued | RDY | RUN

RestrictedSet
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.RestrictedSet.URES
	# All values (1x):
	URES

Result
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Result.FAIL
	# All values (2x):
	FAIL | PASS

ResultStatus2
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.ResultStatus2.DC
	# Last value:
	value = enums.ResultStatus2.ULEU
	# All values (10x):
	DC | INV | NAV | NCAP | OFF | OFL | OK | UFL
	ULEL | ULEU

RetriggerFlag
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.RetriggerFlag.IFPNarrowband
	# All values (4x):
	IFPNarrowband | IFPower | OFF | ON

Sharing
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Sharing.FSHared
	# All values (3x):
	FSHared | NSHared | OCONnection

SignalPath
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SignalPath.NETWork
	# All values (2x):
	NETWork | STANdalone

SignalSlope
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SignalSlope.FEDGe
	# All values (2x):
	FEDGe | REDGe

SignalType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SignalType.SL
	# All values (2x):
	SL | UL

SrsPeriodicity
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.SrsPeriodicity.SL1
	# Last value:
	value = enums.SrsPeriodicity.SL80
	# All values (17x):
	SL1 | SL10 | SL1280 | SL16 | SL160 | SL2 | SL20 | SL2560
	SL32 | SL320 | SL4 | SL40 | SL5 | SL64 | SL640 | SL8
	SL80

StopCondition
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.StopCondition.NONE
	# All values (2x):
	NONE | SLFail

SubCarrSpacing
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SubCarrSpacing.S15K
	# All values (3x):
	S15K | S30K | S60K

SubCarrSpacingB
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SubCarrSpacingB.S15K
	# All values (5x):
	S15K | S1K2 | S30K | S5K | S60K

SubChanSize
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SubChanSize.RB10
	# All values (8x):
	RB10 | RB100 | RB12 | RB15 | RB20 | RB25 | RB50 | RB75

SyncMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SyncMode.ENHanced
	# All values (4x):
	ENHanced | ESSLot | NORMal | NSSLot

TargetStateA
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TargetStateA.OFF
	# All values (3x):
	OFF | RDY | RUN

TargetSyncState
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TargetSyncState.ADJusted
	# All values (2x):
	ADJusted | PENDing

TimeMask
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TimeMask.GOO
	# All values (3x):
	GOO | PPSRs | SBLanking

TraceSelect
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TraceSelect.AVERage
	# All values (3x):
	AVERage | CURRent | MAXimum

