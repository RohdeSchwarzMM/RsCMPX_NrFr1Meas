Offset
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>[:CC<no>]:TXBWidth:OFFSet

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>[:CC<no>]:TXBWidth:OFFSet



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.Cc.TxBwidth.Offset.OffsetCls
	:members:
	:undoc-members:
	:noindex: