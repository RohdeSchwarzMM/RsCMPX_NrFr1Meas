Result
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:RESult:MODulation
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:RESult:SEMask
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:RESult:ACLR
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:RESult:PMONitor
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:RESult:PDYNamics
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:RESult:TXPower
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:RESult[:ALL]
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:RESult:MERRor
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:RESult:PERRor
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:RESult:IEMissions
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:RESult:EVMC
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:RESult:IQ
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:RESult:ESFLatness
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:RESult:TXM

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:MEValuation:RESult:MODulation
	CONFigure:NRSub:MEASurement<Instance>:MEValuation:RESult:SEMask
	CONFigure:NRSub:MEASurement<Instance>:MEValuation:RESult:ACLR
	CONFigure:NRSub:MEASurement<Instance>:MEValuation:RESult:PMONitor
	CONFigure:NRSub:MEASurement<Instance>:MEValuation:RESult:PDYNamics
	CONFigure:NRSub:MEASurement<Instance>:MEValuation:RESult:TXPower
	CONFigure:NRSub:MEASurement<Instance>:MEValuation:RESult[:ALL]
	CONFigure:NRSub:MEASurement<Instance>:MEValuation:RESult:MERRor
	CONFigure:NRSub:MEASurement<Instance>:MEValuation:RESult:PERRor
	CONFigure:NRSub:MEASurement<Instance>:MEValuation:RESult:IEMissions
	CONFigure:NRSub:MEASurement<Instance>:MEValuation:RESult:EVMC
	CONFigure:NRSub:MEASurement<Instance>:MEValuation:RESult:IQ
	CONFigure:NRSub:MEASurement<Instance>:MEValuation:RESult:ESFLatness
	CONFigure:NRSub:MEASurement<Instance>:MEValuation:RESult:TXM



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.MultiEval.Result.ResultCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.nrSubMeas.multiEval.result.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_NrSubMeas_MultiEval_Result_EvMagnitude.rst