Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:PRACh:TRACe:MERRor:MAXimum
	single: FETCh:NRSub:MEASurement<Instance>:PRACh:TRACe:MERRor:MAXimum

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:PRACh:TRACe:MERRor:MAXimum
	FETCh:NRSub:MEASurement<Instance>:PRACh:TRACe:MERRor:MAXimum



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.Prach.Trace.Merror.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: