BwConfig
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:BWConfig

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:MEValuation:BWConfig



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.MultiEval.BwConfig.BwConfigCls
	:members:
	:undoc-members:
	:noindex: