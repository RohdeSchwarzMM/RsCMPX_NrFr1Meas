ListPy
----------------------------------------





.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.ListPy.ListPyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrSubMeas.multiEval.listPy.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrSubMeas_MultiEval_ListPy_Aclr.rst
	NrSubMeas_MultiEval_ListPy_Cc.rst
	NrSubMeas_MultiEval_ListPy_Pmonitor.rst
	NrSubMeas_MultiEval_ListPy_Power.rst
	NrSubMeas_MultiEval_ListPy_Segment.rst
	NrSubMeas_MultiEval_ListPy_SeMask.rst
	NrSubMeas_MultiEval_ListPy_Sreliability.rst