BpwShaping
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:LIMit:BPWShaping:FERRor

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:MEValuation:LIMit:BPWShaping:FERRor



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.MultiEval.Limit.BpwShaping.BpwShapingCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.nrSubMeas.multiEval.limit.bpwShaping.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_NrSubMeas_MultiEval_Limit_BpwShaping_EsFlatness.rst
	Configure_NrSubMeas_MultiEval_Limit_BpwShaping_EvMagnitude.rst
	Configure_NrSubMeas_MultiEval_Limit_BpwShaping_Ibe.rst
	Configure_NrSubMeas_MultiEval_Limit_BpwShaping_IqOffset.rst
	Configure_NrSubMeas_MultiEval_Limit_BpwShaping_Merror.rst
	Configure_NrSubMeas_MultiEval_Limit_BpwShaping_Perror.rst