Rsetting
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:PRACh:RSETting

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:PRACh:RSETting



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.Prach.Rsetting.RsettingCls
	:members:
	:undoc-members:
	:noindex: