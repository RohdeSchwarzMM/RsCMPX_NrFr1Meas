Evm
----------------------------------------





.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.Srs.Trace.Evm.EvmCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrSubMeas.srs.trace.evm.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrSubMeas_Srs_Trace_Evm_Average.rst
	NrSubMeas_Srs_Trace_Evm_Current.rst
	NrSubMeas_Srs_Trace_Evm_Maximum.rst