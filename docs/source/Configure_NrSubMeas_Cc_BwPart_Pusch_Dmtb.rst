Dmtb
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>[:CC<no>]:BWPart:PUSCh:DMTB

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>[:CC<no>]:BWPart:PUSCh:DMTB



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.Cc.BwPart.Pusch.Dmtb.DmtbCls
	:members:
	:undoc-members:
	:noindex: