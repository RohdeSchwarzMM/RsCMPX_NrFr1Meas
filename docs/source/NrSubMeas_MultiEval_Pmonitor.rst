Pmonitor
----------------------------------------





.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.Pmonitor.PmonitorCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrSubMeas.multiEval.pmonitor.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrSubMeas_MultiEval_Pmonitor_Average.rst
	NrSubMeas_MultiEval_Pmonitor_Current.rst
	NrSubMeas_MultiEval_Pmonitor_Maximum.rst
	NrSubMeas_MultiEval_Pmonitor_Minimum.rst
	NrSubMeas_MultiEval_Pmonitor_StandardDev.rst