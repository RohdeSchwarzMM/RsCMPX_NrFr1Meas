Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:PRACh:TRACe:EVM:MAXimum
	single: FETCh:NRSub:MEASurement<Instance>:PRACh:TRACe:EVM:MAXimum

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:PRACh:TRACe:EVM:MAXimum
	FETCh:NRSub:MEASurement<Instance>:PRACh:TRACe:EVM:MAXimum



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.Prach.Trace.Evm.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: