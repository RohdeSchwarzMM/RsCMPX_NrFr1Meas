PvSymbol
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:SRS:PVSYmbol
	single: FETCh:NRSub:MEASurement<Instance>:SRS:PVSYmbol

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:SRS:PVSYmbol
	FETCh:NRSub:MEASurement<Instance>:SRS:PVSYmbol



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.Srs.PvSymbol.PvSymbolCls
	:members:
	:undoc-members:
	:noindex: