Mscheme
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>[:CC<no>]:PSSCh:MSCHeme

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>[:CC<no>]:PSSCh:MSCHeme



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.Cc.Pssch.Mscheme.MschemeCls
	:members:
	:undoc-members:
	:noindex: