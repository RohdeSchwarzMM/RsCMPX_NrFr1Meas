Nr
----------------------------------------





.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.ListPy.Aclr.Nr.NrCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrSubMeas.multiEval.listPy.aclr.nr.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrSubMeas_MultiEval_ListPy_Aclr_Nr_Average.rst
	NrSubMeas_MultiEval_ListPy_Aclr_Nr_Current.rst
	NrSubMeas_MultiEval_ListPy_Aclr_Nr_Negativ.rst
	NrSubMeas_MultiEval_ListPy_Aclr_Nr_Positiv.rst