Nsymbol
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NRSub:MEASurement<Instance>:SRS:MODulation:NSYMbol

.. code-block:: python

	FETCh:NRSub:MEASurement<Instance>:SRS:MODulation:NSYMbol



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.Srs.Modulation.Nsymbol.NsymbolCls
	:members:
	:undoc-members:
	:noindex: