Frequency
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>[:CC<no>]:FREQuency

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>[:CC<no>]:FREQuency



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.Cc.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex: