StandardDev
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:MEValuation:SEMask:SDEViation
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:SEMask:SDEViation

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:MEValuation:SEMask:SDEViation
	FETCh:NRSub:MEASurement<Instance>:MEValuation:SEMask:SDEViation



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.SeMask.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: