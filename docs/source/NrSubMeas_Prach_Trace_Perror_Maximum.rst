Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:PRACh:TRACe:PERRor:MAXimum
	single: FETCh:NRSub:MEASurement<Instance>:PRACh:TRACe:PERRor:MAXimum

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:PRACh:TRACe:PERRor:MAXimum
	FETCh:NRSub:MEASurement<Instance>:PRACh:TRACe:PERRor:MAXimum



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.Prach.Trace.Perror.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: