Sreliability
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST:SRELiability

.. code-block:: python

	FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST:SRELiability



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.ListPy.Sreliability.SreliabilityCls
	:members:
	:undoc-members:
	:noindex: