Modulation
----------------------------------------





.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.ListPy.Segment.Cc.Modulation.ModulationCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrSubMeas.multiEval.listPy.segment.cc.modulation.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrSubMeas_MultiEval_ListPy_Segment_Cc_Modulation_Average.rst
	NrSubMeas_MultiEval_ListPy_Segment_Cc_Modulation_Current.rst
	NrSubMeas_MultiEval_ListPy_Segment_Cc_Modulation_Dallocation.rst
	NrSubMeas_MultiEval_ListPy_Segment_Cc_Modulation_DchType.rst
	NrSubMeas_MultiEval_ListPy_Segment_Cc_Modulation_Dmodulation.rst
	NrSubMeas_MultiEval_ListPy_Segment_Cc_Modulation_Extreme.rst
	NrSubMeas_MultiEval_ListPy_Segment_Cc_Modulation_StandardDev.rst