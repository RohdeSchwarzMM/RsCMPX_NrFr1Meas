Maximum<Maximum>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr2
	rc = driver.nrSubMeas.multiEval.listPy.cc.esFlatness.scIndex.maximum.repcap_maximum_get()
	driver.nrSubMeas.multiEval.listPy.cc.esFlatness.scIndex.maximum.repcap_maximum_set(repcap.Maximum.Nr1)





.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.ListPy.Cc.EsFlatness.ScIndex.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrSubMeas.multiEval.listPy.cc.esFlatness.scIndex.maximum.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrSubMeas_MultiEval_ListPy_Cc_EsFlatness_ScIndex_Maximum_Current.rst