Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:PRACh:TRACe:MERRor:CURRent
	single: FETCh:NRSub:MEASurement<Instance>:PRACh:TRACe:MERRor:CURRent

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:PRACh:TRACe:MERRor:CURRent
	FETCh:NRSub:MEASurement<Instance>:PRACh:TRACe:MERRor:CURRent



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.Prach.Trace.Merror.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: