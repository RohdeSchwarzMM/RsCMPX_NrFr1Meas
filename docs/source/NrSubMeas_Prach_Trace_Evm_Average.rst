Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:PRACh:TRACe:EVM:AVERage
	single: FETCh:NRSub:MEASurement<Instance>:PRACh:TRACe:EVM:AVERage

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:PRACh:TRACe:EVM:AVERage
	FETCh:NRSub:MEASurement<Instance>:PRACh:TRACe:EVM:AVERage



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.Prach.Trace.Evm.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: