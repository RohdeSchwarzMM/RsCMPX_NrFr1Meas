Pdynamics
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:DMARker<No>:PDYNamics

.. code-block:: python

	FETCh:NRSub:MEASurement<Instance>:MEValuation:DMARker<No>:PDYNamics



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.Dmarker.Pdynamics.PdynamicsCls
	:members:
	:undoc-members:
	:noindex: