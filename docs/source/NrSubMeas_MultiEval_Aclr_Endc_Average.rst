Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:MEValuation:ACLR:ENDC:AVERage
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:ACLR:ENDC:AVERage
	single: CALCulate:NRSub:MEASurement<Instance>:MEValuation:ACLR:ENDC:AVERage

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:MEValuation:ACLR:ENDC:AVERage
	FETCh:NRSub:MEASurement<Instance>:MEValuation:ACLR:ENDC:AVERage
	CALCulate:NRSub:MEASurement<Instance>:MEValuation:ACLR:ENDC:AVERage



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.Aclr.Endc.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: