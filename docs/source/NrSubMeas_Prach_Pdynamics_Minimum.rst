Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:PRACh:PDYNamics:MINimum
	single: FETCh:NRSub:MEASurement<Instance>:PRACh:PDYNamics:MINimum
	single: CALCulate:NRSub:MEASurement<Instance>:PRACh:PDYNamics:MINimum

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:PRACh:PDYNamics:MINimum
	FETCh:NRSub:MEASurement<Instance>:PRACh:PDYNamics:MINimum
	CALCulate:NRSub:MEASurement<Instance>:PRACh:PDYNamics:MINimum



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.Prach.Pdynamics.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: