Spectrum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:SCOunt:SPECtrum:SEMask
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:SCOunt:SPECtrum:ACLR

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:MEValuation:SCOunt:SPECtrum:SEMask
	CONFigure:NRSub:MEASurement<Instance>:MEValuation:SCOunt:SPECtrum:ACLR



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.MultiEval.Scount.Spectrum.SpectrumCls
	:members:
	:undoc-members:
	:noindex: