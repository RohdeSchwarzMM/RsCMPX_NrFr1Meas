Ptolerance<PowerStep>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr7
	rc = driver.configure.nrSubMeas.srs.limit.ptolerance.repcap_powerStep_get()
	driver.configure.nrSubMeas.srs.limit.ptolerance.repcap_powerStep_set(repcap.PowerStep.Nr1)



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:SRS:LIMit:PTOLerance<PowerStep>

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:SRS:LIMit:PTOLerance<PowerStep>



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.Srs.Limit.Ptolerance.PtoleranceCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.nrSubMeas.srs.limit.ptolerance.clone()