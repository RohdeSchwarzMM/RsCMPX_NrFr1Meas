Caggregation
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:LIMit:SEMask:STANdard:AREA<nr>:CAGGregation

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:MEValuation:LIMit:SEMask:STANdard:AREA<nr>:CAGGregation



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.MultiEval.Limit.SeMask.Standard.Area.Caggregation.CaggregationCls
	:members:
	:undoc-members:
	:noindex: