Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:PRACh:EVMSymbol:PEAK:CURRent
	single: FETCh:NRSub:MEASurement<Instance>:PRACh:EVMSymbol:PEAK:CURRent

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:PRACh:EVMSymbol:PEAK:CURRent
	FETCh:NRSub:MEASurement<Instance>:PRACh:EVMSymbol:PEAK:CURRent



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.Prach.EvmSymbol.Peak.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: