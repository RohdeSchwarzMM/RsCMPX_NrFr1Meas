Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:PRACh:MODulation:CURRent
	single: FETCh:NRSub:MEASurement<Instance>:PRACh:MODulation:CURRent
	single: CALCulate:NRSub:MEASurement<Instance>:PRACh:MODulation:CURRent

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:PRACh:MODulation:CURRent
	FETCh:NRSub:MEASurement<Instance>:PRACh:MODulation:CURRent
	CALCulate:NRSub:MEASurement<Instance>:PRACh:MODulation:CURRent



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.Prach.Modulation.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: