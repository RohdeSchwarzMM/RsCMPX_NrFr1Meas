Area<AreaReduced>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr4
	rc = driver.configure.nrSubMeas.multiEval.limit.seMask.standard.area.repcap_areaReduced_get()
	driver.configure.nrSubMeas.multiEval.limit.seMask.standard.area.repcap_areaReduced_set(repcap.AreaReduced.Nr1)





.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.MultiEval.Limit.SeMask.Standard.Area.AreaCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.nrSubMeas.multiEval.limit.seMask.standard.area.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_NrSubMeas_MultiEval_Limit_SeMask_Standard_Area_Caggregation.rst
	Configure_NrSubMeas_MultiEval_Limit_SeMask_Standard_Area_Endc.rst