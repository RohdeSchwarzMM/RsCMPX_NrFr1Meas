Pssch
----------------------------------------





.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.Cc.Pssch.PsschCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.nrSubMeas.cc.pssch.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_NrSubMeas_Cc_Pssch_Dport.rst
	Configure_NrSubMeas_Cc_Pssch_Mscheme.rst
	Configure_NrSubMeas_Cc_Pssch_Ndmrs.rst
	Configure_NrSubMeas_Cc_Pssch_Nlayers.rst
	Configure_NrSubMeas_Cc_Pssch_NsubChannels.rst
	Configure_NrSubMeas_Cc_Pssch_Nsymbols.rst