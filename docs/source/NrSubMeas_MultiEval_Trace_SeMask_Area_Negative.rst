Negative
----------------------------------------





.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.Trace.SeMask.Area.Negative.NegativeCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrSubMeas.multiEval.trace.seMask.area.negative.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrSubMeas_MultiEval_Trace_SeMask_Area_Negative_Average.rst
	NrSubMeas_MultiEval_Trace_SeMask_Area_Negative_Current.rst
	NrSubMeas_MultiEval_Trace_SeMask_Area_Negative_Frequency.rst
	NrSubMeas_MultiEval_Trace_SeMask_Area_Negative_Maximum.rst