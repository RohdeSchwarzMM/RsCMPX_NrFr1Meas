Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:SRS:MODulation:AVERage
	single: FETCh:NRSub:MEASurement<Instance>:SRS:MODulation:AVERage
	single: CALCulate:NRSub:MEASurement<Instance>:SRS:MODulation:AVERage

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:SRS:MODulation:AVERage
	FETCh:NRSub:MEASurement<Instance>:SRS:MODulation:AVERage
	CALCulate:NRSub:MEASurement<Instance>:SRS:MODulation:AVERage



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.Srs.Modulation.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: