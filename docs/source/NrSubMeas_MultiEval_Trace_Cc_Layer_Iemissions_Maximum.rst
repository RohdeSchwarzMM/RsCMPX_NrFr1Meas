Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:MEValuation:TRACe[:CC<no>][:LAYer<layer>]:IEMissions:MAXimum
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:TRACe[:CC<no>][:LAYer<layer>]:IEMissions:MAXimum

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:MEValuation:TRACe[:CC<no>][:LAYer<layer>]:IEMissions:MAXimum
	FETCh:NRSub:MEASurement<Instance>:MEValuation:TRACe[:CC<no>][:LAYer<layer>]:IEMissions:MAXimum



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.Trace.Cc.Layer.Iemissions.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: