Aclr
----------------------------------------





.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.ListPy.Segment.Aclr.AclrCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrSubMeas.multiEval.listPy.segment.aclr.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrSubMeas_MultiEval_ListPy_Segment_Aclr_Average.rst
	NrSubMeas_MultiEval_ListPy_Segment_Aclr_Current.rst
	NrSubMeas_MultiEval_ListPy_Segment_Aclr_Dallocation.rst
	NrSubMeas_MultiEval_ListPy_Segment_Aclr_DchType.rst
	NrSubMeas_MultiEval_ListPy_Segment_Aclr_Endc.rst