Slots
----------------------------------------





.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.ListPy.Segment.Pmonitor.Slots.SlotsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrSubMeas.multiEval.listPy.segment.pmonitor.slots.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrSubMeas_MultiEval_ListPy_Segment_Pmonitor_Slots_Array.rst
	NrSubMeas_MultiEval_ListPy_Segment_Pmonitor_Slots_Peak.rst
	NrSubMeas_MultiEval_ListPy_Segment_Pmonitor_Slots_Rms.rst