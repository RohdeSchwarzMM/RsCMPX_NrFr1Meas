Mslot
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:MSLot

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:MEValuation:MSLot



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.MultiEval.Mslot.MslotCls
	:members:
	:undoc-members:
	:noindex: