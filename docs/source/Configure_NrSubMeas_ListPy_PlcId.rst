PlcId
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:LIST:PLCid:MODE

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:LIST:PLCid:MODE



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.ListPy.PlcId.PlcIdCls
	:members:
	:undoc-members:
	:noindex: