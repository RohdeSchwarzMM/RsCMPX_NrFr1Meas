Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:MEValuation:TRACe:ACLR:CURRent
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:TRACe:ACLR:CURRent

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:MEValuation:TRACe:ACLR:CURRent
	FETCh:NRSub:MEASurement<Instance>:MEValuation:TRACe:ACLR:CURRent



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.Trace.Aclr.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: