Nsymbols
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>[:CC<no>]:PSSCh:NSYMbols

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>[:CC<no>]:PSSCh:NSYMbols



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.Cc.Pssch.Nsymbols.NsymbolsCls
	:members:
	:undoc-members:
	:noindex: