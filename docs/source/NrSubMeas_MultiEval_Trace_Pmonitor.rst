Pmonitor
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:MEValuation:TRACe:PMONitor
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:TRACe:PMONitor

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:MEValuation:TRACe:PMONitor
	FETCh:NRSub:MEASurement<Instance>:MEValuation:TRACe:PMONitor



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.Trace.Pmonitor.PmonitorCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrSubMeas.multiEval.trace.pmonitor.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrSubMeas_MultiEval_Trace_Pmonitor_Slots.rst