Cfrequency
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:NRSub:MEASurement<Instance>:LIST:SEGMent<no>:CFRequency

.. code-block:: python

	SENSe:NRSub:MEASurement<Instance>:LIST:SEGMent<no>:CFRequency



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Sense.NrSubMeas.ListPy.Segment.Cfrequency.CfrequencyCls
	:members:
	:undoc-members:
	:noindex: