Dallocation
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation[:CC<no>]:MODulation:DALLocation

.. code-block:: python

	FETCh:NRSub:MEASurement<Instance>:MEValuation[:CC<no>]:MODulation:DALLocation



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.Cc.Modulation.Dallocation.DallocationCls
	:members:
	:undoc-members:
	:noindex: