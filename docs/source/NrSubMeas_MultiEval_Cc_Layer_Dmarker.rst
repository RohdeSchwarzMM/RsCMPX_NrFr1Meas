Dmarker<DeltaMarker>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr2
	rc = driver.nrSubMeas.multiEval.cc.layer.dmarker.repcap_deltaMarker_get()
	driver.nrSubMeas.multiEval.cc.layer.dmarker.repcap_deltaMarker_set(repcap.DeltaMarker.Nr1)





.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.Cc.Layer.Dmarker.DmarkerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrSubMeas.multiEval.cc.layer.dmarker.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrSubMeas_MultiEval_Cc_Layer_Dmarker_EvMagnitude.rst
	NrSubMeas_MultiEval_Cc_Layer_Dmarker_Merror.rst
	NrSubMeas_MultiEval_Cc_Layer_Dmarker_Perror.rst