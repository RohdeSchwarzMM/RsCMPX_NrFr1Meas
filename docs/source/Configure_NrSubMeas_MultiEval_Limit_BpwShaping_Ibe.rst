Ibe
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:NRSub:MEASurement<Instance>:MEValuation:LIMit:BPWShaping:IBE

.. code-block:: python

	CONFigure:NRSub:MEASurement<Instance>:MEValuation:LIMit:BPWShaping:IBE



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.Configure.NrSubMeas.MultiEval.Limit.BpwShaping.Ibe.IbeCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.nrSubMeas.multiEval.limit.bpwShaping.ibe.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_NrSubMeas_MultiEval_Limit_BpwShaping_Ibe_IqOffset.rst