Peak
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation[:CC<no>][:LAYer<layer>]:DMARker<Nr>:EVMagnitude:PEAK

.. code-block:: python

	FETCh:NRSub:MEASurement<Instance>:MEValuation[:CC<no>][:LAYer<layer>]:DMARker<Nr>:EVMagnitude:PEAK



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.Cc.Layer.Dmarker.EvMagnitude.Peak.PeakCls
	:members:
	:undoc-members:
	:noindex: