Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:NRSub:MEASurement<Instance>:SRS:EVMSymbol:AVERage
	single: FETCh:NRSub:MEASurement<Instance>:SRS:EVMSymbol:AVERage

.. code-block:: python

	READ:NRSub:MEASurement<Instance>:SRS:EVMSymbol:AVERage
	FETCh:NRSub:MEASurement<Instance>:SRS:EVMSymbol:AVERage



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.Srs.EvmSymbol.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: