Current
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST:SEMask:MARGin:AREA<nr6g>:NEGativ:CURRent

.. code-block:: python

	FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST:SEMask:MARGin:AREA<nr6g>:NEGativ:CURRent



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.ListPy.SeMask.Margin.Area.Negativ.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: