Area<Area>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr12
	rc = driver.nrSubMeas.multiEval.listPy.seMask.margin.area.repcap_area_get()
	driver.nrSubMeas.multiEval.listPy.seMask.margin.area.repcap_area_set(repcap.Area.Nr1)





.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.ListPy.SeMask.Margin.Area.AreaCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.nrSubMeas.multiEval.listPy.seMask.margin.area.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	NrSubMeas_MultiEval_ListPy_SeMask_Margin_Area_Negativ.rst
	NrSubMeas_MultiEval_ListPy_SeMask_Margin_Area_Positiv.rst