Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST:SEMask:TXPower:AVERage
	single: CALCulate:NRSub:MEASurement<Instance>:MEValuation:LIST:SEMask:TXPower:AVERage

.. code-block:: python

	FETCh:NRSub:MEASurement<Instance>:MEValuation:LIST:SEMask:TXPower:AVERage
	CALCulate:NRSub:MEASurement<Instance>:MEValuation:LIST:SEMask:TXPower:AVERage



.. autoclass:: RsCMPX_NrFr1Meas.Implementations.NrSubMeas.MultiEval.ListPy.SeMask.TxPower.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: